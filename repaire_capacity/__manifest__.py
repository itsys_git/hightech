# -*- coding: utf-8 -*-
{
    'name': "Repair Employee Capacity",



    'author': "IT Systems Corportion Marwa Ahmed",
    'website': "http://www.it-syscorp.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','repair'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',

        'views/maintenance_capacity.xml',


        
    ],

}
