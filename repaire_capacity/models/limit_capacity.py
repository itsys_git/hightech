from odoo import models, fields, api
from odoo.exceptions import ValidationError

class Repaire(models.Model):

    _inherit = 'repair.order'
    date_sheh = fields.Date('Schedule Date', copy=False,track_visibility='onchange')


    @api.constrains('re_assign_to_employee_id.capacity','date_sheh')
    def not_minus(self):
        for rec in self:
            if rec.re_assign_to_employee_id.capacity == 0 and rec.re_assign_to_employee_id :
                raise ValidationError('Capacity is Zero')
            emp_capacity_obj = self.env['employee.capacity']
            search_date = emp_capacity_obj.search(
                [('employee_id', '=', rec.re_assign_to_employee_id.id),
                 ('date', '=',rec.date_sheh)])
            if search_date:
                for recs in search_date:
                    if recs.cap_count -1 == rec.re_assign_to_employee_id.capacity:
                        raise ValidationError("Number of Capacity Completed in This Day")
    #
    # @api.model
    # def create(self, vals):
    #     res = super(Repaire, self).create(vals)
    #     emp_capacity_obj = self.env['employee.capacity']
    #
    #     if vals.get('re_assign_to_employee_id') and vals.get('date_sheh'):
    #         employee_obj = self.env['hr.employee'].browse(vals.get('re_assign_to_employee_id'))
    #         print(employee_obj.capacity)
    #
    #         search_date= emp_capacity_obj.search(
    #             [('employee_id', '=', vals.get('re_assign_to_employee_id')),
    #              ('date', '=', vals.get('date_sheh'))])
    #
    #         if not search_date:
    #             record = self.env['employee.capacity'].create({
    #                 'employee_id': vals.get('re_assign_to_employee_id'),
    #                 'date': vals.get('date_sheh'),
    #                 'cap_count': 1
    #             })
    #         else:
    #             for rec in search_date:
    #                 if rec.cap_count != employee_obj.capacity:
    #                     rec.cap_count += 1
    #
    #     return res

    @api.multi
    def unlink(self):
        emp_capacity_obj = self.env['employee.capacity']
        for recs in self:
            search_date = emp_capacity_obj.search(
              [('employee_id', '=', recs.re_assign_to_employee_id.id),
                 ('date', '=', recs.date_sheh)])
            if search_date:
               for rec in search_date:
                 rec.cap_count -= 1
                 if rec.cap_count==0:
                         rec.unlink()
        return super(Repaire, self).unlink()


    @api.multi
    def write(self,vals):
        emp_capacity_obj = self.env['employee.capacity']
        employee_obj = self.env['hr.employee']


        if vals.get('date_sheh') == False  and not vals.get('re_assign_to_employee_id'):

                    for recs in self:
                        search_date = emp_capacity_obj.search(
                            [('employee_id', '=', recs.re_assign_to_employee_id.id),
                             ('date', '=', recs.date_sheh)])
                        print('jjjjjjj', search_date)
                        if search_date:
                            for rec in search_date:
                                rec.cap_count -= 1
                                if rec.cap_count == 0:
                                    rec.unlink()


        if vals.get('date_sheh') and not vals.get('re_assign_to_employee_id'):

            for recs in self:
                search_date = emp_capacity_obj.search(
                    [('employee_id', '=', recs.re_assign_to_employee_id.id),
                     ('date', '=', recs.date_sheh)])

                if search_date:
                    for rec in search_date:
                        rec.cap_count -= 1
                        if rec.cap_count == 0:
                            rec.unlink()

                if recs.re_assign_to_employee_id != False and  vals.get('date_sheh') != False:
                    search_date = emp_capacity_obj.search(
                        [('employee_id', '=', recs.re_assign_to_employee_id.id),
                         ('date', '=', vals.get('date_sheh'))])

                    if search_date:
                        for rec in search_date:
                            if rec.cap_count != employee_obj.capacity:
                                cap = rec.cap_count + 1
                                rec.write({'cap_count': cap})
                    else:
                        emp_capacity_obj.create({
                            'employee_id': recs.re_assign_to_employee_id.id,
                            'date': vals.get('date_sheh'),
                            'cap_count': 1
                        })

        if vals.get('date_sheh') == False  and vals.get('re_assign_to_employee_id'):

                    for recs in self:
                        search_date = emp_capacity_obj.search(
                            [('employee_id', '=', recs.re_assign_to_employee_id.id),
                             ('date', '=', recs.date_sheh)])
                        print('jjjjjjj', search_date)
                        if search_date:
                            for rec in search_date:
                                rec.cap_count -= 1
                                if rec.cap_count == 0:
                                    rec.unlink()

                        if  vals.get('re_assign_to_employee_id') != False and recs.date_sheh !=False and vals.get('date_sheh') !=False :
                            search_date = emp_capacity_obj.search(
                                [('employee_id', '=', vals.get('re_assign_to_employee_id')),
                                 ('date', '=', recs.date_sheh)])

                            if search_date:
                                for rec in search_date:
                                    if rec.cap_count != employee_obj.capacity:
                                        cap = rec.cap_count + 1
                                        rec.write({'cap_count': cap})
                            else:
                                emp_capacity_obj.create({
                                    'employee_id': vals.get('re_assign_to_employee_id'),
                                    'date': recs.date_sheh,
                                    'cap_count': 1
                                })

        if vals.get('date_sheh')  and vals.get('re_assign_to_employee_id'):
                            for recs in self:
                                search_date = emp_capacity_obj.search(
                                    [('employee_id', '=', recs.re_assign_to_employee_id.id),
                                     ('date', '=', recs.date_sheh)])
                                if search_date:
                                    for rec in search_date:
                                        rec.cap_count -= 1
                                        if rec.cap_count == 0:
                                            rec.unlink()

                                if vals.get('re_assign_to_employee_id') != False and vals.get('date_sheh') != False:
                                    search_date = emp_capacity_obj.search(
                                        [('employee_id', '=', vals.get('re_assign_to_employee_id')),
                                         ('date', '=',  vals.get('date_sheh'))])

                                    if search_date:
                                        for rec in search_date:
                                            if rec.cap_count != employee_obj.capacity:
                                                cap = rec.cap_count + 1
                                                rec.write({'cap_count': cap})
                                    else:
                                        emp_capacity_obj.create({
                                            'employee_id': vals.get('re_assign_to_employee_id'),
                                            'date': vals.get('date_sheh'),
                                            'cap_count': 1
                                        })

        # if vals.get('re_assign_to_employee_id'):
        #
        #     for recs in self:
        #
        #         search_date = emp_capacity_obj.search(
        #             [('employee_id', '=', recs.re_assign_to_employee_id.id),
        #              ('date', '=', recs.date_sheh)])
        #         if search_date:
        #             for rec in search_date:
        #                 rec.cap_count -= 1
        #                 if rec.cap_count == 0:
        #                     rec.unlink()
        #
        #         search_date = emp_capacity_obj.search(
        #             [('employee_id', '=', vals.get('re_assign_to_employee_id')),
        #              ('date', '=', recs.date_sheh)])
        #         if search_date:
        #             for rec in search_date:
        #                 rec.cap_count -= 1
        #                 if rec.cap_count == 0:
        #                     rec.unlink()
        #
        # if vals.get('date_sheh') and not vals.get('re_assign_to_employee_id')  or vals.get('date_sheh')==False and not vals.get('re_assign_to_employee_id'):
        #
        #     for recs in self:
        #         search_date = emp_capacity_obj.search(
        #             [('employee_id', '=', recs.re_assign_to_employee_id.id),
        #              ('date', '=', recs.date_sheh)])
        #         if search_date:
        #             for rec in search_date:
        #                 rec.cap_count -= 1
        #                 if rec.cap_count == 0:
        #                     rec.unlink()
        return super(Repaire, self).write(vals)

    @api.model
    def create(self, vals):
        res=super(Repaire, self).create(vals)
        emp_capacity_obj = self.env['employee.capacity']
        print (res)
        for rec in res:
            if rec.re_assign_to_employee_id and rec.date_sheh :
                employee_obj = self.env['hr.employee'].browse(rec.re_assign_to_employee_id.id)
                print(employee_obj.capacity)
                search_date = emp_capacity_obj.search(
                    [('employee_id', '=', rec.re_assign_to_employee_id.id),
                     ('date', '=', rec.date_sheh)])
                print(search_date)
                if not search_date:
                    print(employee_obj.capacity)
                    record = self.env['employee.capacity'].create({
                        'employee_id': rec.re_assign_to_employee_id.id,
                        'date': rec.date_sheh,
                        'cap_count': 1
                    })
                else:
                    for rec in search_date:
                        if rec.cap_count != employee_obj.capacity:
                            cap = rec.cap_count + 1
                            rec.write({'cap_count': cap})

        return res



    # @api.onchange('re_assign_to_employee_id', 'date_sheh')
    # def emp_capacity(self):
    #     for rec in self:
    #         emp_capacity_obj = self.env['employee.capacity']
    #
    #         if rec.re_assign_to_employee_id and rec.date_sheh and rec.date_sheh != False:
    #             employee_obj = self.env['hr.employee'].browse(rec.re_assign_to_employee_id.id)
    #             print(employee_obj.capacity)
    #             search_date = emp_capacity_obj.search(
    #                 [('employee_id', '=', rec.re_assign_to_employee_id.id),
    #                  ('date', '=', rec.date_sheh)])
    #             print(search_date)
    #             if not search_date:
    #                 print(employee_obj.capacity)
    #                 record = self.env['employee.capacity'].create({
    #                     'employee_id': rec.re_assign_to_employee_id.id,
    #                     'date': rec.date_sheh,
    #                     'cap_count': 1
    #                 })
    #             else:
    #                 for rec in search_date:
    #                     print(rec.cap_count,employee_obj.capacity)
    #                     if rec.cap_count != employee_obj.capacity:
    #                         cap=rec.cap_count+1
    #                         rec.write({'cap_count':cap})
    #

    @api.onchange('re_assign_to_employee_id')
    def emp_capacity2(self):
        res = {}
        lis2 = []
        for rec in self:
                rec.date_sheh = False
                if rec.re_assign_to_employee_id:
                    se_ser = self.env['hr.employee'].search([('service_center', '=', rec.re_assign_to_employee_id.id)])
                    for i in se_ser:
                        lis2.append(i.id)
                    domain = [('id', 'in', lis2)]
                    res['domain'] = {'technition': domain}
        return res




    @api.multi
    def action_repair_cancel(self):
        
           emp_capacity_obj = self.env['employee.capacity']
           for recs in self:
               search_date = emp_capacity_obj.search(
                   [('employee_id', '=', recs.re_assign_to_employee_id.id),
                    ('date', '=', recs.date_sheh)])
               for rec in search_date:
                   if recs.state == 'draft':
                       print ('jjjj')
                       if rec.cap_count != 0:
                           rec.cap_count -= 1
                           if rec.cap_count == 0:
                               rec.unlink()

           
           return super(Repaire, self).action_repair_cancel()

    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    




class EmployeeCapacity(models.Model):
    _name = 'employee.capacity'


    employee_id=fields.Many2one('hr.employee',string='Employee')
    date=fields.Date('Date')
    cap_count=fields.Integer('Count')








