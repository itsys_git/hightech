# -*- coding: utf-8 -*-
{
    'name': "Escalation",

    'summary': """
        add escalation in repair""",

    'description': """
        add escalation in repair
    """,

    'author': "ITsys-Corportion Eman ahmed",
    'website': "http://www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','repair'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/repair_order.xml',
        'views/holdays.xml',
    ],
    # only loaded in demonstration mode

}