# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class RepairOrder(models.Model):
    _inherit ='repair.order'
    escalation_ids=fields.One2many('escalation.line','repair_id')




class EscalationLine(models.Model):
    _name='escalation.line'
    escalation=fields.Char()
    replay=fields.Char()
    respons_id=fields.Many2one('hr.employee','Response')
    date=fields.Date()
    repair_id=fields.Many2one('repair.order')









