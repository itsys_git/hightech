from odoo import models, fields, api

class HoldaysEmployee(models.Model):
    _name='holdays.employee'
    _rec_name='employee_id'
    employee_id=fields.Many2one('hr.employee','Employee')
    date=fields.Date()

