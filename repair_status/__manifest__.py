# -*- coding: utf-8 -*-
{
    'name': "Repair Status",

    'summary': """
        Repair_Status """,

    'description': """
        Repair_Status
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','repair'],

    'data': [
        'security/ir.model.access.csv',
        'views/repairs_order_view.xml',
        'views/repairs_status_view.xml',

    ],

}