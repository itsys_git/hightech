from odoo import models, fields, api
from odoo.exceptions import ValidationError

class RepairsStatus(models.Model):
    _name ='repairs.status'
    _rec_name ='repairs_status'
    repairs_status = fields.Char(string='Repairs Status')
    ivr = fields.Integer(String="IVR")

    @api.multi
    def unlink(self):

        for rec in self:
            repair_order_obj = self.env['repair.order'].search([('repairs_status_id', '=', rec.id)])
            print (repair_order_obj)
            if repair_order_obj:
                for order in repair_order_obj:
                    if order.state  in ('confirmed','under_repair','ready'):
                        raise ValidationError("You can't delete this repair status because it is used in some repair orders ")
        return super(RepairsStatus, self).unlink()