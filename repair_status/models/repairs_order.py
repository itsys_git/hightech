from odoo import models, fields, api
from odoo.exceptions import ValidationError

class RepairOrder(models.Model):
    _inherit ='repair.order'

    @api.multi
    def _get_repair_status_default(self):
        search_id = self.env['repairs.status'].search([('repairs_status', '=', 'In Planning')]).id
        if search_id:
            return search_id
        else:
            create_id = self.env['repairs.status'].create({'repairs_status': 'In Planning'}).id
            return create_id


    repairs_status_id = fields.Many2one('repairs.status', default=_get_repair_status_default,string='Repairs Status',track_visibility='onchange')
    ivr = fields.Integer(related='repairs_status_id.ivr',String="IVR",track_visibility='onchange')
