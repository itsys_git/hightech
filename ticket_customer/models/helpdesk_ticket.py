from odoo import models, fields, api
class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'
    customer_address=fields.Many2one('address.customer')
    repairs_ids=fields.Many2many('repair.order','r1','r2')
    warranty_type=fields.Many2one('warranty.type')

    my_domain1 = fields.Many2many(comodel_name='address.customer', string='Domain', relation='address_customer_ress_ticket',
                                 column1='ticket_id', column2='customer_id', compute='_get_domain_str')


    # my_domain2= fields.Many2many(comodel_name='product.product', string='Domain',
    #                               relation='product_product_ress_ticket',
    #                               column1='ticket_id1', column2='customer_id1', compute='_get_domain_str2')
    #
    # @api.one
    # def _get_domain_str2(self):
    #
    #     for item in self:
    #         res = {}
    #         # rec={}
    #         ls = ''
    #
    #         domain = []
    #         list = []
    #         l_final = []
    #         search_lot = self.env['stock.production.lot'].search([('partner_id', '=', item.partner_id.id)])
    #         if search_lot:
    #             for line in search_lot:
    #                 if line.product_id.spare_parts == False:
    #                     print (line)
    #                     list.append(line.product_id.id)
    #
    #         for l in list_add:
    #             se_address = self.env['address.customer'].search([('partner_id', '=', l)])
    #             l_final.append(se_address.id)
    #
    #         domain1 = [('id', 'in', l_final)]
    #
    #         if list:
    #             domain = [('id', 'in', list)]
    #         else:
    #             lsl = []
    #             search_lot = self.env['product.product'].search([('spare_parts', '=', False)])
    #             for l in search_lot:
    #                 lsl.append(l.id)
    #             domain = [('id', 'in', lsl)]
    #
    #
    #
    #
    #
    #         if domain:
    #             item.my_domain2 = domain[0][2]
    #         else:
    #             item.my_domain2 = self.env['product.product'].sudo().search([]).ids
    #

    @api.one
    def _get_domain_str(self):

        for item in self:
            res = {}
            # rec={}
            ls = ''
            domain1 = []
            street = item.partner_id.street
            street2 = item.partner_id.street2
            city = item.partner_id.city
            state = item.partner_id.state_id.name
            country = item.partner_id.country_id.name
            if item.partner_id:
                item.partner_name = item.partner_id.name
                item.partner_email =item.partner_id.email
                if street == False:
                    street = ''
                if street2 == False:
                    street2 = ''
                if city == False:
                    city = ''

                if state == False:
                    state = ''
                if country == False:
                    country = ''
                customer_address = str(item.partner_id.name) + ':' + str(country) + ' ' + str(state) + ' ' + str(
                    city) + ' ' + str(street) + ' ' + str(street2)

                print (customer_address)
                se_address = self.env['address.customer'].search([('partner_id', '=', item.partner_id.id)])
                if se_address:
                    print ('iiiiiiiiiii')
                    se_address.write({'name': customer_address})
                else:
                    self.env['address.customer'].create({'name': customer_address, 'partner_id': item.partner_id.id})

                list_add = []
                for line in item.partner_id:
                    list_add.append(line.id)
                if item.partner_id.child_ids:
                    for child in item.partner_id.child_ids:
                        list_add.append(child.id)
                        street = child.street
                        street2 = child.street2
                        city = child.city
                        state = child.state_id.name
                        country = child.country_id.name
                        if street == False:
                            street = ''
                        if street2 == False:
                            street2 = ''
                        if city == False:
                            city = ''

                        if state == False:
                            state = ''
                        if country == False:
                            country = ''
                        customer_address = str(child.name) + ':' + str(country) + ' ' + str(state) + ' ' + str(
                            city) + ' ' + str(
                            street) + ' ' + str(street2)
                        se_address = self.env['address.customer'].search([('partner_id', '=', child.id)])
                        if se_address:
                            se_address.write({'name': customer_address})
                        else:
                            self.env['address.customer'].create({'name': customer_address, 'partner_id': child.id})

                domain = []
                list = []
                l_final = []


                for l in list_add:
                    se_address = self.env['address.customer'].search([('partner_id', '=', l)])
                    l_final.append(se_address.id)

                domain1 = [('id', 'in', l_final)]


            if domain1:
                item.my_domain1 = domain1[0][2]
            else:
                item.my_domain1 = self.env['address.customer'].sudo().search([]).ids

    @api.model
    def create(self, vals):
        res=super(HelpdeskTicket, self).create(vals)
        search_customer = self.env['res.partner'].search([('id', '=', res.partner_id.id)])
        se_l = self.search([('partner_id', '=', search_customer.id)])
        if se_l:
            for line in se_l:
                search_customer.write({'ticket_ids': [(4, line.id)]})
        return res

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        res={}
        # rec={}
        ls=''
        street=self.partner_id.street
        street2=self.partner_id.street2
        city=self.partner_id.city
        state=self.partner_id.state_id.name
        country=self.partner_id.country_id.name
        if self.partner_id:
            self.partner_name = self.partner_id.name
            self.partner_email = self.partner_id.email
            if street==False:
                street = ''
            if  street2 == False :
                street2 = ''
            if city == False:
                city= ''

            if state == False:
                state = ''
            if country==False:
                country=''
            customer_address=str(self.partner_id.name)+':'+str(country)+' '+str(state)+' '+str(city)+' '+str(street)+' '+str(street2)

            print (customer_address)
            se_address=self.env['address.customer'].search([('partner_id','=',self.partner_id.id)])
            if se_address:
                print ('iiiiiiiiiii')
                se_address.write({'name': customer_address})
            else:
                self.env['address.customer'].create({'name':customer_address,'partner_id':self.partner_id.id})

            list_add = []
            for line in self.partner_id:
                list_add.append(line.id)
            if self.partner_id.child_ids:
                for child in self.partner_id.child_ids:
                    list_add.append(child.id)
                    street =child.street
                    street2 = child.street2
                    city = child.city
                    state = child.state_id.name
                    country = child.country_id.name
                    if street == False:
                        street = ''
                    if street2 == False:
                        street2 = ''
                    if city == False:
                        city = ''

                    if state == False:
                        state = ''
                    if country == False:
                        country = ''
                    customer_address = str(child.name)+':'+str(country) + ' ' + str(state) + ' ' + str(city) + ' ' + str(
                        street) + ' ' + str(street2)
                    se_address = self.env['address.customer'].search([('partner_id', '=',child.id)])
                    if se_address:
                        se_address.write({'name': customer_address})
                    else:
                        self.env['address.customer'].create({'name': customer_address, 'partner_id':child.id})

            domain=[]
            domain1=[]
            list=[]
            l_final=[]
            search_lot=self.env['stock.production.lot'].search([('partner_id','=',self.partner_id.id)])
            # print (search_lot.product_id)
            if search_lot:
                for line in search_lot:
                    if line.product_id.spare_parts ==False:
                        print (line)
                        list.append(line.product_id.id)

            for l in list_add:
               se_address=self.env['address.customer'].search([('partner_id','=',l)])
               l_final.append(se_address.id)

            domain1 = [('id', 'in', l_final)]

            if list :
                domain = [('id', 'in', list)]
            else:
                lsl=[]
                search_lot = self.env['product.product'].search([('spare_parts','=',False)])
                for l in search_lot:
                    lsl.append(l.id)
                domain = [('id', 'in',lsl)]

            res['domain'] = {'product_id': domain, 'customer_address': domain1}

            se_address_default=self.env['address.customer'].search([('partner_id','=',self.partner_id.id)])

            self.customer_address=se_address_default






        return res


class RepairOrder(models.Model):
    _inherit ='repair.order'
    helpdesk_id=fields.Many2one('helpdesk.ticket')
    res_partner_id=fields.Many2one('res.partner')
    lot_id = fields.Many2one(
        'stock.production.lot', 'Lot/Serial',
        domain="[('product_id','=', product_id),('partner_id','=',partner_id)]",
        help="Products repaired are all belonging to this lot", oldname="prodlot_id")

    @api.onchange('lot_id')
    def on_change_lot_repair(self):
        res = {}
        lis = []
        lis2 = []
        if self.lot_id:
            se_product = self.env['product.product'].search([('id', '=', self.lot_id.product_id.id)])
            se_customer = self.env['res.partner'].search([('id', '=', self.lot_id.partner_id.id)])
            for i in se_product:
                lis.append(i.id)
            for i in se_customer:
                lis2.append(i.id)

            domain1 = [('id', 'in', lis)]
            domain2 = [('id', 'in', lis2)]
            res['domain'] = {'product_id': domain1, 'partner_id': domain2}
        else:
            se_product = self.env['product.product'].search([('spare_parts', '=', False)])
            se_customer = self.env['res.partner'].search([])
            for i in se_product:
                lis.append(i.id)
            for i in se_customer:
                lis2.append(i.id)

            domain1 = [('id', 'in', lis)]
            domain2 = [('id', 'in', lis2)]
            res['domain'] = {'product_id': domain1, 'partner_id': domain2}
        return res
    @api.model
    def create(self,vals):
        res=super(RepairOrder, self).create(vals)
        search_tickets=self.env['helpdesk.ticket'].search([('id','=',res.helpdesk_ticket_id.id)])
        search_customer=self.env['res.partner'].search([('id','=',res.partner_id.id)])
        se_l = self.search([('partner_id', '=', search_customer.id)])
        se_t=self.search([('helpdesk_ticket_id', '=', search_tickets.id)])

        if se_t:
            for line in se_t:
                search_tickets.write({'repairs_ids':[(4,line.id)]})
        if se_l:
            for line in se_l:
                search_customer.write({'repairs_ids': [(4, line.id)]})

        return res



class AddressCustomer(models.Model):
    _name='address.customer'
    name=fields.Char()
    partner_id=fields.Many2one('res.partner')
