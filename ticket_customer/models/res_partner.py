from odoo import models, fields, api
class ResPartner(models.Model):
    _inherit ='res.partner'
    repair_order_count = fields.Integer(compute='_compute_repair_order_count', string='repair Order Count')
    lot_serial_count=fields.Integer(compute='_compute_lot_serial_count', string='Product List')
    repairs_ids = fields.Many2many('repair.order','recu1','recu2')
    lot_ids=fields.Many2many('stock.production.lot', 'l1', 'l2')
    ticket_ids=fields.Many2many('helpdesk.ticket','t1','t2')


    lot_count=fields.Integer(compute='calc_lot', string='Product')

    def _compute_repair_order_count(self):
        repair_data = self.env['repair.order'].read_group(
            [('partner_id', 'in', self.ids)],
            ['partner_id'], ['partner_id'])
        result = dict((data['partner_id'][0], data['partner_id_count']) for data in repair_data)
        for ticket in self:
            ticket.repair_order_count = result.get(ticket.id, 0)


    def _compute_lot_serial_count(self):
        lot_data = self.env['stock.production.lot'].read_group(
            [('partner_id', 'in', self.ids)],
            ['partner_id'], ['partner_id'])
        result = dict((data['partner_id'][0], data['partner_id_count']) for data in lot_data)
        for lot in self:
            lot.lot_serial_count = result.get(lot.id, 0)






    def compute_lot_count(self):
        domain=[]
        lot_obj=self.env['stock.production.lot'].search(['|',('partner_id','=',self.id),('partner_id','=',False)])
        for rec in lot_obj:
            domain.append(rec.id)
        return {
            'domain': [('id', 'in', domain)],
            'name': 'Product',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.production.lot',
            'view_id': False,
            'views': [(self.env.ref('stock.view_production_lot_tree').id, 'tree'),
                      (self.env.ref('stock.view_production_lot_form').id, 'form')],
            'type': 'ir.actions.act_window'
        }


    def calc_lot(self):
        lot_data = self.env['stock.production.lot'].read_group(
            [('partner_id', 'in', self.ids)],
            ['partner_id'], ['partner_id'])
        result = dict((data['partner_id'][0], data['partner_id_count']) for data in lot_data)
        for lot in self:
            lot.lot_count = result.get(lot.id, 0)










