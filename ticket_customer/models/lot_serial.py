from odoo import models, fields, api
from odoo.exceptions import ValidationError
class StockProductionLot(models.Model):
    _inherit ='stock.production.lot'
    name = fields.Char(
        'Lot/Serial Number', default=lambda self: self.env['ir.sequence'].next_by_code('stock.lot.serial'),
        required=False, help="Unique Lot/Serial Number")

    partner_id=fields.Many2one('res.partner','Customer')
    type_id=fields.Many2one('type.lot.serial','Lot/Serial Type')
    product_family=fields.Many2one('product.category')
    product_class_id=fields.Many2one('product.class.lot.serial','Product Class')
    product_capacity=fields.Integer()
    product_model_id=fields.Many2one('product.model.lot.serial','Product Model')
    description=fields.Text()
    dealer_id=fields.Many2one('res.partner','Dealer')
    serial_dealer_name=fields.Char("Dealers Number")
    issue_date=fields.Date()
    installation_date=fields.Date()
    buying_date=fields.Date()
    # warranty_type_id=fields.Many2one('warranty.type.lot.serial','Warranty Type')
    end_of_life_date=fields.Date()
    best_before_date=fields.Date()
    alert_date=fields.Date()
    removal_date=fields.Date()
    inpound=fields.Many2one('in.pound')
    outpound = fields.Many2one('out.pound')

    check=fields.Boolean(compute='check_visible')

    in_door=fields.Boolean('IN Door')
    out_door=fields.Boolean("OUT Door")
    @api.constrains('product_capacity')
    def not_minus(self):
        for rec in self:
            if rec.product_capacity < 0:
                raise ValidationError('Capacity is not minus')

    @api.model
    def create(self, vals):
        res=super(StockProductionLot, self).create(vals)
        search_customer = self.env['res.partner'].search([('id', '=', res.partner_id.id)])
        se_l=self.search([('partner_id','=',search_customer.id)])
        if se_l:
            for line in se_l:
                search_customer.write({'lot_ids': [(4,line.id)]})

        return res



    @api.onchange('in_door','out_door','product_id')
    def check_door(self):
        if self.product_id:
            self.product_id.write({'indoor': self.in_door})
            self.product_id.write({'outdoor': self.out_door})

            # if self.product_id.categ_id.name=='AC' or self.product_id.categ_id.parent_idname=='AC':
            #     self.check=True
            # else:
            #     self.check = False

    @api.depends('product_id')
    def check_visible(self):
        if self.product_id:
            if self.product_id.categ_id.name == 'AC' or self.product_id.categ_id.parent_id.name == 'AC':
                self.check = True
            else:
                self.check = False


class TypeLotSeria(models.Model):
    _name='type.lot.serial'
    name=fields.Char()

class ProductClassLotSerial(models.Model):
    _name='product.class.lot.serial'
    name=fields.Char()

class ProductModelLotSerial(models.Model):
    _name='product.model.lot.serial'
    name=fields.Char()

class WarrantyTypeLotSerial(models.Model):
    _name='warranty.type.lot.serial'
    name=fields.Char(required=1)

class InPound(models.Model):
    _name='in.pound'
    name=fields.Char()

class OutPound(models.Model):
    _name='out.pound'
    name=fields.Char()



class Productproduct(models.Model):
    _inherit = 'product.product'

    indoor=fields.Boolean("IN Door")
    outdoor=fields.Boolean("Out Door")

class Productemplate(models.Model):
    _inherit = 'product.template'

    indoor=fields.Boolean("IN Door")
    outdoor=fields.Boolean("Out Door")




