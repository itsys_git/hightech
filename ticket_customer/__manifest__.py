# -*- coding: utf-8 -*-
{
    'name': "Ticket Customer Repair",

    'summary': """
        appear button ticket in customer""",

    'description': """
       appear button ticket in customer
    """,

    'author': "ITsys-Corportion Eman ahmed",
    'website': "http://www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','repair_status','stock','repair_employee_ticket','hr_employee_edit','payment','account','account_reports','purchase','sms','project','employee_type'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/groups_security.xml',
        'views/res_partner.xml',
        'views/helpdesk_ticket.xml',
        'views/lot_serial.xml',
        'views/repair_order.xml',
    ],
    # only loaded in demonstration mode

}