# -*- coding: utf-8 -*-
{
    'name': "Stock Location",

    'summary': """
        retrive customer in location""",

    'description': """
        retrive customer in location
    """,

    'author': "ITsys-Corportion Eman ahmed",
    'website': "http://www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/stock_location.xml',
    ],
    # only loaded in demonstration mode

}