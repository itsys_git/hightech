# -*- coding: utf-8 -*-
{
    'name': "repair order report ",

    'summary': """
    نموذج بلاغ صيانه(تقرير اختار الفنى)
        """,

    'author': "Marwa Ahmed",
    'website': "http://www.yourcompany.com",

    'depends': ['base','repair','contact_squence_no'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],

}