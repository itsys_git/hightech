# -*- coding: utf-8 -*-
from odoo.exceptions import ValidationError
from openerp import fields, models, api


class RepairOrder(models.Model):
    _inherit = 'repair.order'

    @api.multi
    def write(self,vals):
        res=super(RepairOrder,self).write(vals)
        if vals.get('product_id') or vals.get('lot_id') or not vals.get('lot_id') :
            for each in self:
                ticket_ids=self.env['helpdesk.ticket'].search([('id','=',each.helpdesk_ticket_id.id)])
                for item in ticket_ids:
                    item.write({'product_id': each.product_id.id,'lot_serial_id':each.lot_id and each.lot_id.id or False,'category':each.product_id.categ_id.id})
                    other_repair_orders=item.repairs_ids.filtered(lambda record:record.id!=each.id and record.product_id.id!=vals.get('product_id') or  record.lot_id.id!=vals.get('lot_id') or False)
                    for repair in other_repair_orders:
                        repair.write({'lot_id': each.lot_id and each.lot_id.id or False, 'product_id': each.product_id.id})
        return res
    @api.multi
    def create(self,vals):
        res=super(RepairOrder,self).create(vals)
        for rec in res:
            if vals.get('product_id') or vals.get('lot_id') or not vals.get('lot_id'):
                ticket_ids=self.env['helpdesk.ticket'].search([('id','=',rec.helpdesk_ticket_id.id)])
                for item in ticket_ids:
                    item.write({'product_id': rec.product_id.id,'lot_serial_id':rec.lot_id.id,'category':rec.product_id.categ_id.id})
                    other_repair_orders=item.repairs_ids.filtered(lambda record:record.id!=rec.id and record.product_id.id!=vals.get('product_id') or record.lot_id.id!=vals.get('lot_id') or False)
                    for repair in other_repair_orders:
                        repair.write({'lot_id': rec.lot_id.id, 'product_id': rec.product_id.id})
        return res

