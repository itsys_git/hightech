# -*- coding: utf-8 -*-
{
    'name': "Approve on replace air conditioning",


    'author': "Marwa Ahmed",


    'category': 'Uncategorized',
    'version': '0.1',

     'depends': ['base', 'repair','helpdesk','report_approve_on_replace'],
    # always loaded
    'data': [
        'views/views.xml',
        'views/templates.xml',
    ],
    # # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}