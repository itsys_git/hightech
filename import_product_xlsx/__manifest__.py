##############################################################################

{
    'name': 'import Products from xlxs',
    'version': '11.0',
    'author': 'Said YAHIA)',
    'license': 'AGPL-3',
    'category': 'Stock',
    'summary': 'Import Products from Excel File',
    'depends': ['base',
                'stock',
                'repair_configration','ivr_config'
                ],
    'data': [
        'security/security.xml',

        'views/import_products.xml',
        'security/ir.model.access.csv',

    ],
    'installable': True,
    'application': True,
    'demo': [],
    'test': []
}
