# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import fields, models, _, api
import base64
import datetime
from xlrd import open_workbook


from odoo import SUPERUSER_ID
import logging
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)
class product_master(models.Model):
    _name = "product.master"

    name = fields.Char(string='Name')


class product_audit(models.Model):
    _name = "product.audit"

    name = fields.Char(string='Name')


class Productemplate(models.Model):
    _inherit = 'product.template'
    product_master_id = fields.Many2one('product.master', 'Product Master')
    product_audit_id = fields.Many2one('product.audit', 'Product Audit')


class import_product(models.TransientModel):
    _name = "import.product"

    file_data = fields.Binary(string='Chose File', required=True, )
    file_name = fields.Char(string='File Name')

    def import_button(self):
        product_obj = self.env['product.product']
        xlsx = open_workbook(file_contents=base64.decodestring(self.file_data))
        for s in xlsx.sheets():
            archive_lines = []
            lcount = 0
            for row in range(s.nrows):
                col_value = []
                count = -1
                lcount += 1
                for col in range(s.ncols):
                    value = (s.cell(row, col).value)
                    try:
                        count += 1
                        if count == 9 :
                            value = str(float(value))
                        else:
                        #     value = str(int(value))
                            value = str(int(value))

                            # print((value))
                            value = str(int(value))

                    except:
                        pass
                    col_value.append(value)
                archive_lines.append(col_value)
                _logger.debug('append data', col_value)
                # _logger.info('append data', col_value)

                print('append data', col_value)
        print('archive_lines',archive_lines)
        # self.valid_columns(archive_lines)
        self.valid_columns_serial(archive_lines)

        archive_lines.pop(0)
        # self.valid_product_code(archive_lines, product_obj)
        self.valid_product_code_serial(archive_lines, product_obj)

    @api.model
    def valid_product_code_serial(self, archive_lines, product_obj):
        cont = 0
        if len(archive_lines) > 0:
            for line in archive_lines:
                print(line)
                _logger.debug('try insert line', line,str(line[0]))
                # _logger.info('try insert line', line)

                print('try insert line', line,str(line[0]))

                cont = cont + 1
                prod = False

                prod = product_obj.sudo().search([('id', '=', str(line[0]))], limit=1)
                print('prod', prod)

                if prod:
                    self.env['stock.production.lot'].sudo().create({
                        'product_id': prod.id,
                        'name': str(line[1]),
                        'warranty_active_date': str(line[2]),
                        # 'product_capacity': 1,
                        'serial_dealer_id': line[3] or False,

                    })
                _logger.debug('success insert line', line)
                # _logger.info('success insert line', line)

                print('success insert line', line)
                # raise ValidationError(
                #     _(' %s Rows Update or inserted finished.') % str(cont))

    @api.model
    def valid_product_code(self, archive_lines, product_obj):
        cont = 0
        if len(archive_lines) > 0:
            for line in archive_lines:
                print(line)
                # _logger.debug('try insert line', line)
                # _logger.info('try insert line', line)

                print('try insert line', line)

                cont = cont + 1
                prod=False

                prod=product_obj.sudo().search([('default_code','=',str(line[3]))],limit=1)
                print('prod', prod)

                if prod:
                    self.env['stock.production.lot'].sudo().create({
                        'product_id':prod.id,
                        'name':str(line[10]),
                        'warranty_active_date': str(line[11]),
                        'product_capacity': 1,
                        'serial_dealer_id': line[12] or False,

                    })
                else:
                    prod2=False
                    try:
                        vals = {
                            'hi_code': line[0],
                            'man_code': line[1],

                            'name': line[2],
                            'default_code': str(line[3]),
                            'categ_id': (line[4]),
                            'uom_id': (line[5]),
                            # 'product_master_id': line[6] or False,
                            'product_audit_id': line[7] or False,
                            # 'labor_id': line[8] or False,

                            'type': 'product',
                            'rental': 'TRUE',
                            'list_price': line[9],
                            'sale_ok': 'TRUE',
                            'purchase_ok': 'TRUE',
                            'company_id': '',
                            # 'uom_po_id': line[7],
                            'create_uid': self.env.user,
                            'create_date': datetime.datetime.now(),
                            'write_uid': self.env.user,
                            'write_date': datetime.datetime.now(),
                            'sale_line_warn': 'no-message',
                            # 'standard_price': line[5],
                            'expense_policy': 'no',
                            'invoice_policy': 'order',
                            'responsible_id': '1',
                            'tracking': 'none',
                            'purchase_line_warn': 'no-message',
                            'sale_line_warn': 'no-message',
                            'split_method': 'equal'}
                        # print(vals)
                        prod2=product_obj.sudo().create(vals
                                                  )

                    except:
                        raise UserError("Found Error in line " + str(cont))
                    if prod2:
                        print('prod2', prod2, prod2.id)

                        self.env['stock.production.lot'].sudo().create({
                            'product_id': prod2.id,
                            'name': str(line[10]),
                            'warranty_active_date': str(line[11]),
                            'product_capacity':1,

                            'serial_dealer_id': line[12] or False,

                        })
                # _logger.debug('success insert line', line)
                # _logger.info('success insert line', line)

                print('success insert line', line)
        # raise ValidationError(
        #     _(' %s Rows Update or inserted finished.') % str(cont))

    @api.model
    def valid_columns(self, archive_lines):
        for columns in archive_lines:
            text2 = text = "Xlxs file must have This column [hi_code,man_code,name,default_code,categ_id,sale_" \
                           "price,quantity,product_audit_id,product_master_id,labor_id,serial,date,dealer_id] \n The following columns are not present in the file:"
            if not 'hi_code' in columns[0]: text += "\n[ hi_code ]"
            if not 'man_code' in columns[1]: text += "\n[ man_code ]"
            if not 'name' in columns[2]: text += "\n[ name ]"
            if not 'default_code' in columns[3]: text += "\n[ default_code ]"
            if not 'categ_id' in columns[4]: text += "\n[ categ_id ]"
            if not 'uom_id' in columns[5]: text += "\n[ uom_id ]"
            if not 'product_master_id' in columns[6]: text += "\n[ product_master_id ]"
            if not 'product_audit_id' in columns[7]: text += "\n[ product_audit_id ]"
            if not 'labor_id' in columns[8]: text += "\n[ labor_id ]"
            if not 'list_price' in columns[9]: text += "\n[ list_price ]"
            if not 'serial' in columns[10]: text += "\n[ serial ]"
            if not 'date' in columns[11]: text += "\n[ date ]"
            if not 'dealer_id' in columns[12]: text += "\n[ dealer_id ]"

            if text != text2: raise UserError(text)
            return True

    @api.model
    def valid_columns_serial(self, archive_lines):
        for columns in archive_lines:
            text2 = text = "Xlxs file must have This column [product_id" \
                           "serial,date,dealer_id] \n The following columns are not present in the file:"
            if not 'product_id' in columns[0]: text += "\n[ product_id ]"

            if not 'serial' in columns[1]: text += "\n[ serial ]"
            if not 'date' in columns[2]: text += "\n[ date ]"
            if not 'dealer_id' in columns[3]: text += "\n[ dealer_id ]"

            if text != text2: raise UserError(text)
            return True
