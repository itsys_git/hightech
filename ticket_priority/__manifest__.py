# -*- coding: utf-8 -*-
{
    'name': "Ticket Priority",

    'summary': """
        ticket_priority """,

    'description': """
        ticket_priority
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','helpdesk'],

    'data': [
        'views/helpdesk_ticket_view.xml',

    ],

}