# -*- coding: utf-8 -*-
{
    'name': "Car Plan Report PDF",

    'author': "doaa khaled",

    'category': 'Repair',
    'version': '0.1',
    'license': 'AGPL-3',

    'depends': ['base', 'repair','helpdesk'],

    'data': [
        'views/car_plan_pdf_report.xml',
        'views/plan_wizard_report.xml',

    ],

}