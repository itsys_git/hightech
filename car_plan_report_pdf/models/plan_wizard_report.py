# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class PlanReport(models.TransientModel):
    _name = 'plan.report'
    _description = 'plan report'

    date_from=fields.Date(required=True,string='Schedule Date')
    technician_id = fields.Many2many('hr.employee','h1','h2',string="Technician",required=True)


    @api.multi
    def test_report(self):
        [data] = self.read()
        datas = {
            'ids': [],
            'model': 'repair.order',
            'form': data
        }
        return self.env.ref('car_plan_report_pdf.plan_report').report_action([], data=datas)