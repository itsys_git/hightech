# -*- coding: utf-8 -*-
import time
from datetime import datetime, date,timedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import itertools

class Parser(models.AbstractModel):
    _name = 'report.car_plan_report_pdf.report_plan_report'


    def _get_lines(self,start_date,technician_id):
        from_date = datetime.strptime(start_date, "%Y-%m-%d").date()

        result=[]
        repair=[]
        if len(technician_id)>0:
            repair_ids = self.env['repair.order'].search([('technition', 'in', technician_id),('date_sheh','=', from_date)])
            repairs = []
            for obj in repair_ids: repairs.append(obj.id)
            repairs = self.env['repair.order'].browse(repairs)
            technition = []
            for t in repairs:
                technition.append(t.technition)
            technician_id = technition
            technician_id = list(set(technician_id))
        for person in technician_id:
            lines=[]
            record=0
            res={}
            repair_ids = self.env['repair.order'].search([('technition', '=', person.id),('date_sheh','=', from_date),('repairs_status_id.repairs_status','ilike', 'Printed')])
            repairs=[]
            for obj in repair_ids: repairs.append(obj.id)
            repairs=self.env['repair.order'].browse(repairs)
            for repair in repairs:
                line = []
                line.append(repair.date_sheh)
                line.append(repair.technition.name)
                line.append(repair.helpdesk_ticket_id.ticket_status_id.ticket_status)
                line.append(repair.helpdesk_ticket_id.ticket_type_id.name)
                line.append(repair.helpdesk_ticket_id.name)
                line.append(repair.helpdesk_ticket_id.id)
                line.append(repair.name)

                line.append(repair.repair_type_id.name)
                line.append(repair.helpdesk_ticket_id.priority_new.name)
                line.append(repair.partner_id.name)
                line.append(repair.partner_id.country_id.name)
                line.append(repair.partner_id.state_id.name)
                line.append(repair.partner_id.city_id.name)

                line.append(repair.partner_id.street)
                line.append(repair.partner_id.street2)
                line.append(repair.product_id.name)
                line.append(repair.helpdesk_ticket_id.product_issue_id)
                line.append(repair.lot_id.installation_date)
                line.append(repair.lot_id.serial_dealer_id.name)

                line.append(repair.helpdesk_ticket_id.warranty_type.name)
                line.append(repair.internal_notes)
                line.append(repair.quotation_notes)
                line.append(repair.partner_id.main_mobile_number)
                line.append(repair.partner_id.mobile_number1)
                line.append(repair.partner_id.mobile_number2)
                line.append(repair.partner_id.mobile_number3)
                line.append(repair.lot_id.name)
                line.append(repair.partner_id.door)
                line.append(repair.partner_id.apartment_number)
                line.append(repair.partner_id.special_marque)
                line.append(repair.partner_id.another_details)
                line.append(repair.helpdesk_ticket_id.customer_address.name)
                record += 1
                line.append(record)

                lines.append(line)
                record = len(lines)
            res['lines']=lines
            res['totals']=[record]
            result.append(res)
        return result

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        repair = self.env['ir.actions.report']._get_report_from_name('car_plan_report_pdf.report_plan_report')

        return {
            'doc_ids': self.ids,
            'doc_model': repair.model,
            'date_from':data['form']['date_from'],
            'technician_id':data['form']['technician_id'],
            'current_date':datetime.now().date(),
            'get_lines': self._get_lines(data['form']['date_from'],data['form']['technician_id']),
        }
