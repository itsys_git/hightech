# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime
class HrEmployee(models.Model):
    _inherit ='hr.employee'
    warranty_type=fields.Many2many('warranty.type','w1','w2')
    category=fields.Many2many('product.category','pca1','pca2')
    priority=fields.Many2many('priority.line','pp1','pp2')
    ticket_type_id = fields.Many2many('helpdesk.ticket.type','ht2','ht1')
    repair_type_id= fields.Many2many('helpdesk.repair.type','hr1','hr2')



    @api.constrains('category')
    def change_catg(self):
        lis=[]
        lis2=[]
        lisfi=[]
        for line in self.category:
            search_categ = self.env['product.category'].search([('id', '=',line.id)])
            line.write({'employee_list_ids': [(4,self.id)]})
            lis.append(search_categ.id)
        search_categ_del = self.env['product.category'].search([('employee_list_ids', '=', self.id)])

        for lin in search_categ_del:
            lis2.append(lin.id)
        lisfi=(set(lis2)-set(lis))
        for l in lisfi:
            search_categ_del = self.env['product.category'].search([('id', '=',l)])
            search_categ_del.write({'employee_list_ids':[(3,self.id)]})




class PriorityLine(models.Model):
    _name='priority.line'
    name=fields.Char(string='priority',required=1)

class RepairType(models.Model):
    _name='helpdesk.repair.type'

    name=fields.Char(required=1)
    desc=fields.Char('Description')
    repair_attr1_id=fields.Many2one('attribute1.attribute1',string='Repair Attribute1')
    repair_attr2_id=fields.Many2one('attribute2.attribute2',string='Repair Attribute2')

class WarrantyType(models.Model):
    _name='warranty.type'
    name=fields.Char(required=1)
class Attribute1(models.Model):
    _name = 'attribute1.attribute1'
    _rec_name = 'name'

    name = fields.Char(required=1)

class Attribute2(models.Model):
    _name = 'attribute2.attribute2'
    _rec_name = 'name'

    name = fields.Char(required=1)


