# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime


class RepairOrder(models.Model):
    _inherit ='repair.order'
    technition=fields.Many2one('hr.employee')

    @api.multi
    def _get_repair_type_default(self):
        search_id = self.env['helpdesk.repair.type'].search([('name', '=', 'Filed Visit')]).id
        if search_id:
            return search_id
        else:
            create_id = self.env['helpdesk.repair.type'].create({'name': 'Filed Visit'}).id
            return create_id

    repair_type_id = fields.Many2one('helpdesk.repair.type', string="Repair Type",default=_get_repair_type_default,track_visibility='onchange')


class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'
    priority_new=fields.Many2one('priority.line','Priority')
    product_id = fields.Many2one('product.product', string='Product')
    category = fields.Many2one('product.category')



    @api.onchange('product_id')
    def on_change_product(self):
        if self.product_id:
            self.category=self.product_id.categ_id

    @api.onchange('category')
    def on_change_category(self):
        res={}
        if self.category:
                lsl=[]
                search_lot = self.env['product.product'].search([('categ_id','=',self.category.id),('spare_parts','=',False)])
                for l in search_lot:
                    lsl.append(l.id)
                domain = [('id', 'in',lsl)]

                res['domain'] = {'product_id': domain}

        return res








