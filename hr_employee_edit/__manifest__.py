# -*- coding: utf-8 -*-
{
    'name': "Hr Employee Edit2",

    'description': """
      add field in hr employee
    """,

    'author': "IT Systems Corportion Eman Ahmed",
    'website': "http://www.it-syscorp.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','repair','employee_type','product_category','helpdesk','repair_status','employee_type'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/helpdesk_ticket.xml',
        'views/hr_employee.xml',
        'views/repair_order.xml',
        'views/attributes_views.xml',

        
    ],

}
