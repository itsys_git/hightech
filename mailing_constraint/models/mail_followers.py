# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import fields, models, _, api
import base64
import datetime
from xlrd import open_workbook


from odoo import SUPERUSER_ID
import logging
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

class Followers(models.Model):
    _inherit = 'mail.followers'


    _sql_constraints = [
        ('mail_followers_res_partner_res_model_id_uniq', 'unique(res_model,res_id,partner_id)',
         'Error, a partner cannot follow twice the same object.'),

    ]


# ('mail_followers_res_partner_res_model_id_uniq', 'CHECK (1=1)', 'Error, a partner cannot follow twice the same object.'),
