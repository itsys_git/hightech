from odoo import models, fields, api
from odoo.exceptions import ValidationError
from openerp.tools.translate import _



class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_ids = fields.Many2many('product.template','p1','p2',domain="[('id','!=',id),('spare_parts','=',True)]")
    product_part_id =fields.Many2one('product.template','product')
    spare_parts=fields.Boolean('Spare Parts')

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        for l in res:
            if l.product_ids:
                for line in l.product_ids:
                    if line.spare_parts == False:
                        raise ValidationError("can not save because select in spare parts false please delete"+' '+ _(line.name))

        return res

    @api.multi
    def write(self, vals):
        result = super(ProductTemplate, self).write(vals)
        for l in self:
            if l.product_ids:
                for line in l.product_ids:
                    if line.spare_parts == False:
                        raise ValidationError("can not save because select in spare parts false please delete"+' '+ _(line.name))

        return result


class Productproduct(models.Model):
    _inherit = 'product.product'

    # product_ids = fields.Many2many('product.product','p8','p9',domain="[('id','!=',id),('spare_parts','=',True)]")
    product_part_id =fields.Many2one('product.product','product')
    # spare_parts = fields.Boolean('Spare Parts')

