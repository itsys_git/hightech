from odoo import models, fields, api
from odoo.exceptions import ValidationError

class EmployeeType(models.Model):
    _name = 'employee.type'
    _rec_name = 'employee_type'
    employee_type = fields.Char("Employee Type")

