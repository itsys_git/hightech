# -*- coding: utf-8 -*-
{
    'name': "Employee Type",

    'summary': """
        Employee Type """,

    'description': """
        Employee Type
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','hr','maintenance_repair_fields'],

    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_view.xml',
        'views/employee_type_view.xml',
        'views/product_template_view.xml',
        'views/repair_order_view.xml',

    ],

}