# -*- coding: utf-8 -*-
{
    'name': "Helpdesk Escalation",

    'summary': """
        Helpdesk Escalation """,

    'description': """
        Helpdesk Escalation
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','sale','helpdesk','repair'],

    'data': [
        'security/ir.model.access.csv',
        'views/escalation.xml',
        'views/escalation_classification.xml',
        'views/escalation_facebook_group.xml',
        'views/escalation_instgram_group.xml',
        'views/escalation_reasons.xml',
        'views/escalation_source.xml',
        'views/escalation_twitter_group.xml',
        'views/escalation_type.xml',
        'views/customer_view.xml',

    ],

}