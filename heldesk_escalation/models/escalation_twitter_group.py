from odoo import models, fields, api

class EscalationTwitterGroup(models.Model):
    _name ='escalation.twitter.group'
    _rec_name ='escalation_twitter_group'
    escalation_twitter_group = fields.Char(string='Escalation Twitter Group')