from odoo import models, fields, api

class EscalationClassification(models.Model):
    _name ='escalation.classification'
    _rec_name ='escalation_classification'
    escalation_classification = fields.Char(string='Escalation Classification')
