from odoo import models, fields, api

class EscalationSource(models.Model):
    _name ='escalation.source'
    _rec_name ='escalation_source'
    escalation_source = fields.Char(string='Escalation Source')
