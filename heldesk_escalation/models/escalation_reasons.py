from odoo import models, fields, api

class EscalationReasons(models.Model):
    _name ='escalation.reasons'
    _rec_name ='escalation_reasons'
    escalation_reasons = fields.Char(string='Escalation Reasons')
