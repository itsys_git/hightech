from odoo import models, fields, api

class EscalationFacebookGroup(models.Model):
    _name ='escalation.facebook.group'
    _rec_name ='escalation_facebook_group'
    escalation_facebook_group = fields.Char(string='Escalation Facebook Group')