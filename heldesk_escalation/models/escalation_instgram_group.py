from odoo import models, fields, api

class EscalationInstgramGroup(models.Model):
    _name ='escalation.instgram.group'
    _rec_name ='escalation_Instagram_group'
    escalation_Instagram_group = fields.Char(string='Escalation Instagram Group')