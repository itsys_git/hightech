from odoo import models, fields, api

class Escalation(models.Model):
    _name = 'escalation'
    _rec_name = 'escalation'
    escalation = fields.Char(string='escalation')
    helpdesk_ticket_id = fields.Many2one('helpdesk.ticket' , string ='Helpdesk Ticket')
    repair_order_id = fields.Many2one('repair.order' , string ='Repair Order')
    customer_id = fields.Many2one('res.partner',string ='Customer')
    escalation_classification_id = fields.Many2one('escalation.classification',string='Escalation Classification')
    escalation_reasons_id = fields.Many2one('escalation.reasons',string='Escalation Reasons')
    escalation_source_id = fields.Many2one('escalation.source',string='Escalation Source')
    escalation_details = fields.Text(string = 'Escalation Details ')

    state = fields.Selection([
        ('open', 'Open'),
        ('working', 'Working'),
        ('wait c.p.a close', 'Wait C.P.A Close'),
        ('follow customer', 'Follow Customer'),
        ('canceled', 'Canceled'),
        ('closed', 'Closed'),
    ], default='open')
