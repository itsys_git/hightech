from odoo import models, fields, api

class EscalationType(models.Model):
    _name ='escalation.type'
    _rec_name ='escalation_type'
    escalation_type = fields.Char(string='Escalation Type')
