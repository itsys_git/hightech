from odoo import models, fields, api
from odoo.osv import expression
class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'


    @api.multi
    def name_get(self):
        result = []
        for ticket in self:
            result.append((ticket.id,ticket.serial_number))
        return result


    @api.depends('name')
    def con_serial(self):
        for ticket in self:
            if ticket.id:
                ticket.serial_number='('+'#'+str(ticket.id)+')'





    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        if args is None:
            args = []
        user_ids = []
        if name and operator in ['=', 'ilike']:
            user_ids = self._search([('serial_number', operator, name)] + args, limit=limit, access_rights_uid=name_get_uid)
        if not user_ids:
            user_ids = self._search([('name', operator, name)] + args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(user_ids).name_get()



    serial_number=fields.Char(compute='con_serial',string='Ticket Serial',store=True)


class RepairOrder(models.Model):
    _inherit ='repair.order'
    helpdesk_name = fields.Char(related='helpdesk_ticket_id.name',string='Ticket Name')
