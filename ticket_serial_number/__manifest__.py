# -*- coding: utf-8 -*-
{
    'name': "Ticket Serial Number",

    'summary': """
        Ticket Serial Number""",

    'description': """
       Ticket Serial Number
    """,

    'author': "ITsys-Corportion Eman ahmed",
    'website': "http://www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','helpdesk','hr_employee_edit','modification_on_hightech','ticket_status_create','repair_employee_ticket'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/helpdesk_ticket.xml',

    ],
    # only loaded in demonstration mode

}