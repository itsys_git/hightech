from odoo.exceptions import ValidationError
from openerp import fields, models, api


class RepairLine(models.Model):
    _inherit = 'repair.line'
    repair_order_obj=fields.Many2one('repair.order')
    product_id_rel=fields.Many2one(related='repair_order_obj.product_id',store=True)
    @api.onchange('product_id_rel')
    def product_domain(self):
        list = []
        if self.product_id_rel:
            search_product = self.env['product.product'].search([('id', '=', self.product_id_rel.id)])
            for rec in search_product:
                for line in rec.product_ids:
                    search_product_2 = self.env['product.product'].search([('product_tmpl_id', '=', line.id)])
                    for s in search_product_2:
                         list.append(s.id)
                print ('listtttt',list)
                domain = [('id', 'in', list)]
                print (self.product_id)
                return {'domain': {'product_id':domain}}

