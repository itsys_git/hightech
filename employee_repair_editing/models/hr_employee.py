# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrEmployee(models.Model):

    _inherit = 'hr.employee'

    center_type_id=fields.Many2many("center.type",'c1','c2',string='Center Type')
    area_id=fields.Many2one("area.area",string='Area')
    region_id=fields.Many2one("region.region",string='Region')
    sector_id=fields.Many2one("sectors.sectors",string='Sector')


class CenterType(models.Model):
    _name = 'center.type'
    _rec_name = 'center_type'

    center_type = fields.Char('Center Type',required=1)



class Area(models.Model):
    _name = 'area.area'
    _rec_name = 'area_name'

    area_name = fields.Char('Area Name',required=1)

class Region(models.Model):
    _name = 'region.region'
    _rec_name = 'region_name'

    region_name = fields.Char('Region Name',required=1)

class Sectors(models.Model):
    _name = 'sectors.sectors'
    _rec_name = 'sector_name'


    sector_name = fields.Char('Sector Name',required=1)





