# -*- coding: utf-8 -*-

from odoo import models, fields, api

class HelpdeskTicket(models.Model):

    _inherit = 'helpdesk.ticket'

    lot_serial_id = fields.Many2one(
        'stock.production.lot', 'Lot/Serial',
        domain="[('product_id','=', product_id),('partner_id','=',partner_id)]",
        help="Products repaired are all belonging to this lot", oldname="prodlot_id")



    #
    # @api.multi
    # def open_customer_tickets(self):
    #     return {
    #         'type': 'ir.actions.act_window',
    #         'name': _('Customer Tickets'),
    #         'res_model': 'helpdesk.ticket',
    #         'view_mode': 'kanban,tree,form,pivot,graph',
    #         'context': {'search_default_is_open': True, 'search_default_partner_id': self.partner_id.id}
    #     }

    @api.onchange('lot_serial_id')
    def on_change_lot(self):
        res = {}
        lis = []
        lis2 = []
        if self.lot_serial_id:
            if self.lot_serial_id.partner_id:
                se_product = self.env['product.product'].search([('id', '=', self.lot_serial_id.product_id.id)])
                se_customer= self.env['res.partner'].search([('id', '=', self.lot_serial_id.partner_id.id)])
                for i in se_product:
                    lis.append(i.id)
                for i in se_customer:
                    lis2.append(i.id)

                domain1 = [('id', 'in', lis)]
                domain2 = [('id', 'in', lis2)]
                res['domain'] = {'product_id': domain1,'partner_id':domain2}
            else:
                se_product = self.env['product.product'].search([('spare_parts', '=', False)])
                se_customer = self.env['res.partner'].search([])
                for i in se_product:
                    lis.append(i.id)
                for i in se_customer:
                    lis2.append(i.id)

                domain1 = [('id', 'in', lis)]
                domain2 = [('id', 'in', lis2)]
                res['domain'] = {'product_id': domain1, 'partner_id': domain2}

        else:
            se_product = self.env['product.product'].search([('spare_parts','=',False)])
            se_customer = self.env['res.partner'].search([])
            for i in se_product:
                lis.append(i.id)
            for i in se_customer:
                lis2.append(i.id)

            domain1 = [('id', 'in', lis)]
            domain2 = [('id', 'in', lis2)]
            res['domain'] = {'product_id': domain1, 'partner_id': domain2}
        return res