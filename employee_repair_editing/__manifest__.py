# -*- coding: utf-8 -*-
{
    'name': "employee_repair_editing",

    'author': "Marwa Ahmed",
    'website': "http://www.yourcompany.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','hr','hr_employee_edit','helpdesk'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/ticket_lot.xml',
        'views/templates.xml',
    ],

    'demo': [
        'demo/demo.xml',
    ],
}