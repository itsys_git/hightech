# -*- coding: utf-8 -*-
{
    'name': "customer_mobile_phone",

    'summary': """
        customer_mobile_phone """,

    'description': """
        customer_mobile_phone
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','sale','customer_profile_editing2'],

    'data': [
        # 'security/ir.model.access.csv',
        'views/res_partner_view.xml',

    ],

}