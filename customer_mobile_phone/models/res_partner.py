from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re

class ResPartner(models.Model):
    _inherit ='res.partner'

    type = fields.Selection(
        [('other', 'Other address')
         ], string='Address Type',
        default='other',
        copy=False,
        help="Used by Sales and Purchase Apps to select the relevant address depending on the context.")

    # _sql_constraints = [
    #     ('main_mobile_number_uniq', '', 'The Main Mobil Number must be unique!'),
    #     ('mobile_number2_uniq', '', 'The Mobile Number 2 must be unique!'),
    #     ('mobile_number3_uniq', '', 'The Mobile Number 3 must be unique!'),
    # ]


    @api.multi
    @api.constrains('lan_lind1','lan_lind2','mobile_number2','mobile_number3','phone','main_mobile_number')
    def _check_phone_number(self):
        for rec in self:

            if rec.lan_lind1:
                if len(rec.lan_lind1) > 8 or len(rec.lan_lind1) < 7:
                   raise ValidationError('Landline 1 accept only 7 or 8 numbers')

            if rec.lan_lind2:
                if len(rec.lan_lind2) > 8 or len(rec.lan_lind2) < 7:
                    raise ValidationError('Landline 2 accept only 7 or 8 numbers')

            if rec.mobile_number2:
                if len(rec.mobile_number2) > 11 or len(rec.mobile_number2) < 11:
                    raise ValidationError('You Should enter 11 numbers for mobile number2')

            if rec.mobile_number3:
                if len(rec.mobile_number3) > 11 or len(rec.mobile_number3) < 11:
                    raise ValidationError('You Should enter 11 numbers for mobile number3')

            if rec.phone:
                if len(rec.phone) > 11 or len(rec.phone) < 11:
                    raise ValidationError('You Should enter 11 numbers for phone')

            if rec.main_mobile_number:
                if len(rec.main_mobile_number) > 11 or len(rec.main_mobile_number) < 11:
                    raise ValidationError('You Should enter 11 numbers for main mobile number')

            val = str(rec.phone)
            val2 = str(rec.lan_lind1)
            val3 = str(rec.lan_lind2)
            val4 = str(rec.mobile_number2)
            val5 = str(rec.mobile_number3)
            val6 = str(rec.main_mobile_number)
            if rec.phone:
                if not val.isdigit():
                    raise ValidationError('You Should enter Numbers only for phone field')

            if rec.lan_lind1:
                if not val2.isdigit():
                    raise ValidationError('You Should enter Numbers only for landline1 field')

            if rec.lan_lind2:
                if not val3.isdigit():
                    raise ValidationError('You Should enter Numbers only for landline2 field')

            if rec.mobile_number2:
                if not val4.isdigit():
                    raise ValidationError('You Should enter Numbers only for mobile number2 field')

            if rec.mobile_number3:
                if not val5.isdigit():
                    raise ValidationError('You Should enter Numbers only for mobile number3 field')
            if rec.main_mobile_number:
                if not val6.isdigit():
                    raise ValidationError('You Should enter Numbers only for main mobile number field')
            # if rec.mobile_number2:
            #     mobile_number2_obj = self.search([('mobile_number2', '=', rec.mobile_number2), ('id', '!=', self.id)])
            #     if mobile_number2_obj:
            #         for rec in mobile_number2_obj:
            #             print ("mobile2")
            #             raise ValidationError('Mobile Number 2 Already Exists')
            # if rec.mobile_number3:
            #     mobile_number3_obj = self.search([('mobile_number3', '=', rec.mobile_number3), ('id', '!=', self.id)])
            #     if mobile_number3_obj:
            #         for rec in mobile_number3_obj:
            #             print ("mobile3")
            #             raise ValidationError('Mobile Number 3 Already Exists')
            # if rec.main_mobile_number:
            #     main_mobile_number_obj = self.search([('main_mobile_number', '=', rec.main_mobile_number), ('id', '!=', self.id)])
            #     if main_mobile_number_obj:
            #         for rec in main_mobile_number_obj:
            #             print ("mainnnn")
            #             raise ValidationError('Main Mobile Number Already Exists')

            for line in self.child_ids:
                val_ch = str(line.phone)
                val2_ch = str(line.lan_lind1)
                val3_ch = str(line.lan_lind2)
                val4_ch = str(line.mobile_number2)
                val5_ch = str(line.mobile_number3)
                val6_ch = str(line.main_mobile_number)
                if line.lan_lind1:
                    if not val2_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for landline1 field for contacts')
                    if len(line.lan_lind1) > 8 or len(line.lan_lind1) < 7:
                        raise ValidationError('Landline 1 accept only 7 or 8 numbers for contacts')

                if line.lan_lind2:
                    if not val3_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for landline2 field for contacts')

                    if len(line.lan_lind2) > 8 or len(line.lan_lind2) < 7:
                        raise ValidationError('Landline 2 accept only 7 or 8 numbers for contacts')

                if line.mobile_number2:
                    if not val4_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for mobile number2 field for contacts')

                    if len(line.mobile_number2) > 11 or len(line.mobile_number2) < 11:
                        raise ValidationError('You Should enter 11 numbers for mobile number2 for contacts')

                if line.mobile_number3:
                    if not val5_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for mobile number3 field for contacts')

                    if len(line.mobile_number3) > 11 or len(line.mobile_number3) < 11:
                        raise ValidationError('You Should enter 11 numbers for mobile number3 for contacts')

                if line.phone:
                    if not val_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for phone field')

                    if len(line.phone) > 11 or len(line.phone) < 11:
                        raise ValidationError('You Should enter 11 numbers for phone for contacts')
                if line.main_mobile_number:
                    if not val6_ch.isdigit():
                        raise ValidationError('You Should enter Numbers only for main mobile number for contacts field')
                    if len(line.main_mobile_number) > 11 or len(line.main_mobile_number) < 11:
                        raise ValidationError('You Should enter 11 numbers for main mobile number for contacts')

                if line.mobile_number2:
                    mobile_number2_obj_child = line.search(
                        [('mobile_number2', '=', line.mobile_number2), ('id', '!=', line.id)])
                    if mobile_number2_obj_child:
                        for rec in mobile_number2_obj_child:
                            print ("mobile2")
                            raise ValidationError('Mobile Number 2 for contacts Already Exists')
                if line.mobile_number3:
                    mobile_number3_obj_child = line.search(
                        [('mobile_number3', '=', line.mobile_number3), ('id', '!=', line.id)])
                    if mobile_number3_obj_child:
                        for rec in mobile_number3_obj_child:
                            print ("mobile3")
                            raise ValidationError('Mobile Number 3 for contacts Already Exists')
                if line.main_mobile_number:
                    print("ggggggggg")
                    main_mobile_number_obj_child = line.search(
                        [('main_mobile_number', '=', line.main_mobile_number), ('id', '!=', line.id)])
                    if main_mobile_number_obj_child:
                        for rec in main_mobile_number_obj_child:
                            print ("mainnnn")
                            raise ValidationError('Main Mobile Number for contacts Already Exists')

        return True








