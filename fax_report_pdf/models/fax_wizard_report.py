# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
class RepairOrder(models.Model):
    _inherit='repair.order'
    dealer_name= fields.Many2one('serial.dealer',related='lot_id.serial_dealer_id',store=True)

class FaxReport(models.TransientModel):
    _name = 'fax.report'
    _description = 'plan report'

    date_from=fields.Date(required=True,string='Schedule Date')
    dealer_name= fields.Many2many('serial.dealer','sd1','sd2')


    @api.multi
    def test_report(self):
        print ('kkkkkkkkkkkkk')
        [data] = self.read()
        datas = {
            'ids': [],
            'model': 'repair.order',
            'form': data
        }
        return self.env.ref('fax_report_pdf.plan_report_pdf').report_action([], data=datas)