# -*- coding: utf-8 -*-
{
    'name': "Fax Report PDF",

    'author': "Eman Ahmed",

    'category': 'Repair',
    'version': '0.1',
    'license': 'AGPL-3',

    'depends': ['base', 'repair','helpdesk','ivr_config','repair_status'],

    'data': [

        'views/fax_pdf_report.xml',
        'views/fax_wizard_report.xml',

    ],

}