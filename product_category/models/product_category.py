# -*- coding: utf-8 -*-
from odoo.exceptions import ValidationError
from openerp import fields, models, api


class ProductCategory(models.Model):
    _inherit = 'product.category'

    employee_list_ids = fields.Many2many(comodel_name='hr.employee', string='Technicians')


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    status_ids = fields.Many2many(comodel_name='res.country.state', string='Service Centers')
    capacity = fields.Integer('Capacity')

    @api.constrains('capacity')
    def not_minus(self):
        for rec in self:
            if rec.capacity <= 0:
                raise ValidationError('Capacity is not minus or 0')


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    _sql_constraints = [
        ('uniq_Internal_Reference', 'unique(default_code)',
         'Internal Reference already exists!'),
    ]


class ResCountryState(models.Model):
    _inherit = 'res.country.state'
    price = fields.Float('Price')

    @api.constrains('price')
    def not_minus(self):
        for rec in self:
            if rec.price < 0:
                raise ValidationError('Price is not minus')
