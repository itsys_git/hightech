# -*- coding: utf-8 -*-
{
    'name': "ticket status create",
    'author': "Marwa Ahmed",
    'website': "http://www.it-syscorp.com",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base','helpdesk','repair_status_create',
                ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/ticket_status.xml',
        # 'views/repair_order.xml',
        'sequences/seq_ticket_status.xml',
    ],


}