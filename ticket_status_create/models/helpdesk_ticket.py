from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime
import time


class HelpdeskTiecket(models.Model):
    _inherit ='helpdesk.ticket'

    new_ticket_status=fields.Many2one('ticket.status.action',domain="[('status_from','=',ticket_status_id)]")

    @api.multi
    def _get_ticket_name_default(self):
        search_id = self.env['helpdesk.ticket.type'].search([('name', '=', 'Field Service')]).id
        if search_id:
            return search_id
        else:
            create_id = self.env['helpdesk.ticket.type'].create({'name': 'Field Service'}).id
            return create_id

    ticket_type_id=fields.Many2one('helpdesk.ticket.type', default=_get_ticket_name_default)

    @api.model
    def create(self, vals):
        new_ticket_status = vals.get('new_ticket_status')
        new_ticket_obj = self.env['ticket.status.action'].browse(new_ticket_status)
        print('hhhhhhhhhhh', new_ticket_obj)
        vals['new_ticket_status'] = False
        res = super(HelpdeskTiecket, self).create(vals)
        for rec in res:
            if new_ticket_obj != False:
                if new_ticket_obj.action == 'create':
                    record = rec.copy_data()
                    for reco in record:
                        reco.update({'ticket_status_id': new_ticket_obj.status_ticket_to.id,
                                     'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                                     'new_ticket_status': False})
                        self.env['helpdesk.ticket'].create(reco)

                        rec.update({'ticket_status_id': new_ticket_obj.status_to.id,
                                    # 'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                                    'new_ticket_status': False})

                if new_ticket_obj.action == 'no_create':
                    rec.update({'ticket_status_id': new_ticket_obj.status_to.id,
                                # 'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                                'new_ticket_status': False})

        return res

    @api.multi
    def write(self, vals):
        new_ticket_status = vals.get('new_ticket_status')
        new_ticket_obj = self.env['ticket.status.action'].browse(new_ticket_status)
        vals['new_ticket_status'] = False
        res = super(HelpdeskTiecket, self).write(vals)
        for rec in self:
            if new_ticket_obj != False:
                if new_ticket_obj.action == 'create':
                    print('ooooooooooooooooooooooo')

                    record = rec.copy_data()
                    for reco in record:
                        reco.update({'ticket_status_id': new_ticket_obj.status_ticket_to.id,
                                     'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                                     'new_ticket_status': False})
                        self.env['helpdesk.ticket'].create(reco)
                        rec.update({'ticket_status_id': new_ticket_obj.status_to.id,
                                    # 'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                                    'new_ticket_status': False})

                if  new_ticket_obj.action == 'no_create':
                    records = {'ticket_status_id':new_ticket_obj.status_to.id,
                               # 'ticket_type_id': new_ticket_obj.ticket_type_id.id,
                               'new_ticket_status': False}
                    self.write(records)
        return res




