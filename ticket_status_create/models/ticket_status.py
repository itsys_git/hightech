from odoo import models, fields, api
from odoo.exceptions import ValidationError

class TicketStatusAction(models.Model):
    _name='ticket.status.action'
    _rec_name='status_to'

    group_access_ids=fields.Many2many('set.up',string='Group Access')
    seq = fields.Char(string='Sequence')

    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('ticket.status.action') or '/'
        values['seq'] = seq
        res = super(TicketStatusAction, self).create(values)
        return res

    status_from = fields.Many2one('ticket.status')
    status_to=fields.Many2one('ticket.status')
    status_ticket_to = fields.Many2one('ticket.status','New Status To')
    action=fields.Selection([('create','Create'),('no_create','No Create')])
    ticket_type_id = fields.Many2one('helpdesk.ticket.type', string="Ticket Type")

    @api.constrains('status_from','status_to')
    def uniqe_status_con(self):
        ser_uniq= self.search([('status_from', '=', self.status_to.id),('status_to', '=', self.status_from.id), ('id', '!=', self.id)])
        ser_dub= self.search([('status_from', '=', self.status_from.id),
                           ('action', '=', self.action), ('status_to', '=', self.status_to.id),
                           ('id', '!=', self.id)])
        # if ser_uniq:
        #     raise ValidationError("This Make Recursive in States , Status From and Status To Already Exists")
        if ser_dub:
            raise ValidationError("This Make Dublicate Status ,Status From and Status To Already Exists")
        if self.status_from == self.status_to:
            raise ValidationError("State From Should'nt be The Same State To")


    @api.constrains('action')
    def change_action_ticket(self):
        if self.action == 'no_create':
            if self.status_ticket_to:
                self.status_ticket_to = ''