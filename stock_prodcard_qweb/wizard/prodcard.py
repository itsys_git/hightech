# -*- coding: utf-8 -*-
import time
from datetime import datetime
from dateutil import relativedelta
from odoo import api, fields, models

class prod_card_check(models.TransientModel):
    _name = 'prod.card.check'

    date_start= fields.Date('Date from',required=True,default=time.strftime('%Y-%m-01'),)
    date_end= fields.Date('Date to',required=True,default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10])
    product_id= fields.Many2one('product.product', "Product", required=True)
    location_id= fields.Many2one('stock.location', "Location",)
    value= fields.Boolean('Value')

    # @api.multi
    def check_report(self):
        active_ids = self.env.context.get('active_ids', [])
        print('self.read()[0]', self.read()[0])
        datas = {
            'ids': active_ids,
            'model': 'prod.card.check',
            'form': self.read()[0]
        }
        return self.env.ref('stock_prodcard_qweb.item_card').report_action([], data=datas)