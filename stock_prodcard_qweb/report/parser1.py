# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError

class Parser(models.AbstractModel):
    _name = 'report.stock_prodcard_qweb.report_item_card'

    def _get_partner_name(self, p_id):
        name = self.env['res.partner'].browse(p_id).name
        return name

    def _get_init_balance(self,prod_id,loc_id, date1):
        self._cr.execute(""" 
			SELECT sum(product_qty) as product_qty, sum(value) as value from (
			            (SELECT
                                m.product_id as product_id,
                                coalesce(sum(-m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_id=l.id)
                                 where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and pp.id="""+str(prod_id)+""" 
                                 GROUP BY m.product_id 
                        ) UNION ALL (
                            SELECT
                                m.product_id as product_id,
                                coalesce(sum(m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
                        where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and pp.id="""+str(prod_id)+"""
                             GROUP BY m.product_id
                            )
                            ) as Sub     
                      """)
        sql= """ 
			SELECT sum(product_qty) as product_qty, sum(value) as value from (
			            (SELECT
                                m.product_id as product_id,
                                coalesce(sum(-m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_id=l.id)
                                 where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and pp.id="""+str(prod_id)+""" 
                                 GROUP BY m.product_id 
                        ) UNION ALL (
                            SELECT
                                m.product_id as product_id,
                                coalesce(sum(m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
                        where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and pp.id="""+str(prod_id)+"""
                             GROUP BY m.product_id
                            )
                            ) as Sub  
                                                    """
        res = self._cr.fetchone()

        if loc_id:
            self._cr.execute(""" 
			SELECT sum(product_qty) as product_qty, sum(value) as value from (
                            (SELECT
                                m.product_id as product_id,
                                coalesce(sum(-m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_id=l.id)
                                    where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and l.id="""+str(loc_id[0])+""" and pp.id="""+str(prod_id)+"""
                                 GROUP BY m.product_id 
                        ) UNION ALL (
                            SELECT
                                m.product_id as product_id,
                                coalesce(sum(m.product_qty * pu.factor / pu2.factor)::decimal, 0.0) as product_qty,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value                                
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                        LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                    LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
                                    where m.state='done' and  l.usage = 'internal' and date(m.date)<'"""+str(date1)+"""' and l.id="""+str(loc_id[0])+""" and pp.id="""+str(prod_id)+"""

                             GROUP BY m.product_id
                            )
                            ) as Sub                   
                                    """)
            res = self._cr.fetchone()

        if res:
            if res[0]:
                return [res[0],res[1]]
        else: return [0,0]

    def _get_loc_name(self,loc_id):
        if loc_id:
            self._cr.execute("""select name from stock_location where id="""+str(loc_id))
            res = self._cr.dictfetchone()
            return res["name"]
        else: return ''

    def _get_prod_data1(self, prod_id,loc_id1,date1,date2):
        if not loc_id1:
            self._cr.execute(""" 
                            (SELECT
                                coalesce(m.price_unit ::decimal, 0.0) as price_unit, min(m.id) as id, m.date as date,
                                to_char(m.date, 'YYYY') as year,
                                to_char(m.date, 'MM') as month,
                                p.partner_id as partner_id,m.location_id as location_id,m.location_dest_id as location_dest_id,
                                m.product_id as product_id,  l.usage as location_type, l.scrap_location as scrap_location,
                                m.company_id,
                                m.state as state, 
                                m.product_uom,
                                m.origin,
				m.reference,
				p.scheduled_date,
                                pc.id as product_categ_id,
                                coalesce(sum(-m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value,
                                coalesce(-sum(m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as product_qty
            
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                    LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id)
                                    LEFT JOIN product_category pc ON (pc.id=pt.categ_id)                   
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN stock_location l ON (m.location_id=l.id)
                                where l.usage='internal'  and m.state='done' and product_id= """ +str(prod_id)+""" and date(m.date) >='"""+str(date1)+"""' and date(m.date) <='"""+str(date2)+"""' 
            
                            GROUP BY
                                m.price_unit, m.origin,m.reference, p.scheduled_date,pc.id,m.id, m.product_id, m.product_uom, p.partner_id, m.location_id,  m.location_dest_id,
                                m.date, m.state, l.usage, l.scrap_location, m.company_id, to_char(m.date, 'YYYY'), to_char(m.date, 'MM')
                        ) UNION ALL (
                            SELECT
                                coalesce(m.price_unit ::decimal, 0.0) as price_unit, -m.id as id, m.date as date,
                                to_char(m.date, 'YYYY') as year,
                                to_char(m.date, 'MM') as month,
                                p.partner_id as partner_id,m.location_id as location_id,m.location_dest_id as location_dest_id,
                                m.product_id as product_id, l.usage as location_type, l.scrap_location as scrap_location,
                                m.company_id,
                                m.state as state,
                                m.product_uom,
                                m.origin,
				m.reference,
				p.scheduled_date,
                                pc.id as product_categ_id,
                                coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value,
                                coalesce(sum(m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as product_qty
                            FROM
                                stock_move m
                                    LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                    LEFT JOIN product_product pp ON (m.product_id=pp.id)
                                    LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id)
                                    LEFT JOIN product_category pc ON (pc.id=pt.categ_id)     
                                        LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                                    LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
                                where l.usage='internal'  and m.state='done' and product_id= """ +str(prod_id)+""" and date(m.date) >='"""+str(date1)+"""' and date(m.date) <='"""+str(date2)+"""' 
                            GROUP BY
                                m.price_unit, m.origin,m.reference, p.scheduled_date,pc.id,m.id, m.product_id, m.product_uom, p.partner_id, m.location_id, m.location_dest_id,
                                m.date, m.state, l.usage, l.scrap_location, m.company_id, to_char(m.date, 'YYYY'), to_char(m.date, 'MM')
                            ) order by 3
                   """)
            res = self._cr.dictfetchall()
        if loc_id1:
            loc_id=loc_id1[0]
            self._cr.execute("""
                (SELECT
                    m.price_unit, min(m.id) as id, m.date as date,
                    to_char(m.date, 'YYYY') as year,
                    to_char(m.date, 'MM') as month,
                    p.partner_id as partner_id, 
                    m.location_id as location_id,m.location_dest_id as location_dest_id,
                    m.product_id as product_id,  l.usage as location_type, l.scrap_location as scrap_location,
                    m.company_id,
                    m.state as state, 
                    m.product_uom,
                    m.origin,
		    m.reference,
		    p.scheduled_date,
                    pc.id as product_categ_id,
                    coalesce(sum(-m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value,
                    coalesce(-sum(m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as product_qty

                FROM
                    stock_move m
                        LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                        LEFT JOIN product_product pp ON (m.product_id=pp.id)
                        LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id)
                        LEFT JOIN product_category pc ON (pc.id=pt.categ_id)                   
                            LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                        LEFT JOIN stock_location l ON (m.location_id=l.id)
                        where l.usage='internal'  and m.state='done' and product_id= """ +str(prod_id)+ """ and l.id="""+str(loc_id)+""" and date(m.date) >='"""+str(date1)+"""' and date(m.date) <='"""+str(date2)+"""' 
                GROUP BY
                    m.price_unit, m.origin,m.reference, p.scheduled_date,pc.id,m.id, m.product_id, m.product_uom, p.partner_id, m.location_id,  m.location_dest_id,
                    m.date, m.state, l.usage, l.scrap_location, m.company_id, to_char(m.date, 'YYYY'), to_char(m.date, 'MM')
            ) UNION ALL (
                SELECT
                    m.price_unit, -m.id as id, m.date as date,
                    to_char(m.date, 'YYYY') as year,
                    to_char(m.date, 'MM') as month,
                    p.partner_id as partner_id, 
                    m.location_id as location_id,m.location_dest_id as location_dest_id,
                    m.product_id as product_id, l.usage as location_type, l.scrap_location as scrap_location,
                    m.company_id,
                    m.state as state,
                    m.product_uom,
                    m.origin,
		    m.reference,
		    p.scheduled_date,
                    pc.id as product_categ_id,
                    coalesce(sum(m.price_unit * m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as value,
                    coalesce(sum(m.product_uom_qty * 1/pu2.factor)::decimal, 0.0) as product_qty
                FROM
                    stock_move m
                        LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                        LEFT JOIN product_product pp ON (m.product_id=pp.id)
                        LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id)
                        LEFT JOIN product_category pc ON (pc.id=pt.categ_id)     
                            LEFT JOIN product_uom pu2 ON (m.product_uom=pu2.id)
                        LEFT JOIN stock_location l ON (m.location_dest_id=l.id)
                        where l.usage='internal'  and m.state='done' and product_id= """ +str(prod_id)+ """ and l.id="""+str(loc_id)+""" and date(m.date) >='"""+str(date1)+"""' and date(m.date) <='"""+str(date2)+"""' 
                GROUP BY
                    m.price_unit, m.origin,m.reference, p.scheduled_date,pc.id,m.id, m.product_id, m.product_uom, p.partner_id, m.location_id, m.location_dest_id,
                    m.date, m.state, l.usage, l.scrap_location, m.company_id, to_char(m.date, 'YYYY'), to_char(m.date, 'MM')
                ) order by 3        
            """)
            res = self._cr.dictfetchall()
        return res


    @api.model
    def get_report_values(self, docids, data=None):
        print(data,docids)
        datas = self.env['prod.card.check'].browse(docids)

        if not datas:
            raise UserError(_("Form content is missing, this report cannot be printed."))
        location=None
        if datas.location_id:
            location=datas.location_id
        item_card = self.env['ir.actions.report']._get_report_from_name('stock_prodcard_qweb.report_item_card')
        return {
            'doc_ids': self.ids,
            'doc_model': item_card.model,
            'date_start': datas.date_start,
            'date_end': datas.date_end,
            'product_name': datas.product_id.name,
            'location_name': datas.location_id.name,
            'value': datas.value,
            'init_balance': self._get_init_balance(datas.product_id.id, datas.location_id.id,datas.date_start or [0,0]),
            'get_prod_data1': self._get_prod_data1(datas.product_id.id,datas.location_id.id,datas.date_start,datas.date_end),
            'get_loc_name': self._get_loc_name,
            'get_partner_name': self._get_partner_name,
        }
