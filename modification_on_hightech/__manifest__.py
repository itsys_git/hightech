# -*- coding: utf-8 -*-
{
    'name': "Modification On Hightech",

    'summary': """
        Modification On Hightech """,

    'description': """
        Modification On Hightech
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','sale','helpdesk','product_category'],

    'data': [
        'security/ir.model.access.csv',
        'security/service_center_security.xml',
        'views/helpdesk_ticket_view.xml',
        'views/ticket_status_view.xml',
        'views/product_issue.xml',
        'views/res_country_state_view.xml',
        'views/res_users_view.xml',
        'views/repair_order_view.xml',

    ],

}