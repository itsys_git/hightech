from odoo import models, fields, api
from odoo.exceptions import ValidationError

class TicketStatus(models.Model):
    _name ='ticket.status'
    _rec_name ='ticket_status'

    ticket_status = fields.Char(string='Name')
    desc = fields.Char('Description')

    seq = fields.Char(string='Sequence',readonly=True)

    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('ticket.status') or '/'
        values['seq'] = seq
        res = super(TicketStatus, self).create(values)
        return res



        # @api.multi
    # def unlink(self):
    #
    #     for rec in self:
    #         repair_order_obj = self.env['repair.order'].search([('repairs_status_id', '=', rec.id)])
    #         print ("nnnnnnnnnnnnnnnnnnnnn",repair_order_obj)
    #         if repair_order_obj:
    #             for order in repair_order_obj:
    #                 print ("kkkkkkkkk",order)
    #                 raise ValidationError("You can't delete this repair status because it is used in some ticket ")
    #     return super(TicketStatus, self).unlink()