# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResUsers(models.Model):
    _inherit ='res.users'
    service_center = fields.Boolean(string ='Service Center')