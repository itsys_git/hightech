# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError



class RepairOrder(models.Model):
    _inherit ='repair.order'
    check =fields.Boolean()
    status = fields.Char(String="Service Center Status",default="Waiting Approved")

    def check_true(self):
        self.check =True
        self.status = "Approved"

    def approved(self):
        print("approved")

    def check_waiting_true(self):
        print("waiting")

    # @api.constrains('sup_repair_id')
    # def check_sub_repair(self):
    #     repair_obj = self.env['repair.order'].search([('sup_repair_id', '=', self.sup_repair_id.id)])
    #     print ("repair_obj", repair_obj)
    #     if len(repair_obj) > 1:
    #         raise ValidationError(
    #             'There is a sub repair created and you can only create one sub repair for this repair order')
    #     return True