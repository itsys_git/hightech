# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError



class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'

    check = fields.Boolean()
    status = fields.Char(String="Service Center Status", default="Waiting Approved")

    def check_true(self):
        self.check = True
        self.status = "Approved"

    def approved(self):
        print("approved")

    def check_waiting_true(self):
        print("waiting")

    @api.multi
    def _get_field_channel_default(self):
        if self.env['res.users'].has_group('modification_on_hightech.service_center_group'):
            channel = 'sVC_center'
            return channel
        else:
            channel = 'agent'
            return channel


    channel = fields.Selection([
        ('agent', 'Agent'),
        ('iVR', 'IVR'),
        ('e-mail', 'E-mail'),
        ('fax', 'Fax'),
        ('facebook', 'Facebook'),
        ('whats_app', "What 's App"),
        ('twitter', 'Twitter'),
        ('web_chat', 'Web Chat'),
        ('sVC_center', "SVC Center"),
        ('hi_tech', 'Hi Tech'),
    ], 'Channel', default=_get_field_channel_default)

    # product_issue_id = fields.Many2one('product.issue', string ='Product Issue')

    product_issue_id = fields.Many2many('product.issue',string ='Product Issue')

    @api.multi
    def _get_field_name_default(self):
        search_id = self.env['ticket.status'].search([('ticket_status', '=', 'Open')]).id
        if search_id:
            return search_id
        else:
            create_id = self.env['ticket.status'].create({'ticket_status': 'Open'}).id
            return create_id

    ticket_status_id = fields.Many2one('ticket.status',string ='Ticket Status',default=_get_field_name_default)