# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResCountryState(models.Model):
    _inherit ='res.country.state'

    employee_ids = fields.Many2many('hr.employee','emp1','emp2',string = 'Employees')