from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ProductIssue(models.Model):
    _name = 'product.issue'
    _rec_name = 'product_issue'
    product_issue = fields.Char(string='Product Issue')

    @api.multi
    def unlink(self):
        for rec in self:
            ticket_obj = self.env['helpdesk.ticket'].search([('product_issue_id', '=', rec.id)])
            print ("nnnnnnnnnnnnnnnnnnnnn", ticket_obj)
            if ticket_obj:
                for tic in ticket_obj:
                    print ("kkkkkkkkk", tic)
                    raise ValidationError(
                            "You can't delete this repair status because it is used in some ticket ")
        return super(ProductIssue, self).unlink()