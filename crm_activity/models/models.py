# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ActivityAttribute(models.Model):
    _name = 'activity.attributes'
    _rec_name = 'name'

    name = fields.Char(required=True)
    type = fields.Selection(string="Type",
                            selection=[('attribute1', 'Attribute1'), ('attribute2', 'Attribute2')])


class ActivityStages(models.Model):
    _name = 'attribute.stages'
    _rec_name = 'name'

    name = fields.Char(string='Stage Name', required=1)
    desc = fields.Char(string='Description')


class AttributeType(models.Model):
    _name = 'attributes.type'
    _rec_name = 'name'

    name = fields.Char(required=True)
    desc = fields.Char(string='Description')
    activity_att1_id = fields.Many2one("activity.attributes", string='Activity Attribute1',
                                       domain=[('type', '=', 'attribute1')])
    activity_att2_id = fields.Many2one("activity.attributes", string='Activity Attribute2',
                                       domain=[('type', '=', 'attribute2')])

    @api.constrains('activity_att1_id', 'activity_att2_id')
    def check_type(self):
        for rec in self:
            if rec.activity_att1_id.type == 'attribute2':
                raise ValidationError('Type Should be Attribute1 in Activity Attribute1')

            if rec.activity_att2_id.type == 'attribute1':
                raise ValidationError('Type Should be  Attribute2 in Activity Attribute2')


class MailActivity(models.Model):
    _name = 'mail.activity.report.new'
    _rec_name = 'activity_type_id'
    _order = 'id desc'

    activity_type_id = fields.Many2one(
        'mail.activity.type', 'Activity',
        domain="['|', ('res_model_id', '=', False), ('res_model_id', '=', res_model_id)]", ondelete='restrict')
    summary = fields.Char('Summary')
    note = fields.Html('Note')
    date_deadline = fields.Date('Due Date', index=True, required=True, default=fields.Date.context_today)

    res_model_id = fields.Many2one(
        'ir.model', 'Document Model',
        index=True, ondelete='cascade', required=True)
    user_id = fields.Many2one(
        'res.users', 'Assigned to',
        default=lambda self: self.env.user,
        index=True, required=True)
    state = fields.Selection(string="State",
                              selection=[('draft', 'Draft'), ('done', 'Done')
                                         ], dedault='draft')
    res_id = fields.Integer('Related Document ID', index=True, required=True)

class MailActivity(models.Model):
    _inherit = 'mail.activity'
    _rec_name = 'activity_type_id'
    activity_add_type_id = fields.Many2one("attributes.type")
    activity_stage_id = fields.Many2one('attribute.stages', string='Status')

    stage = fields.Selection(string="Status",
                             selection=[('open', 'open'), ('following', 'following')
                                        ])

    def create_report(self,state):
        for record in self:
            create_vals = {
                'activity_type_id': record.activity_type_id.id,
                'summary': record.summary,
                'automated': True,
                'note': record.note,
                'date_deadline': record.date_deadline,
                'res_model_id': record.res_model_id.id,
                'res_id': record.res_id,
                'user_id': record.user_id.id,
                # 'state':state,

            }
            obj= self.sudo().env['mail.activity.report.new'].create(create_vals)
            print((obj))

    @api.multi
    def unlink(self):
        print('unlink')
        for activity in self:
            if activity.date_deadline <= fields.Date.today():

                activity.create_report('done')

        return super(MailActivity, self.sudo()).unlink()
