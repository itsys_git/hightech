# -*- coding: utf-8 -*-
{
    'name': "crm_activity",

    'author': "Marwa Ahmed",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','crm','mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ]

}