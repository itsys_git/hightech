# -*- coding: utf-8 -*-
{
    'name': "hidding remain capacity",
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','modification_on_hightech',
                'repair_employee_ticket','repair','helpdesk',
                'hr_employee_edit','employee_repair_editing',
                'ticket_customer','maintenance_repair_fields','repair_status','repair_employee_ticket_modify'],

    'data': [
        'views/views.xml',
        'views/helpdesk_mandatory.xml',
    ],

}