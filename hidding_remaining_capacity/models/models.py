# -*- coding: utf-8 -*-
from odoo import models, fields, api

class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'


    user_id=fields.Many2one('res.users',default=lambda self: self.env.user )

    @api.onchange('user_id')
    def onchange_team_member(self):
        res = {}
        lis = []
        if self.user_id:
            se_ser = self.env['helpdesk.team'].search([('member_ids', 'in', self.env.user.id)])
            if se_ser:
                for i in se_ser:
                    lis.append(i.id)
                domain = [('id', 'in', lis)]
                res['domain'] = {'team_id': domain}
        # if self.team_id not in lis and self.team_id:
        #     self.team_id=False


        return res


class RepairOrder(models.Model):
    _inherit = 'repair.order'



    # @api.onchange('re_assign_to_employee_id', 'technition')
    # def onchange_emp_tech(self):
    #     res = {}
    #     lis2 = []
    #     if self.re_assign_to_employee_id:
    #         se_ser = self.env['hr.employee'].search([('service_center', '=', self.re_assign_to_employee_id.id)])
    #         for i in se_ser:
    #             lis2.append(i.id)
    #         domain = [('id', '=', lis2)]
    #         res['domain'] = {'technition': domain}
    #     return res



