# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging
import ast
from odoo.osv.expression import get_unaccent_wrapper
import re

_logger = logging.getLogger(__name__)


class res_partner(models.Model):
    _inherit = "res.partner"

    customer_no = fields.Char( 'Contact No')


    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('res.partner.customer.no')
        print('sequence',sequence)
        vals['customer_no'] = sequence
        res = super(res_partner, self).create(vals)


        return res
    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        self = self.sudo(name_get_uid or self.env.uid)
        if args is None:
            args = []
        if name and operator in ('=', 'ilike', '=ilike', 'like', '=like'):
            self.check_access_rights('read')
            where_query = self._where_calc(args)
            self._apply_ir_rules(where_query, 'read')
            from_clause, where_clause, where_clause_params = where_query.get_sql()
            where_str = where_clause and (" WHERE %s AND " % where_clause) or ' WHERE '

            # search on the name of the contacts and of its company
            search_name = name
            if operator in ('ilike', 'like'):
                search_name = '%%%s%%' % name
            if operator in ('=ilike', '=like'):
                operator = operator[1:]

            unaccent = get_unaccent_wrapper(self.env.cr)

            query = """SELECT id
                         FROM res_partner
                      {where} ({email} {operator} {percent}
                           OR {display_name} {operator} {percent}
                                                      OR {customer_no} {operator} {percent}

                           OR {reference} {operator} {percent}
                           OR {vat} {operator} {percent})
                           -- don't panic, trust postgres bitmap
                     ORDER BY {display_name} {operator} {percent} desc,
                              {display_name}
                    """.format(where=where_str,
                               operator=operator,
                               email=unaccent('email'),
                               display_name=unaccent('display_name'),
                               customer_no=unaccent('customer_no'),

                               reference=unaccent('ref'),
                               percent=unaccent('%s'),
                               vat=unaccent('vat'),)

            where_clause_params += [search_name]*4  # for email / display_name, reference
            where_clause_params += [re.sub('[^a-zA-Z0-9]+', '', search_name)]  # for vat
            where_clause_params += [search_name]  # for order by
            if limit:
                query += ' limit %s'
                where_clause_params.append(limit)
            self.env.cr.execute(query, where_clause_params)
            partner_ids = [row[0] for row in self.env.cr.fetchall()]

            if partner_ids:
                return self.browse(partner_ids).name_get()
            else:
                return []
        return super(res_partner, self)._name_search(name, args, operator=operator, limit=limit, name_get_uid=name_get_uid)
