{
    'name': "Customer sequence no",
    'version': '11.0.0',

    'category': 'Base',
    'price': '10',
    'sequence': 0,
    'depends': [
        'base',


    ],
    'demo': [],
    'data': [

        'views/res_partner.xml',

    ],
    'qweb': [
    ],
    "currency": 'EUR',
    'installable': True,
    'application': True,

    "license": "OPL-1",
}
