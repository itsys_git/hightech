# -*- coding: utf-8 -*-
{
    'name': "Remove Create Edit",

    'summary': """
       Remove create and edit from all Many2one fields in Repair,Helpdesk and contacts""",


    'author': "Marwa Ahmed",
    'website': "http://www.yourcompany.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','repair','hr_employee_edit',
                'repair_status','helpdesk','ticket_customer','customer_profile_editing2',
                'modification_on_hightech'],

    # always loaded
    'data': [

        'security/security.xml',
        'views/views.xml',
        'views/templates.xml',
    ],

}