# -*- coding: utf-8 -*-
{
    'name': "Customer Profile Editing2",

    'summary': """
        edit the customer profile view """,

    'description': """
        edit the customer profile view
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','sale','project','ticket_customer','stock','contacts'],

    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_view.xml',
        'views/partner_kanaban.xml',
    ],

}