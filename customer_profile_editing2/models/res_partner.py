from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit ='res.partner'


    name_concat = fields.Char("Full Name")
    product_list=fields.Boolean( default=True)
    lot_id=fields.One2many('stock.production.lot','partner_id')
    city_id=fields.Many2one('city.model')

    first_name_in_arabic = fields.Char("First Name")
    middle_name_in_arabic =fields.Char("middle Name")
    final_name_in_arabic = fields.Char("Final Name")

    first_name_in_english = fields.Char("First Name")
    middle_name_in_english = fields.Char("middle Name")
    final_name_in_english = fields.Char("Final Name")


    facebook_url =fields.Char("Facebook URL")
    another_details = fields.Char("Another Details")
    apartment_number = fields.Char("Apartment Number")
    special_marque = fields.Char("Special Marque")
    door = fields.Char("Door")
    website = fields.Char("Website")

    # contact_title_id = fields.Many2one('contact.title', string="Contact Title")
    category_partner_id = fields.Many2one('category', string="Partner Category")
    job_position = fields.Char("Job Position")

    main_mobile_number = fields.Char("Main Mobile Number")
    mobile_number1 = fields.Char("Mobile Number1")
    mobile_number2 = fields.Char("Mobile Number2")
    mobile_number3 = fields.Char("Mobile Number3")
    fax = fields.Char("Fax")


    governerate_code1 = fields.Char("Governerate Code1")
    lan_lind1 = fields.Char("Landline1")
    governerate_code2 = fields.Char("Governerate Code2")
    lan_lind2 = fields.Char("Landline2")


    @api.onchange('state_id')
    def change_city(self):
        res={}
        lis=[]
        lis2=[]
        if self.state_id:
           se_city=self.env['city.model'].search([('state','=',self.state_id.id)])
           for i in se_city:
               lis2.append(i.id)
           domain = [('id', 'in',lis2)]
           res['domain'] = {'city_id': domain}
        else:
            se_city = self.env['city.model'].search([])
            for i in se_city:
                lis.append(i.id)
            domain=[('id', 'in',lis)]
            res['domain'] = {'city_id': domain}
        return res

    my_domain = fields.Many2many(comodel_name='city.model', string='Domain', relation='city_model_ress_partner',
                                 column1='partner_id1', column2='partner_id2', compute='_get_domain_str')

    @api.one
    def _get_domain_str(self):
        res = {}
        lis = []
        lis2 = []
        domain=[]
        for item in self:

            if item.state_id:
                se_city = self.env['city.model'].search([('state', '=', item.state_id.id)])
                for i in se_city:
                    lis2.append(i.id)
                domain = [('id', 'in', lis2)]

            else:
                se_city = self.env['city.model'].search([])
                for i in se_city:
                    lis.append(i.id)
                domain = [('id', 'in', lis)]

            if domain:
                item.my_domain = domain[0][2]
            else:
                item.my_domain = self.env['city.model'].sudo().search([]).ids

    @api.onchange('first_name_in_arabic', 'middle_name_in_arabic', 'final_name_in_arabic','name')
    def change_name(self):
        for rec in self:
            if rec.first_name_in_arabic:
                rec.name = str(str(rec.first_name_in_arabic))

            if rec.middle_name_in_arabic:
                rec.name = str(str(rec.middle_name_in_arabic))

            if rec.final_name_in_arabic:
                rec.name = str(str(rec.final_name_in_arabic))

            if rec.first_name_in_arabic and rec.middle_name_in_arabic and not rec.final_name_in_arabic:
                rec.name = str(rec.first_name_in_arabic) + ' ' + str(rec.middle_name_in_arabic)

            if rec.first_name_in_arabic and rec.middle_name_in_arabic and rec.final_name_in_arabic:
                rec.name = str(rec.first_name_in_arabic) + ' ' + str(rec.middle_name_in_arabic) + ' ' + str(
                    rec.final_name_in_arabic)
            if rec.middle_name_in_arabic and rec.final_name_in_arabic and not rec.first_name_in_arabic:
                rec.name = str(str(rec.middle_name_in_arabic) + ' ' + str(rec.final_name_in_arabic))

            if rec.first_name_in_arabic and rec.final_name_in_arabic and not rec.middle_name_in_arabic:
                rec.name = str(str(rec.first_name_in_arabic) + ' ' + str(rec.final_name_in_arabic))

            if  not rec.final_name_in_arabic and not rec.middle_name_in_arabic:
                rec.name = ''

class CityModel(models.Model):
    _name ='city.model'
    name=fields.Char()
    code=fields.Char()
    state=fields.Many2one('res.country.state')