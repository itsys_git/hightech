
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class Category(models.Model):
      _name = 'category'
      _rec_name = 'category'
      category = fields.Char("Category")


