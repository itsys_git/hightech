# -*- coding: utf-8 -*-
{
    'name': "report_approve_on_replace",
    'author':'Marwa Ahmed',

    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'repair','helpdesk'],


    'data': [
        'views/views.xml',
        'views/templates.xml',
    ],

}