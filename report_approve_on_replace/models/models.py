# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import timedelta


class RepairOrder(models.Model):
    _inherit = 'repair.order'

    date_dep=fields.Char(compute='change_create')

    @api.multi
    @api.depends('create_date')
    def change_create(self):
        for rec in self:
            if rec.create_date:
              diff = (fields.Date.today() - (rec.create_date).date())+ timedelta(days=1)
              print("diffff", diff)
              rec.date_dep=diff.days





class Helpdesk(models.Model):
    _inherit = 'helpdesk.ticket'

    diff_ticket=fields.Char(compute='change_create_date')

    @api.multi
    @api.depends('create_date')
    def change_create_date(self):
        for rec in self:
            if rec.create_date:
                diff = (fields.Date.today() - (rec.create_date).date()) + timedelta(days=1)
                print("diffff", diff)
                rec.diff_ticket = diff.days


    # @api.multi
    # @api.depends('create_date')
    # def change_create_date(self):
    #     for rec in self:
    #         if rec.create_date:
    #           diff = (fields.Date.today() - (rec.create_date).date())
    #           print("diffff", diff)
    #           rec.diff_ticket=diff.days
    #
    #


