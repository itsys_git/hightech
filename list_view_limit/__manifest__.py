# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'List views limit',
    'version': '1.0',
    'sequence': 10,
    'author': 'ITSYS CORPORATION',
    'summary': '',
    'website': 'https://www.it-syscorp.com',
    'depends': ['base',
                'contacts',
                ],
    'data': [
        'views/view.xml',

    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
