# -*- coding: utf-8 -*-
{
    'name': "Product Category Troubleshooting",

    'description': """
       product_category_troubleshooting
    """,

    'author': "IT Sys-Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','purchase','product'],

    'data': [
        'security/ir.model.access.csv',
        'views/product_category.xml',
        'views/troubleshooting_view.xml',
        
    ],

}
