# -*- coding: utf-8 -*-
from openerp import fields, models,api


class ProductCategory(models.Model):
    _inherit = 'product.category'

    troubleshooting_ids = fields.One2many('troubleshooting','product_categ',string='Troubleshooting')

