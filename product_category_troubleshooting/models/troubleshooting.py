# -*- coding: utf-8 -*-
from openerp import fields, models,api


class Troubleshooting(models.Model):
    _name = 'troubleshooting'
    _rec_name = 'question'
    product_categ = fields.Many2one('product.category')
    question = fields.Text(string="Questions")
    answer = fields.Text(string="Answer")