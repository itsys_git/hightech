# -*- coding: utf-8 -*-
{
    'name': "lot_serial_edit_issue_date",

    'author': "said",
    'website': "http://www.it-syscorp.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','stock','lot_serial_editing'],

    # always loaded
    'data': [

    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}