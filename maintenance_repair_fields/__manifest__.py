# -*- coding: utf-8 -*-
{
    'name': "Maintenance & Repair New Fields ",


    'author': "Marwa Ahmed",
    'website': "http://www.it-syscorp.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','fleet','repair','hr','helpdesk'],

    # always loaded
    'data': [
       'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}