from odoo import fields,models,api

class RepairOrder(models.Model):

    _inherit = 'repair.order'

    re_assign_to_employee_id=fields.Many2one('hr.employee',string='Assign To Employee')
    re_assign_to_fleet_id=fields.Many2one('fleet.vehicle.cost',string='Assign To Fleet')
    re_assign_to_dealers_id = fields.Many2one('dealers.dealers', string='Assign To Dealers')


class Dealers(models.Model):


     _name='dealers.dealers'
     name=fields.Char('Name')