# -*- coding: utf-8 -*-
{
    'name': "Rule Repair Employee",

    'summary': """
        each employee see repair belong to him""",

    'description': """
        each employee see repair belong to him
    """,

    'author': "ITsys-Corportion Eman ahmed",
    'website': "http://www.it-syscorp.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','repair','maintenance_repair_fields'],

    # always loaded
    'data': [
        'security/security.xml',

    ],
    

}