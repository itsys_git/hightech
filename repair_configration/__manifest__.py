# -*- coding: utf-8 -*-
{
    'name': "Repair Configuration",

    'author': "Marwa Ahmed",
    'website': "http://www.it-syscorp.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','sale','repair','helpdesk','hr_employee_edit'
                    ,'hr_employee_modify','hr',
                'employee_repair_editing','customer_profile_editing2',
                'repair_status_create'],

    # always loaded
    'data': [
       'security/ir.model.access.csv',
        'views/views.xml',

    ],

}