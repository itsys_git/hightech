# -*- coding: utf-8 -*-
from odoo import fields,models,api, _
from openerp.exceptions import ValidationError
import datetime


class LaborCodes(models.Model):
    _name = 'labor.codes'
    _rec_name = 'code_number'

    code = fields.Char(required=True,string='الكود')
    number=fields.Integer('الرقم',required=True)
    code_number=fields.Char(compute='compute_code',string='الكود/الرقم')
    desc=fields.Char('الوصف')

    @api.depends('code','number')
    def compute_code(self):
        for rec in self:
            if rec.code and rec.number:
                rec.code_number= str(rec.code) + str(rec.number)

    @api.multi
    @api.constrains('code', 'number',)
    def code_constraint_unique(self):
        for rec in self:
            if len(self.search([('code', '=', rec.code), ('number', '=', rec.number) ])) > 1:
                raise ValidationError(_('There are labor code  with same code and number'))

    @api.constrains('number')
    def number_not_minus(self):
        for rec in self:
            if rec.number < 0:
                raise ValidationError('Number is not minus')


class LaborSetting(models.Model):
    _name = 'labor.setting'
    _rec_name = 'labor_code_id'

    agent=fields.Float('الوكيل')
    customer=fields.Float('العميل')
    factory=fields.Float("المصنع")
    service_center=fields.Float("مركز الخدمه")
    guarantee_status = fields.Selection([('out_guarantee', 'خارج الضمان'),
                                         ('first_year_guarantee', 'ضمان السنة الاولى'),
                                         ('guarantee_after_year_inclusiv', 'ضمان بعد السنة الأولى شامل'),
                                         ('guarantee_after_year_not_inclusive','ضمان بعد السنة الأولى غير شامل')
                                         ],string='حاله الضمان')

    type_of_notice=fields.Selection([('normal_notice', 'بلاغ عادى'),
                                         ('repeat_14_day_complaint', 'تكرار شكوى خلال 14يوم'),
                                         ('repeat_complaint_after_14days','تكرار شكوى بعد 14يوم'),

                                         ],string='نوع البلاغ')

    labor_code_id=fields.Many2one("labor.codes",string='كود العماله')

    # _sql_constraints = [
    #     ('labor_setting_uniq', 'unique (guarantee_status, type_of_notice,labor_code_id)', 'labor code ,Guarantee Status  And Type of Notice Must be unique')
    # ]

    @api.constrains('agent','customer','factory','service_center')
    def labor_not_minus(self):
        for rec in self:
            if rec.agent < 0:
                raise ValidationError('Agent is not minus')
            if rec.customer < 0:
                raise ValidationError('Customer is not minus')

            if rec.factory < 0:
                raise ValidationError('Factory is not minus')

            if rec.service_center < 0:
                raise ValidationError('Service Center is not minus')




class areacollection(models.Model):
    _name = 'area.collection'
    _rec_name = 'governorate'

    governorate = fields.Many2one('city.model', string='المدينة')
    city=fields.Many2one("res.country.state",string='المحافظة')
    agent = fields.Float('الوكيل')
    customer = fields.Float("العميل")
    factory = fields.Float("المصنع")
    service_center = fields.Float("مركز الخدمه")

    @api.onchange('city')
    def change_city22(self):
        res = {}
        lis = []
        lis2 = []
        if self.city:
            se_city = self.env['city.model'].search([('state', '=', self.city.id)])
            for i in se_city:
                lis2.append(i.id)
            domain = [('id', 'in', lis2)]
            res['domain'] = {'governorate': domain}
        else:
            se_city = self.env['city.model'].search([])
            for i in se_city:
                lis.append(i.id)
            domain = [('id', 'in', lis)]
            res['domain'] = {'governorate': domain}
        return res

    service_center_id=fields.Many2one("hr.employee",string='اسم مركز الخدمه')

    guarantee_status = fields.Selection([('out_guarantee', 'خارج الضمان'),
                                         ('first_year_guarantee', 'ضمان السنة الاولى'),
                                         ('guarantee_after_year_inclusiv', 'ضمان بعد السنة الأولى شامل'),
                                         ('guarantee_after_year_not_inclusive','ضمان بعد السنة الأولى غير شامل')
                                         ],string='حاله الضمان')

    type_of_notice = fields.Selection([('normal_notice', 'بلاغ عادى'),
                                       ('repeat_14_day_complaint', 'تكرار شكوى خلال 14يوم'),
                                       ('repeat_complaint_after_14days', 'تكرار شكوى بعد 14يوم'),

                                       ],string='نوع البلاغ')

    _sql_constraints = [
        ('area_collection_uniq', 'unique (guarantee_status, type_of_notice,service_center_id,city,governorate)',
         'You Cant Duplicate Data')
    ]

    @api.constrains('agent', 'customer', 'factory', 'service_center')
    def area_not_minus(self):
        for rec in self:
            if rec.agent < 0:
                raise ValidationError('Agent is not minus')
            if rec.customer < 0:
                raise ValidationError('Customer is not minus')

            if rec.factory < 0:
                raise ValidationError('Factory is not minus')

            if rec.service_center < 0:
                raise ValidationError('Service Center is not minus')


class hightechcollection(models.Model):
    _name = 'hightech.collection'
    _rec_name = 'city'

    # governorate = fields.Many2one('city.model', string='المدينة')
    city = fields.Many2one("res.country.state", string='المحافظة')
    service_center_id=fields.Many2one("hr.employee",string='اسم مركز الخدمه')
    agent = fields.Float('الوكيل ')
    customer = fields.Float("العميل")
    factory = fields.Float("المصنع")
    service_center = fields.Float("مركز الخدمه")

    guarantee_status = fields.Selection([('out_guarantee', 'خارج الضمان'),
                                         ('first_year_guarantee', 'ضمان السنة الاولى'),
                                         ('guarantee_after_year_inclusiv', 'ضمان بعد السنة الأولى شامل'),
                                         ('guarantee_after_year_not_inclusive','ضمان بعد السنة الأولى غير شامل')
                                         ],string='حاله الضمان')


    type_of_notice = fields.Selection([('normal_notice', 'بلاغ عادى'),
                                       ('repeat_14_day_complaint', 'تكرار شكوى خلال 14يوم'),
                                       ('repeat_complaint_after_14days', 'تكرار شكوى بعد 14يوم'),

                                       ],string='نوع البلاغ')


    center_type = fields.Many2one("center.type")

    _sql_constraints = [
        ('area_collection_uniq','unique (guarantee_status,center_type, type_of_notice,city)',
         'You Cant Duplicate Data')
    ]


    @api.constrains('agent', 'customer', 'factory', 'service_center','guarantee_status','center_type'
                    ,'type_of_notice','city')
    def hightech_not_minus(self):
        for rec in self:
            if rec.agent < 0:
                raise ValidationError('Agent is not minus')
            if rec.customer < 0:
                raise ValidationError('Customer is not minus')

            if rec.factory < 0:
                raise ValidationError('Factory is not minus')

            if rec.service_center < 0:
                raise ValidationError('Service Center is not minus')
            if len(self.search([('guarantee_status', '=', rec.guarantee_status),('center_type', '=', rec.center_type.id),
                                ('type_of_notice', '=', rec.type_of_notice),('city', '=', rec.city.id)])) > 1:
                 raise ValidationError('You Cant Duplicate Data')


# class ProductProduct(models.Model):
#     _inherit = 'product.product'
#     labor_id=fields.Many2one("labor.codes",string='كود العماله')

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    labor_id=fields.Many2one("labor.codes",string='كود العماله')


class RepairOrderLine(models.Model):

    _inherit ='repair.line'


    labor_re_id=fields.Many2one("labor.codes",related='product_id.labor_id', string='كود العماله')
    customer_p = fields.Float(string="العميل",compute='check_guarantee_status')
    agent_p = fields.Float(string="الوكيل")
    factory_p = fields.Float(string="المصنع",compute='check_guarantee_status')
    service_center_p = fields.Float(string='مركز الخدمه')
    guarantee_status = fields.Selection([('damedge_guarantee', 'ضمان بتالف'),
                                         ('not_damedee_guarantee', 'ضمان بدون تالف'),
                                         ('in_pay', 'بالاجر')
                                         ],string='موقف التكلفة')

    @api.one
    @api.depends('guarantee_status')
    def check_guarantee_status(self):
        if self.guarantee_status=='damedge_guarantee':
            self.factory_p=self.product_id.lst_price
        elif self.guarantee_status=='not_damedee_guarantee':
            self.factory_p = self.product_id.lst_price
        elif self.guarantee_status=='in_pay':
            self.customer_p = self.product_id.lst_price





class RepairOrder(models.Model):

    _inherit = 'repair.order'

    customer_labor=fields.Float("العميل",readonly=True)
    agent_labor=fields.Float("الوكيل",readonly=True)
    factory_labor=fields.Float("المصنع",readonly=True)
    service_center_labor=fields.Float('مركز الخدمه',readonly=True)

    customer_area = fields.Float("العميل",readonly=True)
    agent_area = fields.Float("الوكيل",readonly=True)
    factory_area = fields.Float("المصنع",readonly=True)
    service_center_area = fields.Float('مركز الخدمه',readonly=True)

    customer_high = fields.Float("العميل",readonly=True)
    agent_high = fields.Float("الوكيل",readonly=True)
    factory_high = fields.Float("المصنع",readonly=True)
    service_center_high = fields.Float('مركز الخدمه',readonly=True)

    guarantee_status_p = fields.Selection([('out_guarantee', 'خارج الضمان'),
                                         ('first_year_guarantee', 'ضمان السنة الاولى'),
                                         ('guarantee_after_year_inclusiv', 'ضمان بعد السنة الأولى شامل'),
                                         ('guarantee_after_year_not_inclusive','ضمان بعد السنة الأولى غير شامل')
                                         ],string='حاله الضمان')

    @api.multi
    @api.depends('operations.labor_re_id')
    def calc_max_labor(self):
        return
        print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
        max_num=[]
        element=[]
        for lin in self:
            for rec in lin.operations:
                if rec.labor_re_id:
                    max_num.append(rec.labor_re_id.number)
                    element.append(rec.labor_re_id)
                    max_l=max(max_num)
                for line in element:
                    labor_obj=self.env['labor.codes'].search([('number', '=',max_l),('id','=',line.id)])
                    if labor_obj:
                        lin.labor_max=labor_obj

    @api.one
    @api.depends('customer_labor','agent_labor','factory_labor','service_center_labor')
    def calc_labor_total(self):
        for rec in self:
            rec.labor_total_collection =rec.customer_labor+rec.agent_labor+rec.factory_labor+rec.service_center_labor

    @api.one
    @api.depends('customer_area', 'agent_area', 'factory_area', 'service_center_area')
    def calc_area_total(self):
        for rec in self:
            rec.area_total_collection = rec.customer_area + rec.agent_area + rec.factory_area + rec.service_center_area

    @api.one
    @api.depends('customer_high', 'agent_high', 'factory_high', 'service_center_high')
    def calc_hightech_total(self):
        for rec in self:
            rec.hightech_total_collection = rec.customer_high + rec.agent_high + rec.factory_high + rec.service_center_high


    @api.depends('last_helpdesk_id')
    def determine_notice(self):
        for lin in self:
            date_now = fields.Date.today()
            if not lin.last_helpdesk_id or not lin.last_helpdesk_id.date_sheh:
              lin.type_of_notice = 'normal_notice'
            if lin.last_helpdesk_id and lin.last_helpdesk_id.date_sheh:
                diff = fields.Date.today() + datetime.timedelta(days=14) - lin.last_helpdesk_id.date_sheh
                print("diffffffff", diff.days)
                lin.lst_helpdesk_date=lin.last_helpdesk_id.date_sheh
                if diff.days <= 14:
                    lin.type_of_notice = 'repeat_14_day_complaint'
                if diff.days > 14:
                    lin.type_of_notice = 'repeat_complaint_after_14days'


    last_helpdesk_id=fields.Many2one("repair.order",string='Last Repair Order', domain="[('partner_id','=',partner_id)]")
    lst_helpdesk_date=fields.Date(string='Last Repair Date',compute='determine_notice')


    type_of_notice = fields.Selection([('normal_notice', 'بلاغ عادى'),
                                       ('repeat_14_day_complaint', 'تكرار شكوى خلال 14يوم'),
                                       ('repeat_complaint_after_14days', 'تكرار شكوى بعد 14يوم'),
                                       ],compute='determine_notice',string='موقف تكرار الشكوى')
    labor_max=fields.Many2one('labor.codes',string='اعلى كود عماله',compute='calc_max_labor')

    labor_total_collection = fields.Float('مجموع التحصيل على العماله', compute='calc_labor_total')
    area_total_collection = fields.Float('مجموع التحصيل على المنطقه', compute='calc_area_total')
    hightech_total_collection = fields.Float("مجموع التحصيلى على هاى تيك", compute='calc_hightech_total')

    @api.multi
    def show_total_collection(self):
        if self.operations:
            cust_tot_la = 0.0
            fact_tot_la = 0.0
            service_tot_la = 0.0
            agent_tot_la = 0.0

            cust_tot_area = 0.0
            fact_tot_area = 0.0
            service_tot_area = 0.0
            agent_tot_area = 0.0

            cust_tot_high = 0.0
            fact_tot_high = 0.0
            service_tot_high = 0.0
            agent_tot_high = 0.0
            for lin in self:
                if lin.guarantee_status_p and lin.type_of_notice :
                    labor_obj = self.env['labor.setting'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('labor_code_id', '=', lin.labor_max.id),
                        ('type_of_notice', '=', lin.type_of_notice), ])
                    if labor_obj:
                        for rec in labor_obj:
                            cust_tot_la += rec.customer
                            fact_tot_la += rec.factory
                            service_tot_la += rec.service_center
                            agent_tot_la += rec.agent
                        self.customer_labor = cust_tot_la
                        self.factory_labor = fact_tot_la
                        self.service_center_labor = service_tot_la
                        self.agent_labor = agent_tot_la

                if lin.guarantee_status_p:
                    area_obj = self.env['area.collection'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('type_of_notice', '=', lin.type_of_notice),
                        ('governorate', '=', lin.partner_id.city_id.id),
                        ('city', '=', lin.partner_id.state_id.id),
                        ('service_center_id', '=', lin.re_assign_to_employee_id.id)

                    ])
                    if area_obj:
                        for rec in area_obj:
                            cust_tot_area += rec.customer
                            fact_tot_area += rec.factory
                            service_tot_area += rec.service_center
                            agent_tot_area += rec.agent
                        self.customer_area = cust_tot_area
                        self.factory_area = fact_tot_area
                        self.service_center_area = service_tot_area
                        self.agent_area = agent_tot_area

                    high_obj = self.env['hightech.collection'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('type_of_notice', '=', lin.type_of_notice),
                        ('city', '=', lin.partner_id.state_id.id),
                        ('center_type','=',lin.re_assign_to_employee_id.center_type.id),
                    ])
                    if high_obj:
                        for rec in high_obj:
                            cust_tot_high += rec.customer
                            fact_tot_high += rec.factory
                            service_tot_high += rec.service_center
                            agent_tot_high += rec.agent
                        self.customer_high = cust_tot_high
                        self.factory_high = fact_tot_high
                        self.service_center_high = service_tot_high
                        self.agent_high = agent_tot_high

    @api.one
    @api.depends('amount_untaxed', 'amount_tax', 'labor_total_collection', 'area_total_collection',
                 'hightech_total_collection')
    def _amount_total(self):
        self.amount_total = self.pricelist_id.currency_id.round(
            self.amount_untaxed + self.amount_tax) + self.area_total_collection + self.labor_total_collection + self.hightech_total_collection

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    center_type = fields.Many2one('center.type',string='Center Type')



