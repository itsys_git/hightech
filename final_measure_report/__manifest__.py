# -*- coding: utf-8 -*-
{
    'name': "Final Measure Report",


    'description': """
       Final Measure Report
    """,
    'author': "Marwa Ahmed",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','repair'],

    'data': [
        'views/views.xml',
        'views/templates.xml',
    ],

}