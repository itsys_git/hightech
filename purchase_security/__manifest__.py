# -*- coding: utf-8 -*-
{
    'name': "purchase security",

    'description': """
      appear unit price to manager
    """,

    'author': "IT Systems Corportion Eman Ahmed",
    'website': "http://www.it-syscorp.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase'],

    # always loaded
    'data': [
        'views/purchase_order_views.xml',

        
    ],

}
