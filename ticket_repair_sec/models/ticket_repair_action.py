# -*- coding: utf-8 -*-

from odoo import models, api


class TicketRepairSecurity(models.Model):
    _name = 'fix.ticket.repair.errors'

    @api.model
    def open_tickets(self):
        if self.env.user.has_group('hr_employee_modify.customer_service_group'):
            user = self.env.user
            all_tickets = []
            ticket_has_my_repairs = self.env['repair.order'].sudo().search(
                ['|', '|', ('re_assign_to_employee_id.user_id', '=', user.id),
                 ('re_assign_to_employee_id.service_center.user_id', '=', user.id), ('create_uid', 'in', [user.id])])

            for item in ticket_has_my_repairs:
                all_tickets.append(item.helpdesk_ticket_id.id)
            my_tickets = self.env['helpdesk.ticket'].sudo().search([('create_uid', '=', user.id)])._ids
            all_tickets += my_tickets
            ids = list(set(all_tickets))
        else:
            ids = self.env['helpdesk.ticket'].sudo().search([])._ids
        domain = "[('id','in',[" + ','.join(map(str, ids)) + "])]"
        action = self.env.ref('helpdesk.helpdesk_ticket_action_main_tree').read()[0]
        action.update({'domain': domain})
        return action

    @api.model
    def open_repairs(self):
        if self.env.user.has_group('hr_employee_modify.customer_service_group'):
            user = self.env.user
            ids = self.env['repair.order'].sudo().search(
                ['|', '|', ('re_assign_to_employee_id.user_id', '=', user.id),
                 ('re_assign_to_employee_id.service_center.user_id', '=', user.id),
                 ('create_uid', 'in', [user.id])])._ids
        else:
            ids = self.env['repair.order'].sudo().search([])._ids
        domain = "[('id','in',[" + ','.join(map(str, ids)) + "])]"
        action = self.env.ref('repair.action_repair_order_tree').read()[0]
        action.update({'domain': domain})
        return action


TicketRepairSecurity()
