# -*- coding: utf-8 -*-
{
    'name': "Ticket Security",

    'summary': """
        Create new menus for tickets to fix security problem """,

    'description': """
        Fix issue in ticket security 
    """,
    'category': 'Helpdesk',
    'version': '0.1',

    'depends': ['base', 'helpdesk', 'hr_employee_modify', 'repair', 'stock'],

    'data': [
        'security/security.xml',
        'views/ticket_repair_action_view.xml',

    ],

}
