from odoo import models, fields, api


class Productproduct(models.Model):
    _inherit = 'product.product'

    lot_ids = fields.Many2many('stock.production.lot','l6','l7',domain="[('product_id','=',id)]")

class ProductCategory(models.Model):

    _inherit = 'product.category'


    product_class_id=fields.Many2one("product.class.lot.serial",string='Product Class')
    product_capacity=fields.Integer('Product Capacity')
    product_model_id=fields.Many2one("product.model.lot.serial",string='Product Model')
    description=fields.Text("Description")
    dealers_id=fields.Many2one("res.partner",string='Dearlers')
    serial_dealers_name=fields.Char("Dealers Number")
    issue_date=fields.Date('Issue Date')


class StockProductionLot(models.Model):
    _inherit ='stock.production.lot'

    product_class_id = fields.Many2one('product.class.lot.serial', 'Product Class',compute='onchange_product_id44')
    product_capacity = fields.Integer(compute='onchange_product_id44')
    product_model_id = fields.Many2one('product.model.lot.serial', 'Product Model',compute='onchange_product_id44')
    description = fields.Text(compute='onchange_product_id44')
    dealer_id = fields.Many2one('res.partner', 'Dealer',compute='onchange_product_id44')
    serial_dealer_name = fields.Char(compute='onchange_product_id44',string="Dealers Number")
    issue_date = fields.Date()
    product_family = fields.Many2one('product.category',compute='onchange_product_id44')

    @api.depends('product_id')
    def onchange_product_id44(self):
        for rec in self:

            rec.product_class_id=rec.product_id.categ_id.product_class_id
            rec.product_capacity=rec.product_id.categ_id.product_capacity
            rec.product_model_id=rec.product_id.categ_id.product_model_id
            rec.description=rec.product_id.categ_id.description
            rec.dealer_id=rec.product_id.categ_id.dealers_id
            rec.serial_dealer_name= rec.product_id.categ_id.serial_dealers_name
            rec.issue_date=rec.product_id.categ_id.issue_date
            rec.product_family=rec.product_id.categ_id




class RepairOrderLine(models.Model):

    _inherit = 'repair.line'

    collect_status_id=fields.Many2one("collect.status",'Collecting Status')
    parts_status_id = fields.Many2one("part.status", 'Parts Status')
    repair_reason_id = fields.Many2one("repair.reason", 'Changing Reason')


class PartStatus(models.Model):
     _name = 'part.status'
     _rec_name = 'name'

     name = fields.Char()

class CollectStaus(models.Model):

     _name = 'collect.status'
     _rec_name = 'name'

     name = fields.Char()


class RepairReson(models.Model):
     _name = 'repair.reason'
     _rec_name = 'reason'

     reason = fields.Char()











