# -*- coding: utf-8 -*-
{
    'name': "Editing Lot",

    'author': "Marwa Ahmed",
    'website': "http://www.it-syscorp.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','repair','sale_management','ticket_customer','hr_employee_edit'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',

    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}