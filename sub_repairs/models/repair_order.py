# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime


class RepairOrder(models.Model):
    _inherit ='repair.order'
    sup_repair_id = fields.Many2one('repair.order',string='Parent Repair Order',readonly=True)
    # compute = 'change_fields_id', store = True
    sub_repair_order_count = fields.Integer(compute='_compute_subrepair_order_count', string='sub repair Order Count')
    repair_or_sub = fields.Selection([('repair', 'Repair'), ('sub', 'Sub Repair'),],string="Repair/Sub Repair")
    # repair_or_sub = fields.Char(string="Repair/Sub Repair",readonly=True)

    @api.onchange('sup_repair_id')
    def change_fields_id(self):
        if self.sup_repair_id:
            print('aaaaaaaaaaaaaaa')
            self.helpdesk_ticket_id = self.sup_repair_id.helpdesk_ticket_id
            self.repairs_status_id = self.sup_repair_id.repairs_status_id
            self.product_id = self.sup_repair_id.product_id
            self.product_qty =self.sup_repair_id.product_qty
            self.product_uom = self.sup_repair_id.product_uom
            self.lot_id = self.sup_repair_id.lot_id
            self.product_uom = self.sup_repair_id.product_uom
            self.lot_id = self.sup_repair_id.lot_id
            self.partner_id = self.sup_repair_id.partner_id
            self.re_assign_to_employee_id = self.sup_repair_id.re_assign_to_employee_id
            self.re_assign_to_fleet_id = self.sup_repair_id.re_assign_to_fleet_id
            self.re_assign_to_dealers_id = self.sup_repair_id.re_assign_to_dealers_id
            self.location_id = self.sup_repair_id.location_id
            self.guarantee_limit = self.sup_repair_id.guarantee_limit
            self.invoice_method = self.sup_repair_id.invoice_method
            self.guarantee_limit = self.sup_repair_id.guarantee_limit
            self.invoice_method = self.sup_repair_id.invoice_method
            self.repair_or_sub = 'sub'
        else:
            self.repair_or_sub = 'repair'




    def _compute_subrepair_order_count(self):
        repair_data = self.env['repair.order'].read_group(
            [('sup_repair_id', 'in', self.ids)],
            ['sup_repair_id'], ['sup_repair_id'])
        result = dict((data['sup_repair_id'][0], data['sup_repair_id_count']) for data in repair_data)
        for repair in self:
            repair.sub_repair_order_count = result.get(repair.id, 0)

