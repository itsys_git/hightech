# -*- coding: utf-8 -*-
{
    'name': "Sub Repairs",

    'summary': """
        sub_repairs """,

    'description': """
        sub_repairs
    """,

    'author': "ITsys-Corportion Doaa Khaled",
    'website': "http://www.it-syscorp.com",

    'category': 'Sale',
    'version': '0.1',

    'depends': ['base','repair','repair_status'],

    'data': [
        # 'security/ir.model.access.csv',
        'views/repair_order_view.xml',

    ],

}