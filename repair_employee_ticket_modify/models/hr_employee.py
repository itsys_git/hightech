# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime
class HrEmployee(models.Model):
    _inherit ='hr.employee'
    city_ids=fields.Many2many('city.model','emp_id','emp2')

    @api.onchange('status_ids')
    def change_city(self):
        res = {}
        lis = []
        lis2 = []
        lis3=[]
        if self.status_ids:
            for l in self.status_ids:
                lis3.append(l.id)
            se_city = self.env['city.model'].search([('state', 'in', lis3)])
            for i in se_city:
                lis2.append(i.id)
            domain = [('id', 'in', lis2)]
            res['domain'] = {'city_ids': domain}
        else:
            se_city = self.env['city.model'].search([])
            for i in se_city:
                lis.append(i.id)
            domain = [('id', 'in', lis)]
            res['domain'] = {'city_ids': domain}
        return res

    my_domain = fields.Many2many(comodel_name='city.model', string='Domain', relation='city_model_ress_emp',
                                 column1='hr_emp1', column2='hr_emp2', compute='_get_domain_str')

    @api.one
    def _get_domain_str(self):
        res = {}
        lis = []
        lis2 = []
        lis3 = []
        domain = []
        for item in self:

            if item.status_ids:
                for l in item.status_ids:
                    lis3.append(l.id)
                se_city = self.env['city.model'].search([('state', 'in', lis3)])
                for i in se_city:
                    lis2.append(i.id)
                domain = [('id', 'in', lis2)]
            else:
                se_city = self.env['city.model'].search([])
                for i in se_city:
                    lis.append(i.id)
                domain = [('id', 'in', lis)]

            if domain:
                item.my_domain = domain[0][2]
            else:
                item.my_domain = self.env['city.model'].sudo().search([]).ids

    # def _sync_user(self, user):
    #         vals = dict(
    #             # name=user.name,
    #             image=user.image,
    #             work_email=user.email,
    #         )
    #         if user.tz:
    #             vals['tz'] = user.tz
    #         return vals

# class CityModel(models.Model):
#     _inherit='city.model'
#     emp_id=fields.Many2one('hr.employee')