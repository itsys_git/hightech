# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime


class RepairOrder(models.Model):
    _inherit = 'repair.order'

    # partner_id = fields.Many2one(
    #     'res.partner', 'Customer',
    #     index=True, states={'confirmed': [('readonly', True)]},
    #     help='Choose partner for whom the order will be invoiced and delivered. You can find a partner by its Name, TIN, Email or Internal Reference.'
    #     , copy=False)

    # re_assign_to_employee_id = fields.Many2one('hr.employee', string='Assign To Employee', copy=False)

    # helpdesk_ticket_id = fields.Many2one('helpdesk.ticket', 'Helpdesk Ticket', copy=False)
    my_domain = fields.Many2many(comodel_name='hr.employee',string='Domain',relation='hr_employee_ress_repair',column1='repair_id',column2='employee_id', compute='_get_domain_str')

    @api.one
    def _get_domain_str(self):
        return
        values1 = []
        values = []
        lis = []
        res = {}
        list2 = []
        l_final = []
        domain1 = []
        lsl = []
        for item in self:
            if item.product_id and item.helpdesk_ticket_id:
                print('-----------------------------')
                if item.product_id == item.helpdesk_ticket_id.product_id:
                    for lin in item.product_id.categ_id.employee_list_ids:
                        values1.append(lin.id)

            if values1:
                for lin in values1:
                    employee_state2 = self.env['hr.employee'].search(
                        [('id', '=', lin), ('city_ids', 'in', item.partner_id.city_id.id)])
                    for l in employee_state2:
                        values.append(l.id)

            if values:
                for lin in values:
                    employee_state = self.env['hr.employee'].search(
                        ['|', ('warranty_type', '=', False),('warranty_type', '=', item.helpdesk_ticket_id.warranty_type.id),
                         '|', ('ticket_type_id', '=', False),('ticket_type_id', '=', item.helpdesk_ticket_id.ticket_type_id.id),
                         '|', ('priority', '=', False), ('priority', '=', item.helpdesk_ticket_id.priority_new.id),
                         '|',('repair_type_id', '=', item.repair_type_id.id),('repair_type_id', '=', False),])

                    print('jjjj aaaa',employee_state)
                    lis_emp = []
                    if employee_state:
                        for l in employee_state:
                            lis_emp.append(l.id)
                        print('l', lis_emp, values)
                        for li in lis_emp:
                            if li == lin:
                                lis.append(li)
                        if lis:
                            domain1 = [('id', 'in', lis)]
                        else:
                            domain1 = [('id', 'in', lis)]
                    else:
                        domain1 = [('id', 'in', lis)]
            if domain1:
                item.my_domain = domain1[0][2]
            else:
                item.my_domain = self.env['hr.employee'].sudo().search([]).ids

    @api.multi
    def show_remaind(self):
        values = []
        values1 = []
        lis = []
        res = {}
        # if self.product_id and self.helpdesk_ticket_id:
        #     print('-----------------------------')
        #     if self.product_id == self.helpdesk_ticket_id.product_id:
        #         for lin in self.product_id.categ_id.employee_list_ids:
        #             values1.append(lin.id)
        #
        # if not self.re_assign_to_employee_id:
        #     print ('fffffffff')
        #     if values1:
        #         for lin in values1:
        #             employee_state2 = self.env['hr.employee'].search(
        #                 [('id', '=', lin), ('city_ids', 'in', self.partner_id.city_id.id)])
        #             for l in employee_state2:
        #                 values.append(l.id)
        #
        #
        #
        #     if values:
        if not self.re_assign_to_employee_id:
            if self.employee_ids:
                self.employee_ids.unlink()
            x = self.change_values()
            if x: #self.change_values():
                # x = self.change_values()
                if x['domain']['re_assign_to_employee_id']:
                    y = x['domain']['re_assign_to_employee_id'][0][2]
                    for lin in y:
                        employee_state = self.env['hr.employee'].search([('id', '=', lin)])

                        if employee_state:
                            for l in employee_state:
                                if self.date_from and self.date_to:
                                    if self.date_to < self.date_from:
                                        raise ValidationError("Date To must be bigger than Date From")
                                    else:
                                        difference = self.date_to - self.date_from
                                        days = difference.days

                                        for day in range(0, days + 1):
                                            print('day', day)
                                            date = self.date_from + datetime.timedelta(days=day)
                                            print('date', date)
                                            employee_capacity = self.env['employee.capacity'].search(
                                                [('employee_id', '=', l.id), ('date', '=', date)])
                                            print('cap', employee_capacity)
                                            reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                            if employee_capacity:
                                                self.write({'employee_ids': [(0, 6,
                                                                              {'employee_id': l.id,
                                                                               'cap_count': reminder_capacity,
                                                                               'date': date})]})
                                            else:
                                                self.write({'employee_ids': [(0, 6,
                                                                              {'employee_id': l.id,
                                                                               'cap_count': l.capacity,
                                                                               'date': date})]})

                                if self.date_from and not self.date_to:
                                    employee_capacity = self.env['employee.capacity'].search(
                                        [('employee_id', '=', l.id), ('date', '=', self.date_from)])
                                    print('cap', employee_capacity)
                                    reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                    if employee_capacity:
                                        self.write({'employee_ids': [(0, 6,
                                                                      {'employee_id': l.id,
                                                                       'cap_count': reminder_capacity,
                                                                       'date': self.date_from})]})
                                    else:
                                        self.write({'employee_ids': [(0, 6,
                                                                      {'employee_id': l.id,
                                                                       'cap_count': l.capacity,
                                                                       'date': self.date_from})]})
                                if not self.date_from and not self.date_to:
                                    self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                                    self.date_to = fields.Date.today() + datetime.timedelta(days=6)
                                    if self.date_to < self.date_from:
                                        raise ValidationError("Date To must be bigger than Date From")
                                    else:
                                        difference = self.date_to - self.date_from
                                        days = difference.days

                                        for day in range(0, days + 1):
                                            print('day', day)
                                            date = self.date_from + datetime.timedelta(days=day)
                                            print('date', date)
                                            employee_capacity = self.env['employee.capacity'].search(
                                                [('employee_id', '=', l.id), ('date', '=', date)])
                                            print('cap', employee_capacity)
                                            reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                            if employee_capacity:
                                                self.write({'employee_ids': [(0, 6,
                                                                              {'employee_id': l.id,
                                                                               'cap_count': reminder_capacity,
                                                                               'date': date})]})
                                            else:
                                                self.write({'employee_ids': [(0, 6,
                                                                              {'employee_id': l.id,
                                                                               'cap_count': l.capacity,
                                                                               'date': date})]})
                                if not self.date_from and self.date_to:
                                    self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                                    if self.date_from and self.date_to:
                                        if self.date_to < self.date_from:
                                            raise ValidationError("Date To must be bigger than Date From")
                                        else:
                                            difference = self.date_to - self.date_from
                                            days = difference.days

                                            for day in range(0, days + 1):
                                                print('day', day)
                                                date = self.date_from + datetime.timedelta(days=day)
                                                print('date', date)
                                                employee_capacity = self.env['employee.capacity'].search(
                                                    [('employee_id', '=', l.id), ('date', '=', date)])
                                                print('cap', employee_capacity)
                                                reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                                if employee_capacity:
                                                    self.write({'employee_ids': [(0, 6,
                                                                                  {'employee_id': l.id,
                                                                                   'cap_count': reminder_capacity,
                                                                                   'date': date})]})
                                                else:
                                                    self.write({'employee_ids': [(0, 6,
                                                                                  {'employee_id': l.id,
                                                                                   'cap_count': l.capacity,
                                                                                   'date': date})]})

        else:
            print('nhggggggggggggggggggg')
            if self.employee_ids:
                self.employee_ids.unlink()
            employee_state = self.env['hr.employee'].search(
                [('id', '=', self.re_assign_to_employee_id.id)])

            if employee_state:
                for l in employee_state:
                    if self.date_from and self.date_to:
                        if self.date_to < self.date_from:
                            raise ValidationError("Date To must be bigger than Date From")
                        else:
                            difference = self.date_to - self.date_from
                            days = difference.days

                            for day in range(0, days + 1):
                                print('day', day)
                                date = self.date_from + datetime.timedelta(days=day)
                                print('date', date)
                                employee_capacity = self.env['employee.capacity'].search(
                                    [('employee_id', '=', l.id), ('date', '=', date)])
                                print('cap', employee_capacity)
                                reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                if employee_capacity:
                                    self.write({'employee_ids': [(0, 6,
                                                                  {'employee_id': l.id,
                                                                   'cap_count': reminder_capacity,
                                                                   'date': date})]})
                                else:
                                    self.write({'employee_ids': [(0, 6,
                                                                  {'employee_id': l.id,
                                                                   'cap_count': l.capacity,
                                                                   'date': date})]})

                    if self.date_from and not self.date_to:
                        employee_capacity = self.env['employee.capacity'].search(
                            [('employee_id', '=', l.id), ('date', '=', self.date_from)])
                        print('cap', employee_capacity)
                        reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                        if employee_capacity:
                            self.write({'employee_ids': [(0, 6,
                                                          {'employee_id': l.id,
                                                           'cap_count': reminder_capacity,
                                                           'date': self.date_from})]})
                        else:
                            self.write({'employee_ids': [(0, 6,
                                                          {'employee_id': l.id,
                                                           'cap_count': l.capacity,
                                                           'date': self.date_from})]})
                    if not self.date_from and not self.date_to:
                        self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                        self.date_to = fields.Date.today() + datetime.timedelta(days=6)
                        if self.date_to < self.date_from:
                            raise ValidationError("Date To must be bigger than Date From")
                        else:
                            difference = self.date_to - self.date_from
                            days = difference.days

                            for day in range(0, days + 1):
                                print('day', day)
                                date = self.date_from + datetime.timedelta(days=day)
                                print('date', date)
                                employee_capacity = self.env['employee.capacity'].search(
                                    [('employee_id', '=', l.id), ('date', '=', date)])
                                print('cap', employee_capacity)
                                reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                if employee_capacity:
                                    self.write({'employee_ids': [(0, 6,
                                                                  {'employee_id': l.id,
                                                                   'cap_count': reminder_capacity,
                                                                   'date': date})]})
                                else:
                                    self.write({'employee_ids': [(0, 6,
                                                                  {'employee_id': l.id,
                                                                   'cap_count': l.capacity,
                                                                   'date': date})]})
                    if not self.date_from and self.date_to:
                        self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                        if self.date_from and self.date_to:
                            if self.date_to < self.date_from:
                                raise ValidationError("Date To must be bigger than Date From")
                            else:
                                difference = self.date_to - self.date_from
                                days = difference.days

                                for day in range(0, days + 1):
                                    print('day', day)
                                    date = self.date_from + datetime.timedelta(days=day)
                                    print('date', date)
                                    employee_capacity = self.env['employee.capacity'].search(
                                        [('employee_id', '=', l.id), ('date', '=', date)])
                                    print('cap', employee_capacity)
                                    reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                    if employee_capacity:
                                        self.write({'employee_ids': [(0, 6,
                                                                      {'employee_id': l.id,
                                                                       'cap_count': reminder_capacity,
                                                                       'date': date})]})
                                    else:
                                        self.write({'employee_ids': [(0, 6,
                                                                      {'employee_id': l.id,
                                                                       'cap_count': l.capacity,
                                                                       'date': date})]})

    # @api.multi
    # def read(self, fields=None, load='_classic_read'):
    #     res=super(RepairOrder, self).read(fields=fields, load=load)
    #
    #     self.change_values()
    #     return res

    # @api.onchange('partner_id', 'product_id', 'helpdesk_ticket_id', 'repair_type_id')
    def change_values(self):
        values1 = []
        values = []
        lis = []
        res = {}
        list2 = []
        l_final = []
        domain1 = []
        domain2 = []
        lsl = []
        self.re_assign_to_employee_id=False
        if self.product_id and self.helpdesk_ticket_id:
            print('-----------------------------')
            if self.product_id == self.helpdesk_ticket_id.product_id:
                for lin in self.product_id.categ_id.employee_list_ids:
                    values1.append(lin.id)

        if values1:
            for lin in values1:
                employee_state2 = self.env['hr.employee'].search(
                    [('id', '=', lin), ('city_ids', 'in', self.partner_id.city_id.id)])
                for l in employee_state2:
                    values.append(l.id)

        if values:
            for lin in values:
                # employee_state = self.env['hr.employee'].search(
                #     [('warranty_type', '=', self.helpdesk_ticket_id.warranty_type.id),
                #      ('ticket_type_id', '=', self.helpdesk_ticket_id.ticket_type_id.id),
                #      ('priority', '=', self.helpdesk_ticket_id.priority_new.id),
                #      ('repair_type_id', '=', self.repair_type_id.id)])

                employee_state = self.env['hr.employee'].search(
                    ['|',('warranty_type', '=', False),('warranty_type', '=', self.helpdesk_ticket_id.warranty_type.id),
                     '|',('ticket_type_id', '=', False),('ticket_type_id','=', self.helpdesk_ticket_id.ticket_type_id.id),
                    '|',('priority', '=', False),('priority', '=', self.helpdesk_ticket_id.priority_new.id),
                     '|',('repair_type_id','=',  self.repair_type_id.id),('repair_type_id', '=',False),
                     ])

                print('jjjjjjjjjjfff',self.repair_type_id)
                lis_emp = []
                if employee_state:
                    for l in employee_state:
                        lis_emp.append(l.id)
                    print('l', lis_emp, values)
                    for li in lis_emp:
                        if li == lin:
                            print(li, lin)
                            lis.append(li)
                    print(lis)
                    if lis:
                        domain1 = [('id', 'in', lis)]
                    else:
                        domain1 = [('id', 'in',  lis)]
                else:
                    domain1 = [('id', 'in',  lis)]
        # if self.partner_id:
        #
        #     search_lot = self.env['stock.production.lot'].search([('partner_id', '=', self.partner_id.id)])
        #     # print (search_lot.product_id)
        #     if search_lot:
        #         for line in search_lot:
        #             if line.product_id.spare_parts == False:
        #                 list2.append(line.product_id.id)
        #
        #     if list2:
        #         domain2 = [('id', 'in', list2)]
        #     else:
        #         search_lot = self.env['product.product'].search([('spare_parts', '=', False)])
        #         for l in search_lot:
        #             lsl.append(l.id)
        #         domain2 = [('id', 'in', lsl)]
        res['domain'] = {'re_assign_to_employee_id': domain1}

        return res

    @api.onchange('partner_id')
    def change_values2(self):
        values1 = []
        values = []
        lis = []
        res = {}
        list2 = []
        l_final = []
        domain1 = []
        domain2 = []
        lsl = []

        if self.partner_id:

            search_lot = self.env['stock.production.lot'].search([('partner_id', '=', self.partner_id.id)])
            # print (search_lot.product_id)
            if search_lot:
                for line in search_lot:
                    if line.product_id.spare_parts == False:
                        list2.append(line.product_id.id)

            if list2:
                domain2 = [('id', 'in', list2)]
            else:
                search_lot = self.env['product.product'].search([('spare_parts', '=', False)])
                for l in search_lot:
                    lsl.append(l.id)
                domain2 = [('id', 'in', lsl)]
        res['domain'] = {'product_id': domain2}

        return res

    @api.model
    def create(self, vals):
        res = super(RepairOrder, self).create(vals)
        for rec in res:
            if rec.date_sheh and rec.re_assign_to_employee_id:
                if rec.employee_ids:
                    rec.employee_ids.unlink()
                    # for l in rec.employee_ids:

                    # if l.date == rec.date_sheh and l.employee_id == rec.re_assign_to_employee_id:
                    #     print('lll', rec.date_sheh,rec.re_assign_to_employee_id)
                    #     pass
                    # else:
                    #   rec.write({'employee_ids': [(3,l.id)]})

        return res

    @api.multi
    def write(self, vals):
        res = super(RepairOrder, self).write(vals)
        for rec in self:
            if rec.date_sheh and rec.re_assign_to_employee_id:
                if rec.employee_ids:
                    rec.employee_ids.unlink()
                    # for l in rec.employee_ids:
                    #
                    #     if l.date == rec.date_sheh and l.employee_id == rec.re_assign_to_employee_id:
                    #         print('lll', rec.date_sheh,rec.re_assign_to_employee_id)
                    #         pass
                    #     else:
                    #       rec.write({'employee_ids': [(3,l.id)]})

        return res