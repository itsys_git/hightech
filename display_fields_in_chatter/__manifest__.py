# -*- coding: utf-8 -*-
{
    'name': "Display Fields In Chatter",

    'summary': """
        Display all repair fields in chatter and log""",

    'author': "Marwa Ahmed",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','repair','stock_no_negative'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],

}