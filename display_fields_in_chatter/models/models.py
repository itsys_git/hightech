# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import time

class RepairOreder(models.Model):
    _inherit = 'repair.order'


    product_id = fields.Many2one(
        'product.product', string='Product to Repair',
        readonly=True, required=True,track_visibility='onchange',  track_sequence=2, states={'draft': [('readonly', False)]})

    partner_id = fields.Many2one(
        'res.partner', 'Customer',
        index=True,track_visibility='onchange',  track_sequence=1,states={'confirmed': [('readonly', True)]},
        help='Choose partner for whom the order will be invoiced and delivered. You can find a partner by its Name, TIN, Email or Internal Reference.')

    helpdesk_ticket_id = fields.Many2one('helpdesk.ticket', 'Helpdesk Ticket',track_visibility='onchange',  track_sequence=3)
    helpdesk_name = fields.Char(related='helpdesk_ticket_id.name',string='Ticket Name',track_visibility='onchange',  track_sequence=4)

    amount_tax = fields.Float('Taxes', compute='_amount_tax', store=True,track_visibility='onchange',  track_sequence=6)
    amount_total = fields.Float('Total', compute='_amount_total', store=True,track_visibility='onchange',  track_sequence=7)
    amount_untaxed = fields.Float('Untaxed Amount', compute='_amount_untaxed', store=True,track_visibility='onchange',  track_sequence=5)
    lot_id = fields.Many2one(
        'stock.production.lot', 'Lot/Serial',
        domain="[('product_id','=', product_id)]",track_visibility='onchange',
        help="Products repaired are all belonging to this lot", oldname="prodlot_id")

    technition = fields.Many2one('hr.employee',string='technician',track_visibility='onchange')
    re_assign_to_employee_id=fields.Many2one('hr.employee',string='Center Name',track_visibility='onchange')

    @api.model
    def _default_stock_location(self):
        warehouse = self.env['stock.warehouse'].search([], limit=1)
        if warehouse:
            return warehouse.lot_stock_id.id
        return False

    location_id = fields.Many2one(
        'stock.location', 'Location',
        default=_default_stock_location,
        index=True, readonly=True, required=True,track_visibility='onchange',
        help="This is the location where the product to repair is located.",
        states={'draft': [('readonly', False)], 'confirmed': [('readonly', True)]})

    last_helpdesk_id=fields.Many2one("repair.order",string='Last Repair Order', domain="[('partner_id','=',partner_id)]",track_visibility='onchange',track_sequence=10)
    lst_helpdesk_date=fields.Date(string='Last Repair Date',compute='determine_notice',track_visibility='onchange',track_sequence=11)

    type_of_notice = fields.Selection([('normal_notice', 'بلاغ عادى'),
                                       ('repeat_14_day_complaint', 'تكرار شكوى خلال 14يوم'),
                                       ('repeat_complaint_after_14days', 'تكرار شكوى بعد 14يوم'),
                                       ], compute='determine_notice' ,track_visibility='onchange',string='موقف تكرار الشكوى')
    internal_notes = fields.Text('Internal Notes',track_visibility='onchange',track_sequence=9)
    quotation_notes = fields.Text('Quotation Notes',track_visibility='onchange',  track_sequence=10)
    labor_max=fields.Many2one('labor.codes',string='اعلى كود عماله',compute='calc_max_labor',track_visibility='onchange')

    guarantee_status_p = fields.Selection([('out_guarantee', 'خارج الضمان'),
                                           ('first_year_guarantee', 'ضمان السنة الاولى'),
                                           ('guarantee_after_year_inclusiv', 'ضمان بعد السنة الأولى شامل'),
                                           ('guarantee_after_year_not_inclusive', 'ضمان بعد السنة الأولى غير شامل')
                                           ] ,track_visibility='onchange',string='حاله الضمان')


    # ivr = fields.Integer(related='repairs_status_id.ivr',String="IVR" ,track_visibility='onchange')

    date_from = fields.Date(readonly=False, default=fields.Date.context_today,track_visibility='onchange',track_sequence=14)
    date_to = fields.Date(track_visibility='onchange',readonly=False, default=fields.Date.today() + datetime.timedelta(days=6),track_sequence=15)
