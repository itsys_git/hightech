# -*- coding: utf-8 -*-

{
    'name': 'Stock Inventory Valuation Report',
    'version': '2.0',
    'category': 'stock',
    'summary': 'Stock Valuation Report in XLS/PDF',
    'description': """
Stock Valuation Report on Date,
----------------------------------
""",
    'depends': ['stock'],
    'data': [
        'security/stock_valuation_security.xml',
        'wizard/stock_valuation.xml',
        'views/stock_valuation_menu.xml',
        'views/stock_valuation_template.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
