# -*- coding: utf-8 -*-
{
    'name': "reject confirm quoation",

    'summary': """
        Reject service center sales user to confirm sale order""",


    'author': "Marwa Ahmed",


    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','modification_on_hightech','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/security.xml',
        'views/views.xml',
        'views/templates.xml',
    ],

}