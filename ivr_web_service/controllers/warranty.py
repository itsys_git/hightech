# -*- coding: utf-8 -*-

import json
from odoo import http,fields
from odoo.http import request, Controller
import datetime
from dateutil.relativedelta import relativedelta


class active_create_ticket(Controller):
    @http.route('/active_create_ticket', auth='public', cors='*')
    def active_create_ticket(self, **args):
        # http://localhost:1212/active_create_ticket?customerid=7&device_type=ddccc&device_serial=0000049&type=1
        customerid = args.get('customerid')
        code = args.get('device_type')
        lot = args.get('device_serial')
        type = args.get('type')
        result = {'result': False, 'data': {'date': False}}
        self.create_log('create_ticket_warranty', False,False,False,customerid,lot,code)

        print(customerid, code, lot, type)
        if not customerid or not code or not lot and not type:
            result = {'result': False, 'data': {'date': False}}
        lotobj = request.env['stock.production.lot'].sudo().search([('name', '=', lot)], limit=1)
        customerobj = request.env['res.partner'].sudo().search([('id', '=', int(customerid))], limit=1)
        # print(lotobj,customerobj)
        if not customerobj or not lotobj:
            result = {'result': False, 'data': {}}
        if int(type) == 1:
            self.create_log('warranty', False, customerobj.id, lotobj.id, customerobj.id, lotobj.id, code)

            result = self.warranty(lotobj)

        if int(type) == 2:
            self.create_log('create_ticket', False, customerobj.id, lotobj.id, customerobj.id, lotobj.id, code)

            result = self.create_ticket(customerobj, lotobj, code)

        return http.request.make_response(json.dumps(result), {
            'Cache-Control': 'no-cache',
            'Content-Type': 'JSON; charset=utf-8',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
        })

    def warranty(self, lotobj):
        # DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

        current_date = datetime.datetime.now().date()
        new_date = current_date
        new_date = lotobj.issue_date.replace(lotobj.issue_date.year + 1, lotobj.issue_date.month, lotobj.issue_date.day)
        # print(current_date,new_date,lotobj.issue_date)
        if new_date >= current_date :


        # if lotobj.issue_date >= current_date and lotobj.issue_date <= new_date:
            lotobj.warranty = True
            return {'result': lotobj.warranty, 'data': {'date': False}}
        else:
            return {'result': lotobj.warranty, 'data': {'date': False}}

            # lotobj
            # return {'result': False, 'data': {}}

    def create_ticket(self, customerobj, lotobj, code):
        if not lotobj.product_id:
            return {'result': False, 'data': {}}

        repairobj = request.env['repair.order'].sudo().create(
            {
                'partner_id': customerobj.id,
                'lot_id': lotobj.id,
                'product_id': lotobj.product_id.id,
                'product_uom': lotobj.product_id.uom_id.id,

            }
        )
        if not repairobj:
            return {'result': False, 'data': {}}
        repairobj = repairobj[0]

        re_assign_to_employee_id, date = self.get_employee_date(repairobj)
        print(re_assign_to_employee_id, date)
        if not re_assign_to_employee_id or not date:
            return {'result': False, 'data': {}}
        repairobj.re_assign_to_employee_id = re_assign_to_employee_id.id
        repairobj.date_sheh = date
        # print('repairobj',repairobj)


        return {'result': True, 'data': {'date': str(repairobj.date_sheh)}}

    def get_employee_date(self, repair):
        values = []
        lis = []
        res = {}
        date_from = False
        date_to = False
        if repair.product_id:
            for lin in repair.product_id.categ_id.employee_list_ids:
                values.append(lin.id)
        else:
            return False, False
        if not repair.re_assign_to_employee_id:
            if values:
                if repair.employee_ids:
                    repair.employee_ids.unlink()
                for lin in values:
                    # employee_state=self.env['hr.employee'].search([('id','=',lin),('status_ids','in',self.partner_id.state_id.id)])

                    employees = request.env['hr.employee'].search([('id', '=', lin)])
                    for employee_state in employees:
                        # print(repair.res_partner_id.state_id.id, employee_state.status_ids.ids)
                        if repair.partner_id.state_id.id in employee_state.status_ids.ids:
                            # date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                            # date_to = date_from + datetime.timedelta(days=6)
                            # date_from = datetime.datetime.now().date()
                            # date_to = date_from
                            # date_to = date_to.replace(date_to.year, date_to.month, date_to.day + 6)

                            repair.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
                            repair.date_to = fields.Date.today() + datetime.timedelta(days=6)
                            # print(date_from, date_to)
                            if 1:

                                difference = repair.date_to - repair.date_from
                                days = difference.days

                                for day in range(0, days + 1):
                                    # print('day', day)
                                    date = repair.date_from + datetime.timedelta(days=day)
                                    # print('date', date)
                                    employee_capacity = request.env['employee.capacity'].search(
                                        [('employee_id', '=', employee_state.id), ('date', '=', date)])
                                    reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
                                    # print('cap', reminder_capacity, employee_capacity)

                                    if reminder_capacity > 0 or not employee_capacity:
                                        # print(employee_state, date)
                                        return employee_state, date

        return False, False

    def create_log(self, type,phone=False,partner_id=False,lot_id=False,partner_id_value=False,lot_id_value=False,code=False):
        logobj = request.env['ivr.log'].sudo().create(
            {
                'phone': phone,
                'type': type,
                'partner_id':partner_id,
                'lot_id': lot_id,
                'partner_id_value': partner_id_value,
                'lot_id_value': lot_id_value,
                'code': code,

            }
        )
