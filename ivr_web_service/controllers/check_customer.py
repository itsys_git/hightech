# -*- coding: utf-8 -*-

import json
from odoo import http
from odoo.http import request, Controller


class check_customer(Controller):
    @http.route('/check_customer', auth='public', cors='*')
    def check_customer(self, **args):
        # http://localhost:1212/check_customer?phone=123
        phone = args.get('phone')
        self.create_log('check', phone)
        if not phone:
            result = {'result': False, 'data': {}}
        else:
            partner_obj = request.env['res.partner']
            customers = partner_obj.sudo().search([('phone', '=', phone)], limit=1)
            if not customers or len(customers) > 1:
                result = {'result': False, 'data': {}}
            else:
                if len(customers.child_ids) > 1:
                    result = {'result': False, 'data': {}}
                else:
                    result = {'result': True, 'data': {'customerid': customers.id}}

        return http.request.make_response(json.dumps(result), {
            'Cache-Control': 'no-cache',
            'Content-Type': 'JSON; charset=utf-8',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
        })

    def create_log(self, type, phone=False, partner_id=False, lot_id=False, partner_id_value=False, lot_id_value=False,
                   code=False):
        logobj = request.env['ivr.log'].sudo().create(
            {
                'phone': phone,
                'type': type,
                'partner_id': partner_id,
                'lot_id': lot_id,
                'partner_id_value': partner_id_value,
                'lot_id_value': lot_id_value,
                'code': code,

            })
