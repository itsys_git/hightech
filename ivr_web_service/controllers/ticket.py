# -*- coding: utf-8 -*-

import json
from odoo import http
from odoo.http import request, Controller


class check_customer(Controller):
    @http.route('/ticket_status', auth='public', cors='*')
    def ticket_status(self, **args):
        # http://localhost:1212/ticket_status?customerid=7&device_type=ddccc&device_serial=0000049
        customerid = args.get('customerid')
        code = args.get('device_type')
        lot = args.get('device_serial')
        # print(customerid,code,lot)
        self.create_log('check_ticket', False,False,False,customerid,lot,code)

        if not customerid or not code or not lot:
            result = {'result': False, 'data': {}}
        lotobj = request.env['stock.production.lot'].sudo().search([('name', '=', lot)], limit=1)
        customerobj = request.env['res.partner'].sudo().search([('id', '=', int(customerid))], limit=1)
        # print(lotobj,customerobj)

        if not customerobj or not lotobj:
            result = {'result': False, 'data': {}}
        self.create_log('check_ticket', False,customerobj.id,lotobj.id,customerobj.id,lotobj.id,code)

        result = self.get_ticket(customerobj, lotobj, code)

        return http.request.make_response(json.dumps(result), {
            'Cache-Control': 'no-cache',
            'Content-Type': 'JSON; charset=utf-8',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
        })

    def get_ticket(self, customerobj, lotobj, code):
        # ('res_partner_id', '=', customerobj.id),
        repairobj = request.env['repair.order'].sudo().search(
            [('partner_id', '=', customerobj.id), ('lot_id', '=', lotobj.id), ('code', '=', code)], limit=1,
            order='id desc')
        print('repairobj',repairobj,repairobj.res_partner_id,customerobj.id,repairobj.state,repairobj.repairs_status_id,repairobj.repairs_status_id.repairs_status)
        if not repairobj:
            return {'result': False, 'data': {}}
        repairobj = repairobj[0]
        if repairobj.state in ['draft', 'confirmed'] and repairobj.repairs_status_id:
            return {'result': True,
                    'data': {'state': repairobj.repairs_status_id.repairs_status, 'date': str(repairobj.date_from),
                             'type': repairobj.ivr}}
        else:
            return {'result': False, 'data': {}}

    def create_log(self, type,phone=False,partner_id=False,lot_id=False,partner_id_value=False,lot_id_value=False,code=False):
        logobj = request.env['ivr.log'].sudo().create(
            {
                'phone': phone,
                'type': type,
                'partner_id':partner_id,
                'lot_id': lot_id,
                'partner_id_value': partner_id_value,
                'lot_id_value': lot_id_value,
                'code': code,

            }
        )
