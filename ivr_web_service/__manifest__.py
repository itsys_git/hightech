# -*- coding: utf-8 -*-

##############################################################################
{
    'name': 'ivr_web_service',
    'summary': """ivr_web_service""",
    'version': '12.0.1.0.0',
    'description': """ivr_web_service""",
     'author': "Said YAHIA",

    'category': 'base',
    'depends': ['base', 'stock','product','ivr_config'],
    'license': 'AGPL-3',
    'data': [
    ],
    'images': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
