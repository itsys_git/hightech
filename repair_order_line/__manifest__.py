# -*- coding: utf-8 -*-
{
    'name': "Repair Order Line",

    'description': """
        Appear Repair Order Lines
    """,

    'author': "Marwa Ahmed",
    'website': "http://www.yourcompany.com",
    'depends': ['base','repair','helpdesk','repair_employee_ticket','stock','hr',
                'maintenance_repair_fields','lot_serial_editing','repair_configration',
                'hr_employee_modify'],
    'data': [
      'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/transfer_wizard.xml',
        'views/repair_lines_screen2.xml',
        # 'views/transfer_screen2_wizard.xml',
    ],

}