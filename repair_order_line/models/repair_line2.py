# -*- coding: utf-8 -*-
from odoo import models, fields, api,_


class RepairOrderLine(models.Model):

    _inherit = 'repair.line'



    transfer_status_screen = fields.Selection([('draft_s', 'Draft'), ('done_s', 'Done')],
                                       default='draft_s')


    check_transfer2=fields.Boolean(default=False)

    def transfer_repair_screen2(self):
        move = self.env['stock.picking'].create({
            'check_bool': True,
            'repair_id': self.repair_id.id,
            'move_ids_without_package': [(0, 0,
            {
            'name':'Moves',
            'location_id': self.location_id.id,
            'location_dest_id': self.location_dest_id.id,
            'product_id':self.product_id.id,
            'product_uom': self.product_uom.id,
            'product_uom_qty': self.product_uom_qty,
                             })],
        })
        self.check_transfer2=True
        self.transfer_status_screen='done_s'
        return {
            'domain': [('id', '=',move.id)],
            'name': 'Transfer',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'stock.picking',
            'view_id': False,
            'views': [
                (self.env.ref('stock.vpicktree').id, 'tree'),
                (self.env.ref('stock.view_picking_form').id, 'form')],
            'type': 'ir.actions.act_window',
        }

