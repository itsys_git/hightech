# -*- coding: utf-8 -*-
from odoo import models, fields, api

class TransferWizardScreen(models.TransientModel):
    _name = 'transfer.wizard.screen'

    _description = "send the selected data to picking"


    @api.multi
    def send_to_stock_picking(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        picking_obj2 = self.env['repair.line'].browse(active_ids)
        for rec in picking_obj2:
            if not rec.transfer_status_screen == 'draft_s':
                continue
            move2 = self.env['stock.picking'].create({
            'partner_id': rec.partner_id.id,
            'location_id': rec.location_id.id,
            'location_dest_id':rec.location_dest_id.id,
            'product_id': rec.product_id.id,
            'product_uom': rec.product_uom.id,
            'product_uom_qty': rec.product_uom_qty,
            'picking_type_id' :rec.picking_type_id.id,
            'move_ids_without_package': [(0, 0,
                                              {
                                                  'name': 'Moves',
                                                  'location_id': rec.location_id.id,
                                                  'location_dest_id': rec.location_dest_id.id,
                                                  'product_id': rec.product_id.id,
                                                  'product_uom': rec.product_uom.id,
                                                  'product_uom_qty': rec.product_uom_qty,
                                              })],
            })
            rec.check_transfer2=True
            rec.transfer_status_screen='done_s'

