# -*- coding: utf-8 -*-
from odoo import models, fields, api

class TransferWizard(models.TransientModel):
    _name = 'transfer.wizard'

    _description = "send the selected data to picking"


    @api.multi
    def send_to_picking(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        picking_obj = self.env['repair.line'].browse(active_ids)
        for rec in picking_obj:
            if rec.parts_status == 'required_to_provide':
                move1 = self.env['stock.picking'].create({
                'partner_id': rec.partner_id.id,
                'repair_id': rec.repair_id.id,
                'location_id': rec.location_id.id,
                'location_dest_id':rec.location_dest_id.id,
                'product_id': rec.product_id.id,
                'product_uom': rec.product_uom.id,
                'product_uom_qty': rec.product_uom_qty,
                'picking_type_id' :rec.picking_type_id.id,
                'move_ids_without_package': [(0, 0,
                                                  {
                                                      'name': 'Moves',
                                                      'location_id': rec.location_id.id,
                                                      'location_dest_id': rec.location_dest_id.id,
                                                      'product_id': rec.product_id.id,
                                                      'product_uom': rec.product_uom.id,
                                                      'product_uom_qty': rec.product_uom_qty,
                                                  })],
                })
                rec.check_transfer = True
                rec.transfer_status = 'done'
                rec.parts_status = 'provides'

            elif rec.parts_status == 'used' and rec.guarantee_status == 'damedge_guarantee':
                if not rec.transfer_status_screen == 'draft_s':
                    continue
                move2 = self.env['stock.picking'].create({
                'partner_id': rec.partner_id.id,
                'repair_id': rec.repair_id.id,
                'location_id': rec.location_id.id,
                'location_dest_id':rec.location_dest_id.id,
                'product_id': rec.product_id.id,
                'product_uom': rec.product_uom.id,
                'product_uom_qty': rec.product_uom_qty,
                'picking_type_id' :rec.picking_type_id.id,
                'move_ids_without_package': [(0, 0,
                                                  {
                                                      'name': 'Moves',
                                                      'location_id': rec.location_id.id,
                                                      'location_dest_id': rec.location_dest_id.id,
                                                      'product_id': rec.product_id.id,
                                                      'product_uom': rec.product_uom.id,
                                                      'product_uom_qty': rec.product_uom_qty,
                                                  })],
                })
                rec.check_transfer2=True
                rec.transfer_status_screen='done_s'

