# -*- coding: utf-8 -*-
from odoo import models, fields, api,_
from odoo.exceptions import ValidationError


class RepairOrderLine(models.Model):

    _inherit = 'repair.line'

    @api.depends('parts_status','check_transfer')
    def get_tans_satu(self):
        for rec in self:
            if rec.parts_status=='used':
                rec.transfer_status=False
            if rec.parts_status=='required_to_provide':
                rec.transfer_status='draft'
            if rec.check_transfer==True:
                rec.transfer_status ='done'

    picking_type_id=fields.Many2one("stock.picking.type",string='Operation Type',required=True)
    partner_id=fields.Many2one('res.partner', related='repair_id.partner_id', string='Partner', readonly=True,store=True)
    ticket_id=fields.Many2one(related='repair_id.helpdesk_ticket_id',string='Helpdesk Ticket', readonly=True,store=True)
    assign_to_employee_id=fields.Many2one('hr.employee',related='repair_id.re_assign_to_employee_id',string='Center Name')
    technition_id=fields.Many2one('hr.employee',related='repair_id.technition',string='Technician')
    transfer_status = fields.Selection([('draft', 'مطلوب توفيرها'), ('done', 'تم التوفير')],
                                      compute='get_tans_satu')
    parts_status = fields.Selection([('used', 'تم الاستخدام'), ('required_to_provide', 'مطلوب توفيرها'),
                                     ('provides', 'تم التوفير')],string='موقف ق.غ')
    check_transfer=fields.Boolean(default=False)


    @api.multi
    def unlink(self):
        for rec in self:
            if rec.transfer_status=='done':
                raise ValidationError("You can't delete repair line where status id Done")
        return super(RepairOrderLine, self).unlink()


    def transfer_func(self):
        move = self.env['stock.picking'].create({
            'check_bool': True,
            'repair_id': self.repair_id.id,
            'move_ids_without_package': [(0, 0,
            {
            'name':'Moves',
            'location_id': self.location_id.id,

            'location_dest_id': self.location_dest_id.id,
            'product_id':self.product_id.id,
            'product_uom': self.product_uom.id,
            'product_uom_qty': self.product_uom_qty,
                             })],
        })
        self.check_transfer=True
        # self.transfer_status='done'
        self.parts_status='provides'
        return {
            'domain': [('id', '=',move.id)],
            'name': 'Transfer',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'stock.picking',
            'view_id': False,
            'views': [
                (self.env.ref('stock.vpicktree').id, 'tree'),
                (self.env.ref('stock.view_picking_form').id, 'form')],
            'type': 'ir.actions.act_window',
        }

class RepairOrder(models.Model):
    _inherit = 'repair.order'

    @api.onchange('operations','technition')
    def get_location(self):
        for rec in self.operations:
            if rec.guarantee_status == 'damedge_guarantee' and rec.parts_status == 'used':
                if self.technition:
                    sur_obj = self.env['stock.location'].search([
                        ('employee_number', '=', self.technition.emp_number),
                        ('flag', '=', False)])
                    des_obj = self.env['stock.location'].search([
                        ('employee_number', '=', self.technition.emp_number),
                        ('flag', '=', True)])
                    rec.location_id=sur_obj.id
                    rec.location_dest_id=des_obj.id

            if rec.guarantee_status == 'not_damedee_guarantee' and rec.parts_status == 'used':
                if self.technition:
                    sur_obj2 = self.env['stock.location'].search([
                        ('employee_number', '=', self.technition.emp_number),
                        ('flag', '=', False)])
                    rec.location_id=sur_obj2.id
                    rec.location_dest_id=9

            if rec.guarantee_status == 'in_pay' and rec.parts_status == 'used':
                if self.technition:
                    sur_obj2 = self.env['stock.location'].search([
                        ('employee_number', '=', self.technition.emp_number),
                        ('flag', '=', False)])
                    rec.location_id=sur_obj2.id
                    rec.location_dest_id=9

            if rec.parts_status == 'required_to_provide':
                if self.re_assign_to_employee_id and self.technition:
                    sur_obj3 = self.env['stock.location'].search([
                        ('employee_number', '=', self.re_assign_to_employee_id.emp_number),
                        ('flag', '=', False)])
                    des_obj3 = self.env['stock.location'].search([
                        ('employee_number', '=', self.technition.emp_number),
                        ('flag', '=', False)])
                    rec.location_id=sur_obj3.id
                    rec.location_dest_id=des_obj3.id

    @api.constrains('operations')
    def check_parts_oper(self):
        for rec in self:
              if rec.operations :
                  if not rec.technition:
                        raise ValidationError('You Must Enter Technician')



class StockPickng(models.Model):
    _inherit = 'stock.picking'

    check_bool = fields.Boolean()
    repair_id=fields.Many2one("repair.order")
class StockLocat(models.Model):

    _inherit = 'stock.location'

    employee_number =fields.Integer(string='Employee Number')
    flag=fields.Boolean(string='مخزن تكلفه')




    @api.constrains('flag', 'employee_number')
    def emp_num_unique(self):
        for rec in self:
            if len(self.search([('flag', '=', True), ('employee_number', '=', rec.employee_number)])) > 1:
                raise ValidationError(_('There are location with the same  number and Flag'))

            if len(self.search([('flag', '=', False), ('employee_number', '=', rec.employee_number)])) > 1:
                raise ValidationError(_('There are location with the same Employee Number'))

            if rec.employee_number < 0:
                raise ValidationError('Employee Number is not minus')



class Hr_Employee(models.Model):
    _inherit = 'hr.employee'

    emp_number=fields.Integer('Employee Number')

    @api.constrains('emp_number')
    def num_not_minus(self):
        for rec in self:
            if rec.emp_number < 0:
                raise ValidationError('Employee Number is not minus')

    _sql_constraints = [
        ('num_uniq', '', 'Employee Number must be unique!'),
    ]




