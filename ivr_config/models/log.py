from odoo import models, fields, api

from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re

class ivrlog(models.Model):
    _name = 'ivr.log'
    _rec_name = 'type'
    _order = 'id desc'
    partner_id = fields.Many2one('res.partner','customer')
    lot_id = fields.Many2one('stock.production.lot','lot')
    partner_id_value = fields.Char( 'customer value')
    lot_id_value = fields.Char('lot value')

    code = fields.Char('Device Code')
    phone = fields.Char('Phone')
    company_id = fields.Many2one('res.company', string='Company',  default=lambda self: self.env.user.company_id)
    type = fields.Selection([
        ('check', 'Check Customer'), ('check_ticket', 'Check Ticket'), ('create_ticket_warranty', 'Create Ticket/Warranty'),('warranty', 'Warranty'), ('create_ticket', 'Create Ticket')], string='State')


