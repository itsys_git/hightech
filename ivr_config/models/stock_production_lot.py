from odoo import models, fields, api

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError
import re

class Productemplate(models.Model):
    _inherit = 'product.template'

    hi_code = fields.Char("Hitech code", )
    man_code = fields.Char("Manfacture Code", )
    warranty_expiry_date = fields.Date(string='warranty Expiry date')

class productcat(models.Model):
    _inherit = 'product.category'

    code = fields.Char('code')

class SerialDealer(models.Model):
    _name='serial.dealer'
    name=fields.Char(required=True)

class stocklot(models.Model):
    _inherit = 'stock.production.lot'

    warranty = fields.Boolean('Warranty')
    code = fields.Char('code',related='product_id.categ_id.code')
    warranty_active_date=fields.Date('warranty active date')
    warranty_active_user_id=fields.Many2one('res.users','warranty active user')
    warranty_active_date1 = fields.Date('warranty active date')
    warranty_active_user_id1 = fields.Many2one('res.users', 'warranty active user')
    installation_date = fields.Date()
    buying_date = fields.Date()
    serial_dealer_id = fields.Many2one('serial.dealer', 'Serial Dealer name')

    # @api.constrains('installation_date', 'buying_date')
    # def date_constrains33(self):
    #
    #     for rec in self:
    #           if rec.installation_date != False and rec.buying_date != False:
    #                 if rec.installation_date > rec.buying_date:
    #                     raise ValidationError(_('Buying Date must be bigger than  Installation Date.'))

    @api.onchange('warranty')
    def onechangewarranty(self):
        for rec in self:
            if rec.warranty:
                rec.warranty_active_date=fields.Date.today()
                rec.warranty_active_user_id=rec.env.user.id
            else:
                rec.warranty_active_date = False
                rec.warranty_active_user_id= False
    # @api.depends('warranty_active_date1','warranty_active_date1')
    # def warrantydetail(self):
    #     for l in self:
    #         l.warranty_active_date=l.warranty_active_date1
    #         l.warranty_active_user_id=l.warranty_active_user_id1
class repair(models.Model):
    _inherit = 'repair.order'

    code = fields.Char('code',related='product_id.categ_id.code')
