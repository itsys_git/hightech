# -*- coding: utf-8 -*-

##############################################################################
{
    'name': 'IVR config',
    'summary': """IVR config""",
    'version': '12.0.1.0.0',
    'description': """IVR config""",
     'author': "Said YAHIA",

    'category': 'base',
    'depends': ['base', 'stock','product','repair','product'],
    'license': 'AGPL-3',
    'data': [
        'security/security.xml',

        'views/stock_production_lot.xml',
        'views/log_views.xml',
        'security/ir.model.access.csv',

    ],
    'images': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
