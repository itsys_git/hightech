# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime
class HrEmployee(models.Model):
    _inherit ='hr.employee'

    service_center=fields.Many2one('hr.employee',domain="[('is_service_center','=',True)]")
    is_service_center=fields.Boolean()
    service_center_branches_ids=fields.Many2many('service.center.branches','s1','s2')


    @api.onchange('employee_type_id')
    def service_center_create(self):
        if self.employee_type_id.employee_type =='Center' or self.employee_type_id.employee_type =='center':
            self.is_service_center=True

        else:
            self.is_service_center =False
    @api.model
    def create(self,vals):
        res=super(HrEmployee, self).create(vals)
        if res.is_service_center==True:
            res.service_center=res

        return res


class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'

    @api.multi
    def _get_field_name_default(self):
        search_id = self.env['priority.line'].search([('name','=','Normal')]).id
        if search_id:
            return search_id
        else:
            create_id = self.env['priority.line'].create({'name': 'Normal'}).id
            return create_id
    priority_new=fields.Many2one('priority.line','Priority',default=_get_field_name_default)


    users_ids=fields.Many2many(comodel_name='res.users',string='users',relation='res_users_helpdesk_ticket',column1='res_users_ticket1', column2='res_users_ticket2'
                               ,compute='collect_user',store=True)

    repairs_ids = fields.Many2many('repair.order', 'r1', 'r2',copy=False)


    @api.one
    @api.depends('repairs_ids')
    def collect_user(self):
        lis_user=[]
        for rec in self:
            for l in rec.repairs_ids:
                if l.re_assign_to_employee_id.user_id:
                    lis_user.append(l.re_assign_to_employee_id.user_id.id)

                elif  l.re_assign_to_employee_id.user_id.id == False and l.re_assign_to_employee_id.service_center.user_id :
                    lis_user.append(l.re_assign_to_employee_id.service_center.user_id.id)

            rec.users_ids=[(6,0,lis_user)]




class ServiceCenterBranches(models.Model):
    _name='service.center.branches'
    _rec_name='branch'
    branch=fields.Many2one('branches')
    service_center=fields.Many2one('hr.employee',domain="[('is_service_center','=',True)]")



class Branches(models.Model):
    _name='branches'
    name=fields.Char(required=True)


class RepairOrder(models.Model):
    _inherit ='repair.order'


class ResUsers(models.Model):
    _inherit ='res.users'
    # help_id=fields.Many2one('helpdesk.ticket')