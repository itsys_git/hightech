# -*- coding: utf-8 -*-
{
    'name': "Hr Employee Edit",

    'description': """
      add field in hr employee
    """,

    'author': "IT Systems Corportion Eman Ahmed",
    'website': "http://www.it-syscorp.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr_employee_edit','hr','employee_type','ticket_customer'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/hr_employee.xml',
        'views/helpdesk_ticket.xml',

        
    ],

}
