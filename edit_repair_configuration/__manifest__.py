# -*- coding: utf-8 -*-
{
    'name': "Edit repair configuration",

    'author': "Marwa Ahmed",
    'website': "http://www.yourcompany.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','import_product_xlsx','repair','helpdesk','repair_status_create','repair_configration'],

    # always loaded
    'data': [
        'views/views.xml',
    ],

}