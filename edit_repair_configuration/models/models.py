# -*- coding: utf-8 -*-

from odoo import models, fields, api

class RepairOrder(models.Model):
    _inherit = 'repair.order'

    repair_product_audit_id=fields.Many2one('product.audit',string='Product Audit')

    @api.model
    def create(self, vals):
        # print('Check AValueeeee',self.check_stat)
        # if vals.get('check_stat')==True:
        #     print('Check AValueeeee', vals.get('check_stat'))
        #     return super(RepairOrder, self)
        new_repair_status = vals.get('new_repair_status')
        new_repair_obj = self.env['repairs.status.action'].browse(new_repair_status)
        vals['new_repair_status'] = False
        res = super(RepairOrder, self).create(vals)
        for rec in res:
            if new_repair_obj != False:
                if new_repair_obj.action == 'create':
                    record = rec.copy_data()
                    for reco in record:
                        reco.update({'repairs_status_id': new_repair_obj.statusss_to.id,
                                     'repair_type_id': new_repair_obj.repair_type_id.id,
                                     'new_repair_status': False, 'operations': False, 'escalation_ids': False,
                                     'employee_ids': False, 'add_parts': False,'technition':False
                                        , 'move_id': False, 'invoice_line_id': False, 'fees_lines': False,
                                     'customer_labor': 0.0, 'agent_labor': 0.0,
                                     'factory_labor': 0.0, 'service_center_labor': 0.0, 'customer_area': 0.0,
                                     'agent_area': 0.0,
                                     'factory_area': 0.0, 'service_center_area': 0.0, 'customer_high': 0.0,
                                     'agent_high': 0.0,
                                     'factory_high': 0.0, 'service_center_high': 0.0, 'labor_total_collection': 0.0,
                                     'area_total_collection': 0.0,
                                     'hightech_total_collection': 0.0,
                                     'internal_notes': '', 'quotation_notes': '', 'repaired': False, 'invoiced': False
                                     })

                        self.env['repair.order'].create(reco)
                        rec.update({'repairs_status_id': new_repair_obj.status_to.id,
                                    'new_repair_status': False})
                if new_repair_obj.action == 'no_create':
                    rec.update({'repairs_status_id': new_repair_obj.status_to.id,
                                'new_repair_status': False})

        return res

    @api.multi
    def write(self, vals):
        new_repair_status = vals.get('new_repair_status')
        new_repair_obj = self.env['repairs.status.action'].browse(new_repair_status)
        print('writeeeeeeeeeeeeeeeeeeeeeeeeeee')
        vals['new_repair_status'] = False
        res = super(RepairOrder, self).write(vals)
        for rec in self:
            if new_repair_obj != False:

                if new_repair_obj.action == 'create':
                    print("ooooooooooooooooo")
                    record = rec.copy_data()
                    for reco in record:
                        reco.update({'repairs_status_id': new_repair_obj.statusss_to.id,
                                     'repair_type_id': new_repair_obj.repair_type_id.id,
                                    'technition':False,
                                     'new_repair_status': False, 'operations': False, 'escalation_ids': False,
                                     'employee_ids': False, 'add_parts': False
                                        , 'move_id': False, 'invoice_line_id': False, 'fees_lines': False,
                                     'customer_labor': 0.0, 'agent_labor': 0.0,
                                     'factory_labor': 0.0, 'service_center_labor': 0.0, 'customer_area': 0.0,
                                     'agent_area': 0.0,
                                     'factory_area': 0.0, 'service_center_area': 0.0, 'customer_high': 0.0,
                                     'agent_high': 0.0,
                                     'factory_high': 0.0, 'service_center_high': 0.0, 'labor_total_collection': 0.0,
                                     'area_total_collection': 0.0,
                                     'hightech_total_collection': 0.0,
                                     'internal_notes': '', 'quotation_notes': '', 'repaired': False, 'invoiced': False})
                        self.env['repair.order'].create(reco)
                        rec.update({'repairs_status_id': new_repair_obj.status_to.id,
                                    'new_repair_status': False})

                if new_repair_obj.action == 'no_create':
                    records = {'repairs_status_id': new_repair_obj.status_to.id,
                               'new_repair_status': False}
                    self.write(records)
        return res

    @api.multi
    def show_total_collection(self):
        if self.operations:
            cust_tot_la = 0.0
            fact_tot_la = 0.0
            service_tot_la = 0.0
            agent_tot_la = 0.0

            cust_tot_area = 0.0
            fact_tot_area = 0.0
            service_tot_area = 0.0
            agent_tot_area = 0.0

            cust_tot_high = 0.0
            fact_tot_high = 0.0
            service_tot_high = 0.0
            agent_tot_high = 0.0
            for lin in self:
                if lin.guarantee_status_p and lin.type_of_notice:
                    labor_obj = self.env['labor.setting'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('labor_code_id', '=', lin.labor_max.id),
                        ('labor_product_audit_id', '=', lin.product_id.product_audit_id.id),
                        ('type_of_notice', '=', lin.type_of_notice), ])
                    if labor_obj:
                        for rec in labor_obj:
                            cust_tot_la += rec.customer
                            fact_tot_la += rec.factory
                            service_tot_la += rec.service_center
                            agent_tot_la += rec.agent
                        self.customer_labor = cust_tot_la
                        self.factory_labor = fact_tot_la
                        self.service_center_labor = service_tot_la
                        self.agent_labor = agent_tot_la


                if lin.guarantee_status_p:
                    area_obj = self.env['area.collection'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('type_of_notice', '=', lin.type_of_notice),
                        ('governorate', '=', lin.partner_id.city_id.id),
                        ('city', '=', lin.partner_id.state_id.id),
                        ('service_center_id', '=', lin.re_assign_to_employee_id.id)

                    ])
                    if area_obj:
                        for rec in area_obj:
                            cust_tot_area += rec.customer
                            fact_tot_area += rec.factory
                            service_tot_area += rec.service_center
                            agent_tot_area += rec.agent
                        self.customer_area = cust_tot_area
                        self.factory_area = fact_tot_area
                        self.service_center_area = service_tot_area
                        self.agent_area = agent_tot_area

                    high_obj = self.env['hightech.collection'].search([
                        ('guarantee_status', '=', lin.guarantee_status_p),
                        ('type_of_notice', '=', lin.type_of_notice),
                        ('city', '=', lin.partner_id.state_id.id),
                        ('center_type', '=', lin.re_assign_to_employee_id.center_type.id),
                    ])
                    if high_obj:
                        for rec in high_obj:
                            cust_tot_high += rec.customer
                            fact_tot_high += rec.factory
                            service_tot_high += rec.service_center
                            agent_tot_high += rec.agent
                        self.customer_high = cust_tot_high
                        self.factory_high = fact_tot_high
                        self.service_center_high = service_tot_high
                        self.agent_high = agent_tot_high


class LaborSetting(models.Model):
    _inherit = 'labor.setting'

    labor_product_audit_id = fields.Many2one('product.audit', string='Product Audit')
    _sql_constraints = [
        ('labor_setting_uniq', 'unique (guarantee_status, type_of_notice,labor_code_id,labor_product_audit_id)',
         'You Cant Duplicate Data ')
    ]

class NewModule(models.Model):
    _inherit = 'repair.line'

    type = fields.Selection([
        ('add', 'Add'),
        ('remove', 'Remove')], 'Type', required=True,default='add')
