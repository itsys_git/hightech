# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import fields, models, _, api
import base64
import datetime
from xlrd import open_workbook


from odoo import SUPERUSER_ID
import logging
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

class stocklot(models.Model):
    _inherit = 'stock.production.lot'

    # _sql_constraints = [
    #     ('name_ref_uniq', 'CHECK (1=1)', 'The combination of serial number and product must be unique !'),
    # ]

    _sql_constraints = [
        ('name_ref_uniq', 'unique (name, product_id,issue_date)', 'The combination of serial number and product and Issue Date must be unique !'),
    ]
  #
  # _sql_constraints = [('name_ref_uniq', 'CHECK(1=1)', 'The combination of serial number and product must be unique !'),]