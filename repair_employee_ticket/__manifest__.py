# -*- coding: utf-8 -*-
{
    'name': "Repair Employee Ticket",

    'description': """
      repair for employee in the same category and the same employee 
    """,

    'author': "IT Systems Corportion Eman Ahmed",
    'website': "http://www.it-syscorp.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase','hr','product',
                'sale_management','contacts','repair','repaire_capacity',
                'maintenance_repair_fields','product_category',
                'helpdesk','hr_employee_edit','employee_repair_editing',
                'employee_type'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/helpdesk_ticket.xml',
        'views/repair_order.xml',

        
    ],

}
