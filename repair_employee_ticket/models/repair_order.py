# -*- coding: utf-8 -*-
import datetime
import time
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime


class RepairOrder(models.Model):
    _inherit ='repair.order'
    helpdesk_ticket_id=fields.Many2one('helpdesk.ticket','Helpdesk Ticket')
    employee_ids=fields.One2many('employee.capacity.new','repair_id')
    date_from=fields.Date(readonly=False,default=fields.Date.context_today)
    date_to=fields.Date(readonly=False,default=fields.Date.today()+datetime.timedelta(days=6))
    @api.onchange('product_id')
    def onchange_product_id(self):
        self.guarantee_limit = False
        self.lot_id = False
        if self.product_id:
            self.product_uom = self.product_id.uom_id.id
        if self.helpdesk_ticket_id:
            self.lot_id=self.helpdesk_ticket_id.lot_serial_id



    @api.onchange('helpdesk_ticket_id')
    def change_partner_id(self):
        if self.helpdesk_ticket_id:
            print('self.helpdesk_ticket_id.lot_serial_id',self.helpdesk_ticket_id.lot_serial_id.id)
            self.partner_id=self.helpdesk_ticket_id.partner_id
            self.product_id=self.helpdesk_ticket_id.product_id
            self.lot_id=self.helpdesk_ticket_id.lot_serial_id



    # @api.multi
    # def show_remaind(self):
    #     values = []
    #     lis = []
    #     res = {}
    #     if self.product_id and self.helpdesk_ticket_id:
    #         print('-----------------------------')
    #         if self.product_id == self.helpdesk_ticket_id.product_id:
    #             for lin in self.product_id.categ_id.employee_list_ids:
    #                 values.append(lin.id)
    #
    #     if not self.re_assign_to_employee_id:
    #         if values:
    #            if self.employee_ids:
    #                 self.employee_ids.unlink()
    #            for lin in values:
    #                employee_state = self.env['hr.employee'].search(['&','|',('id','=',lin),('status_ids','in',self.partner_id.state_id.id)
    #                                                           ,'|','|','|',('warranty_type','=',self.helpdesk_ticket_id.warranty_type.id),
    #                                                           ('ticket_type_id','=',self.helpdesk_ticket_id.ticket_type_id.id),
    #                                                           ('priority','=',self.helpdesk_ticket_id.priority_new.id),
    #                                                           ('repair_type_id','=',self.repair_type_id.id)])
    #
    #
    #                if employee_state:
    #                 for l in employee_state:
    #                  if self.date_from and self.date_to:
    #                      if self.date_to < self.date_from:
    #                          raise ValidationError("Date To must be bigger than Date From")
    #                      else:
    #                          difference = self.date_to - self.date_from
    #                          days = difference.days
    #
    #                          for day in range(0, days + 1):
    #                              print('day', day)
    #                              date = self.date_from + datetime.timedelta(days=day)
    #                              print('date', date)
    #                              employee_capacity = self.env['employee.capacity'].search(
    #                                  [('employee_id', '=', l.id), ('date', '=', date)])
    #                              print('cap', employee_capacity)
    #                              reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                              if employee_capacity:
    #                                  self.write({'employee_ids': [(0, 6,
    #                                                                {'employee_id': l.id,
    #                                                                 'cap_count': reminder_capacity,
    #                                                                 'date': date})]})
    #                              else:
    #                                  self.write({'employee_ids': [(0, 6,
    #                                                                {'employee_id': l.id,
    #                                                                 'cap_count': l.capacity,
    #                                                                 'date': date})]})
    #
    #                  if self.date_from and  not self.date_to:
    #                      employee_capacity = self.env['employee.capacity'].search(
    #                              [('employee_id', '=', l.id),('date','=',self.date_from)])
    #                      print('cap',employee_capacity)
    #                      reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                      if employee_capacity:
    #                         self.write({'employee_ids': [(0,6,
    #                                         {'employee_id': l.id,
    #                                          'cap_count':reminder_capacity ,
    #                                           'date': self.date_from})]})
    #                      else:
    #                         self.write({'employee_ids': [(0,6,
    #                                     {'employee_id': l.id,
    #                                      'cap_count': l.capacity,
    #                                      'date': self.date_from})]})
    #                  if not self.date_from and  not self.date_to:
    #                      self.date_from=datetime.datetime.now().strftime("%Y-%m-%d")
    #                      self.date_to=fields.Date.today()+datetime.timedelta(days=6)
    #                      if self.date_to < self.date_from:
    #                          raise ValidationError("Date To must be bigger than Date From")
    #                      else:
    #                          difference = self.date_to - self.date_from
    #                          days = difference.days
    #
    #                          for day in range(0, days + 1):
    #                              print('day', day)
    #                              date = self.date_from + datetime.timedelta(days=day)
    #                              print('date', date)
    #                              employee_capacity = self.env['employee.capacity'].search(
    #                                  [('employee_id', '=', l.id), ('date', '=', date)])
    #                              print('cap', employee_capacity)
    #                              reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                              if employee_capacity:
    #                                  self.write({'employee_ids': [(0, 6,
    #                                                                {'employee_id': l.id,
    #                                                                 'cap_count': reminder_capacity,
    #                                                                 'date': date})]})
    #                              else:
    #                                  self.write({'employee_ids': [(0, 6,
    #                                                                {'employee_id': l.id,
    #                                                                 'cap_count': l.capacity,
    #                                                                 'date': date})]})
    #                  if not self.date_from and self.date_to:
    #                      self.date_from=datetime.datetime.now().strftime("%Y-%m-%d")
    #                      if self.date_from and self.date_to:
    #                          if self.date_to < self.date_from:
    #                              raise ValidationError("Date To must be bigger than Date From")
    #                          else:
    #                              difference = self.date_to - self.date_from
    #                              days = difference.days
    #
    #                              for day in range(0, days + 1):
    #                                  print('day', day)
    #                                  date = self.date_from + datetime.timedelta(days=day)
    #                                  print('date', date)
    #                                  employee_capacity = self.env['employee.capacity'].search(
    #                                      [('employee_id', '=', l.id), ('date', '=', date)])
    #                                  print('cap', employee_capacity)
    #                                  reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                                  if employee_capacity:
    #                                      self.write({'employee_ids': [(0, 6,
    #                                                                    {'employee_id': l.id,
    #                                                                     'cap_count': reminder_capacity,
    #                                                                     'date': date})]})
    #                                  else:
    #                                      self.write({'employee_ids': [(0, 6,
    #                                                                    {'employee_id': l.id,
    #                                                                     'cap_count': l.capacity,
    #                                                                     'date': date})]})
    #     else:
    #         if self.employee_ids:
    #             self.employee_ids.unlink()
    #         employee_state = self.env['hr.employee'].search(
    #             [('id', '=',self.re_assign_to_employee_id.id)])
    #
    #         if employee_state:
    #           for l in employee_state:
    #             if self.date_from and self.date_to:
    #                 if self.date_to < self.date_from:
    #                     raise ValidationError("Date To must be bigger than Date From")
    #                 else:
    #                     difference = self.date_to - self.date_from
    #                     days = difference.days
    #
    #                     for day in range(0, days + 1):
    #                         print('day', day)
    #                         date = self.date_from + datetime.timedelta(days=day)
    #                         print('date', date)
    #                         employee_capacity = self.env['employee.capacity'].search(
    #                             [('employee_id', '=', l.id), ('date', '=', date)])
    #                         print('cap', employee_capacity)
    #                         reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                         if employee_capacity:
    #                             self.write({'employee_ids': [(0, 6,
    #                                                           {'employee_id': l.id,
    #                                                            'cap_count': reminder_capacity,
    #                                                            'date': date})]})
    #                         else:
    #                             self.write({'employee_ids': [(0, 6,
    #                                                           {'employee_id': l.id,
    #                                                            'cap_count': l.capacity,
    #                                                            'date': date})]})
    #
    #             if self.date_from and not self.date_to:
    #                 employee_capacity = self.env['employee.capacity'].search(
    #                     [('employee_id', '=', l.id), ('date', '=', self.date_from)])
    #                 print('cap', employee_capacity)
    #                 reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                 if employee_capacity:
    #                     self.write({'employee_ids': [(0, 6,
    #                                                   {'employee_id': l.id,
    #                                                    'cap_count': reminder_capacity,
    #                                                    'date': self.date_from})]})
    #                 else:
    #                     self.write({'employee_ids': [(0, 6,
    #                                                   {'employee_id': l.id,
    #                                                    'cap_count':l.capacity,
    #                                                    'date': self.date_from})]})
    #             if not self.date_from and not self.date_to:
    #                 self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
    #                 self.date_to = fields.Date.today() + datetime.timedelta(days=6)
    #                 if self.date_to < self.date_from:
    #                     raise ValidationError("Date To must be bigger than Date From")
    #                 else:
    #                     difference = self.date_to - self.date_from
    #                     days = difference.days
    #
    #                     for day in range(0, days + 1):
    #                         print('day', day)
    #                         date = self.date_from + datetime.timedelta(days=day)
    #                         print('date', date)
    #                         employee_capacity = self.env['employee.capacity'].search(
    #                             [('employee_id', '=', l.id), ('date', '=', date)])
    #                         print('cap', employee_capacity)
    #                         reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                         if employee_capacity:
    #                             self.write({'employee_ids': [(0, 6,
    #                                                           {'employee_id': l.id,
    #                                                            'cap_count': reminder_capacity,
    #                                                            'date': date})]})
    #                         else:
    #                             self.write({'employee_ids': [(0, 6,
    #                                                           {'employee_id': l.id,
    #                                                            'cap_count': l.capacity,
    #                                                            'date': date})]})
    #             if not self.date_from and self.date_to:
    #                 self.date_from = datetime.datetime.now().strftime("%Y-%m-%d")
    #                 if self.date_from and self.date_to:
    #                     if self.date_to < self.date_from:
    #                         raise ValidationError("Date To must be bigger than Date From")
    #                     else:
    #                         difference = self.date_to - self.date_from
    #                         days = difference.days
    #
    #                         for day in range(0, days + 1):
    #                             print('day', day)
    #                             date = self.date_from + datetime.timedelta(days=day)
    #                             print('date', date)
    #                             employee_capacity = self.env['employee.capacity'].search(
    #                                 [('employee_id', '=', l.id), ('date', '=', date)])
    #                             print('cap', employee_capacity)
    #                             reminder_capacity = employee_capacity.employee_id.capacity - employee_capacity.cap_count
    #                             if employee_capacity:
    #                                 self.write({'employee_ids': [(0, 6,
    #                                                               {'employee_id': l.id,
    #                                                                'cap_count': reminder_capacity,
    #                                                                'date': date})]})
    #                             else:
    #                                 self.write({'employee_ids': [(0, 6,
    #                                                               {'employee_id': l.id,
    #                                                                'cap_count': l.capacity,
    #                                                                'date': date})]})
    #
    # @api.onchange('partner_id', 'product_id','helpdesk_ticket_id','repair_type_id')
    # def change_values(self):
    #     values = []
    #     lis = []
    #     res = {}
    #     list2 = []
    #     l_final = []
    #     domain1=[]
    #     domain2=[]
    #     lsl=[]
    #     if self.product_id and self.helpdesk_ticket_id:
    #         print('-----------------------------')
    #         if self.product_id==self.helpdesk_ticket_id.product_id:
    #             for lin in self.product_id.categ_id.employee_list_ids:
    #                 values.append(lin.id)
    #
    #
    #     if values:
    #        for lin in values:
    #            employee_state=self.env['hr.employee'].search(['&','|',('id','=',lin),('status_ids','in',self.partner_id.state_id.id)
    #                                                           ,'|','|','|',('warranty_type','=',self.helpdesk_ticket_id.warranty_type.id),
    #                                                           ('ticket_type_id','=',self.helpdesk_ticket_id.ticket_type_id.id),
    #                                                           ('priority','=',self.helpdesk_ticket_id.priority_new.id),
    #                                                           ('repair_type_id','=',self.repair_type_id.id)])
    #
    #
    #            if employee_state:
    #             for l in employee_state:
    #                 lis.append(l.id)
    #        print('emp',lis)
    #        domain1 = [('id', 'in',lis)]
    #
    #     if self.partner_id:
    #
    #         search_lot = self.env['stock.production.lot'].search([('partner_id', '=', self.partner_id.id)])
    #         # print (search_lot.product_id)
    #         if search_lot:
    #             for line in search_lot:
    #                 if line.product_id.spare_parts == False:
    #                     list2.append(line.product_id.id)
    #
    #         if list2 :
    #             domain2 = [('id', 'in', list2)]
    #         else:
    #             search_lot = self.env['product.product'].search([('spare_parts','=',False)])
    #             for l in search_lot:
    #                 lsl.append(l.id)
    #             domain2 = [('id', 'in',lsl)]
    #     res['domain'] = {'re_assign_to_employee_id': domain1,'product_id':domain2}
    #
    #     return res


class HelpdeskTicket(models.Model):
    _inherit ='helpdesk.ticket'
    repair_order_count=fields.Integer(compute='_compute_repair_order_count', string='repair Order Count')





    def _compute_repair_order_count(self):
            repair_data = self.env['repair.order'].read_group(
                [('helpdesk_ticket_id', 'in', self.ids)],
                ['helpdesk_ticket_id'], ['helpdesk_ticket_id'])
            result = dict((data['helpdesk_ticket_id'][0], data['helpdesk_ticket_id_count']) for data in repair_data)
            for ticket in self:
                ticket.repair_order_count = result.get(ticket.id,0)


class EmployeeCapacityNew(models.Model):
    _name= 'employee.capacity.new'
    repair_id=fields.Many2one('repair.order')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    date = fields.Date('Date')
    cap_count = fields.Integer('Count')

    @api.multi
    def send_emp_date(self):
        se_repair=self.env['repair.order'].search([('id','=',self.repair_id.id)])
        se_repair.write({'re_assign_to_employee_id':self.employee_id.id,'date_sheh':self.date})
        # se_repair.emp_capacity()




