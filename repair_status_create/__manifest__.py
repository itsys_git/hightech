# -*- coding: utf-8 -*-
{
    'name': " repair status create",

    'author': "Marwa Ahmed & Eman Ahmed",
    'website': "http://www.it-syscorp.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','repair_status','helpdesk','hr_employee_edit','customer_profile_editing2','escalation','repair_employee_ticket','repair_parts'],

    # always loaded
    'data': [

        'security/ir.model.access.csv',
        'security/security.xml',
        'views/repair_status_action.xml',
        'sequences/seq_repair_status.xml',
        'views/setup.xml',





    ],

}