from odoo import models, fields, api
from odoo.exceptions import ValidationError

class SetUp(models.Model):
    _name='set.up'

    _rec_name = 'group_name'

    seq = fields.Char()

    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('set.up') or '/'
        values['seq'] = seq
        res = super(SetUp, self).create(values)
        return res

    group_name = fields.Char()
    users=fields.Many2many('res.users','res1','res2')