from odoo import models, fields, api
from odoo.exceptions import ValidationError

class RepairsStatus(models.Model):
    _inherit ='repairs.status'
    # _rec_name ='seq'
    seq=fields.Char()

    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('repair.status') or '/'
        values['seq'] = seq
        res = super(RepairsStatus, self).create(values)
        return res

class RepairsStatusAction(models.Model):
    _name='repairs.status.action'
    _rec_name='status_to'
    group_access=fields.Many2many('set.up',string='Group Access')
    seq = fields.Char()

    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('repair.status.action') or '/'
        values['seq'] = seq
        res = super(RepairsStatusAction, self).create(values)
        return res

    status_from = fields.Many2one('repairs.status')
    status_to=fields.Many2one('repairs.status')
    statusss_to = fields.Many2one('repairs.status',string='New Status To')
    action=fields.Selection([('create','Create'),('no_create','No Create')])
    repair_type_id = fields.Many2one('helpdesk.repair.type', string="Repair Type")

    @api.constrains('status_from','status_to')
    def uniqe_service_center(self):
        ser2 = self.search([('status_from', '=', self.status_to.id),('status_to', '=', self.status_from.id), ('id', '!=', self.id)])
        ser1= self.search([('status_from', '=', self.status_from.id),('action', '=', self.action), ('status_to', '=', self.status_to.id),('id', '!=', self.id)])
        # if ser2:
        #     raise ValidationError("This Make Recursive in States , Status From and Status To Already Exists")
        if ser1:
            raise ValidationError("This Make Dublicate Status,Status From and Status To Already Exists")
        if self.status_from == self.status_to:
            raise ValidationError("State From Should'nt be  The Same State To")

    @api.constrains('action')
    def change_action(self):
        if self.action=='no_create':
            if self.statusss_to:
                self.statusss_to=''