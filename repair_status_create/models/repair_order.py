from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime
import time

class RepairOrder(models.Model):
    _inherit ='repair.order'

    city=fields.Many2one('city.model',related='partner_id.city_id',store=True)
    state_id=fields.Many2one('res.country.state',related='partner_id.state_id',string='State',store=True)
    #
    # @api.constrains('date_from','date_to')
    # def no_old_date(self):
    #     current_date=datetime.datetime.now().strftime("%Y-%m-%d")
    #     for rec in self:
    #         if rec.date_from and datetime.datetime.strftime(rec.date_from,"%Y-%m-%d") < current_date:
    #             raise ValidationError("can not enter old date in date from")
    #
    #         if rec.date_to and datetime.datetime.strftime(rec.date_to,"%Y-%m-%d") < current_date:
    #             raise ValidationError("can not enter old date in date to")

            # if rec.date_sheh and datetime.datetime.strftime(rec.date_sheh,"%Y-%m-%d") < current_date:
            #     raise ValidationError("can not enter old date in schedule date")

    # @api.constrains('date_sheh')
    # def no_old_date2(self):
    #     current_date2 = datetime.datetime.now().strftime("%Y-%m-%d")
    #     for rec in self:
    #         if rec.date_sheh and datetime.datetime.strftime(rec.date_sheh, "%Y-%m-%d") < current_date2:
    #             raise ValidationError("can not enter old date in schedule date")

    new_repair_status=fields.Many2one('repairs.status.action',domain="[('status_from','=',repairs_status_id)]")
    check_stat=fields.Boolean()
