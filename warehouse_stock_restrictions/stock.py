# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import Warning

class ResUsers(models.Model):
    _inherit = 'res.users'

    restrict_locations = fields.Boolean('Restrict Location')

    stock_location_ids = fields.Many2many(
        'stock.location',
        'location_security_stock_location_users',
        'user_id',
        'location_id',
        'Stock Locations')

    default_picking_type_ids = fields.Many2many(
        'stock.picking.type', 'stock_picking_type_users_rel',
        'user_id', 'picking_type_id', string='Default Warehouse Operations')

    @api.multi
    def write(self,vals):
        print("kkkkkkkkkkkkkkkkkkkkk",vals)
        if vals.get('stock_location_ids'):
            rule_id=self.env.ref('warehouse_stock_restrictions.filter_user_stock_picking_source_location')
            rule2_id=self.env.ref('warehouse_stock_restrictions.filter_user_nove_move_source_location')
            if rule_id:
                rule_id.sudo().write({'active':False})
                rule_id.sudo().write({'active': True})
            if rule2_id:
                rule2_id.sudo().write({'active':False})
                rule2_id.sudo().write({'active': True})


        return super(ResUsers,self).write(vals)

class stock_move(models.Model):
    _inherit = 'stock.move'

    @api.one
    @api.constrains('state', 'location_id','location_dest_id')
    def check_user_location_rights(self):
        print(self.picking_id.state,self.state)
        if self.state == 'draft' or self.state=='confirmed' or self.state=='assigned':
            return True
        if self.picking_id and self.picking_id.picking_type_id and self.picking_id.picking_type_id.code == 'internal':

            user_locations = self.env.user.stock_location_ids
            print('internal          ')
            print("Checking access %s" %self.env.user.default_picking_type_ids)
            if self.env.user.restrict_locations:
                message = _(
                    'Invalid Location. You cannot process this move since you do '
                    'not control the location "%s". '
                    'Please contact your Adminstrator.')
                # if self.location_id not in user_locations:
                #     raise Warning(message % self.location_id.name)
                if self.location_dest_id not in user_locations:
                    raise Warning(message % self.location_dest_id.name)
        else:
            user_locations = self.env.user.stock_location_ids
            print(user_locations)
            print("Checking access %s" % self.env.user.default_picking_type_ids)
            if self.env.user.restrict_locations:
                message = _(
                    'Invalid Location. You cannot process this move since you do '
                    'not control the location "%s". '
                    'Please contact your Adminstrator.')
                if self.location_id not in user_locations:
                    raise Warning(message % self.location_id.name)
                elif self.location_dest_id not in user_locations:
                    raise Warning(message % self.location_dest_id.name)


















































